package com.upc.sealback.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.upc.sealback.bean.SealAuth;
import com.upc.sealback.bean.SealUser;
import com.upc.sealback.config.ResponseResult;
import com.upc.sealback.mapper.SealConMapper;
import com.upc.sealback.service.SealAuthService;
import com.upc.sealback.service.SealConService;
import com.upc.sealback.service.SealUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

/**
 * @description 授权印章信息控制类
 * @create zymac
 **/
@Controller
public class SealAuthController {
    @Autowired
    SealAuthService sealAuthService;
    @Autowired
    SealConService sealConService;
    @Autowired
    SealUserService sealUserService;

    /**
     * 新增授权用户
     * @param sealAuth 授权用户信息
     * @return 是否新增成功的提示
     */
    @ResponseBody
    @PostMapping("api/addSealAuth")
    public ResponseResult<Object> addSealAuth(@RequestBody SealAuth sealAuth){
        if(sealAuth.getAuthSealId() != null){
            sealAuth.setCreateTime(new Date());
            sealAuth.setAuthStatus(1);
            int result = sealAuthService.addSealAuth(sealAuth);
            int next = 0;
            if(result == 1){
                int num = sealConService.getSealAuthorizeNumBySealId(sealAuth.getAuthSealId()) + 1;
                next = sealConService.updateSealAuthorizeNum(sealAuth.getAuthSealId(),num);
            }
            return ResponseResult.getMysqlRUDResult(next);
        }
        return ResponseResult.getSuccessResult("failed");
    }

    /**
     * 根据印章id分页查询授权印章列表
     * @param current 当前页面
     * @param pageSize 分页大小
     * @param authSealId 授权印章id
     * @return
     */
    @ResponseBody
    @GetMapping("api/getSealAuthById")
    public ResponseResult<Object> getSealAuthById(@RequestParam Integer current,@RequestParam Integer pageSize,@RequestParam String authSealId){
        if(authSealId != null){
            List<SealAuth> sealAuths = sealAuthService.getSealAuthBySealId(new Page<>(current,pageSize),authSealId);
            for (SealAuth sealAuth : sealAuths){
                String name = sealUserService.getUserByUserId(sealAuth.getAuthSealUser()).getUserName();
                sealAuth.setAuthSealUserName(name);
            }
            return ResponseResult.getSuccessResult(sealAuths);
        }else {
            return ResponseResult.getSuccessResult("failed");
        }
    }

    /**
     * 根据id获取授权印章数量
     * @param authSealId 授权印章id
     * @return
     */
    @ResponseBody
    @GetMapping("api/getSealAuthNumBySealId")
    public ResponseResult<Object> getSealAuthNumBySealId(@RequestParam String authSealId){
        Integer num = sealAuthService.getSealAuthNumBySealId(authSealId);
        return ResponseResult.getSuccessResult(num);
    }

    /**
     * 更新授权信息
     * @param sealAuth
     * @return
     */
    @ResponseBody
    @PutMapping("api/updateSealAuth")
    public ResponseResult<Object> updateSealAuth(@RequestBody SealAuth sealAuth){
        if(sealAuth.getUuid() != null){
            Date date = new Date();
            if(date.compareTo(sealAuth.getAuthEndTime()) > 0){
                sealAuth.setAuthStatus(0);
            }else {
                sealAuth.setAuthStatus(1);
            }
            sealAuth.setUpdateTime(new Date());
            int result = sealAuthService.updateSealAuthById(sealAuth);
            return ResponseResult.getMysqlRUDResult(result);
        }else {
            return ResponseResult.getSuccessResult("failed");
        }
    }

    /**
     * 根据uuid删除授权印章
     * @param uuid
     * @return
     */
    @ResponseBody
    @DeleteMapping("api/delSealAuth")
    public ResponseResult<Object> delSealAuth(@RequestParam Integer uuid){
        if (uuid != null){
            SealAuth sealAuth = sealAuthService.getSealAuthById(uuid);
            int result = sealAuthService.deleteSealAuth(uuid);
            int next = 0;
            if(result == 1){
                int num = sealConService.getSealAuthorizeNumBySealId(sealAuth.getAuthSealId()) - 1;
                next = sealConService.updateSealAuthorizeNum(sealAuth.getAuthSealId(),num);
            }
            return ResponseResult.getMysqlRUDResult(next);
        }else {
            return ResponseResult.getSuccessResult("failed");
        }
    }
}
