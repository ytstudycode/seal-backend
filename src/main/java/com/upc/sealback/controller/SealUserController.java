package com.upc.sealback.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.upc.sealback.bean.SealUser;
import com.upc.sealback.config.ResponseResult;
import com.upc.sealback.service.SealUserService;
import com.upc.sealback.utils.Authentication;
import jdk.jfr.Description;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.Objects;

@Controller
public class SealUserController {
    @Autowired
    SealUserService sealUserService;

    /**
     * 更新用户邮箱
     * @param sealUser 用户信息
     * @return 是否更新成功的提示
     */
    @ResponseBody
    @PutMapping("api/updateUserEmail")
    public ResponseResult<Object> updateUserEmail(@RequestBody SealUser sealUser){
        if(sealUser.getUserId() != null){
            Date date = new Date();
            int result = sealUserService.updateUserEmail(sealUser.getUserId(),sealUser.getUserEmail(),date);
            if(result == 1){
                return ResponseResult.getSuccessResult("success");
            }
        }
        return ResponseResult.getErrorResult("400","databaseError");
    }

    /**
     * 实名认证
     * @param sealUser
     * @return
     */
    @ResponseBody
    @PutMapping("api/certificate")
    public ResponseResult<Object> Certificate(@RequestBody SealUser sealUser){
        if(sealUser.getUserId() != null){
            ResponseResult<Object> responseResult = Authentication.Certificate(sealUser);
            if(responseResult.getResult() == "success"){
                sealUser.setUserStatus(2);
                Date date = new Date();
                sealUser.setUpdateTime(date);
                int a = sealUserService.updateUser(sealUser);
                if(a == 1){
                    return responseResult;
                }else {
                    return ResponseResult.getErrorResult("400","databaseError");
                }
            }
            return responseResult;
        }else {
            return ResponseResult.getSuccessResult("failed");
        }
    }

    /**
     * 通过用户名或者手机号搜索用户信息
     * @param content 搜索内容
     * @return 符合条件的用户信息
     */
    @ResponseBody
    @GetMapping("api/getUserBySearch")
    public ResponseResult<Object> getUserBySearch(@RequestParam String content){
        if(Objects.equals(content, "")){
            return ResponseResult.getSuccessResult(null);
        }
        return ResponseResult.getSuccessResult(sealUserService.getSealUserBySearch(new Page<>(1,5),content));
    }
}
