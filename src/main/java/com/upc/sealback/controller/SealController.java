package com.upc.sealback.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.upc.sealback.bean.SealAuth;
import com.upc.sealback.bean.SealCon;
import com.upc.sealback.bean.SealDetail;
import com.upc.sealback.config.ResponseResult;
import com.upc.sealback.service.SealAuthService;
import com.upc.sealback.service.SealConService;
import com.upc.sealback.service.SealDetailService;
import com.upc.sealback.service.SealUserService;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.DigestUtils;
import org.springframework.web.bind.annotation.*;

import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.List;

/**
 * 印章数据存储于获取控制类
 */
@Controller
public class SealController {
    @Autowired
    SealConService sealConService;
    @Autowired
    SealDetailService sealDetailService;
    @Autowired
    SealUserService sealUserService;
    @Autowired
    SealAuthService sealAuthService;

    /**
     * 保存印章详情
     * @param sealDetail 印章详情
     * @return 是否保存成功的提示
     */
    @ResponseBody
    @PostMapping("api/addSealDetail")
    public ResponseResult<Object> addSealDetail(@RequestBody @NotNull SealDetail sealDetail) throws Exception {

        ResponseResult<Object> responseResult = null;
        //将文件名使用md5加密生成印章编号
        String fileName = sealDetail.getSealPath().split(".com/")[1].split("\\.")[0];
        String nameToMd5 = DigestUtils.md5DigestAsHex(fileName.getBytes(StandardCharsets.UTF_8));
        sealDetail.setSealId(nameToMd5);
        Date date =new Date();
        sealDetail.setCreateTime(date);
        int a = sealDetailService.addSealDetail(sealDetail);
        return ResponseResult.getMysqlRUDResult(a);
    }

    /**
     * 新增印章
     * @param sealCon 创建印章的信息
     * @return 是否新增成功的提示
     */
    @ResponseBody
    @PostMapping("api/addSeal")
    public ResponseResult<Object> addSeal(@RequestBody SealCon sealCon){
        //将文件名使用md5加密生成印章编号
        String fileName = sealCon.getSealPath().split(".com/")[1].split("\\.")[0];
        String nameToMd5 = DigestUtils.md5DigestAsHex(fileName.getBytes(StandardCharsets.UTF_8));
        sealCon.setSealId(nameToMd5);
        Date date =new Date();
        sealCon.setCreateTime(date);
        int result = sealConService.addSealCon(sealCon);
        return ResponseResult.getMysqlRUDResult(result);
    }

    /**
     * 根据用户名获取印章列表
     * @param userId 用户id
     * @param current 当前页码
     * @param pageSize 分页大小
     * @return 印章列表
     */
    @ResponseBody
    @GetMapping("api/getSealByCreater")
    public ResponseResult<Object> getSealByCreater(@RequestParam String userId,@RequestParam Integer current,@RequestParam Integer pageSize){
        Page<SealCon> sealConPage = new Page<>(current,pageSize);
        List<SealCon> sealConList = sealConService.getAllBySealCreater(sealConPage,userId);
        return ResponseResult.getSuccessResult(sealConList);
    }

    /**
     * 获取印章数量
     * @param userId 印章创建人
     * @return
     */
    @ResponseBody
    @GetMapping("api/getSealNum")
    public ResponseResult<Object> getSealNum(@RequestParam String userId){
        int num = sealConService.getSealNum(userId);
        return ResponseResult.getSuccessResult(num);
    }

    /**
     * 根据用户 分页联查 被授权的印章 table[seal_con,seal_auth]
     * @param authSealUser 被授权的用户
     * @return
     */
    @ResponseBody
    @GetMapping("api/getAuthSeal")
    public ResponseResult<Object> getAuthSeal(@RequestParam String authSealUser,@RequestParam Integer current,@RequestParam Integer pageSize){
        if(authSealUser != null){
            Page<SealCon> sealConPage = new Page<>(current,pageSize);
            List<SealCon> sealConList = sealConService.getSealAuth(sealConPage,authSealUser);
            for (SealCon sealCon : sealConList){
                Date date = new Date();
                if(date.compareTo(sealCon.getSealAuth().getAuthEndTime()) > 0){
                    sealCon.getSealAuth().setAuthStatus(0);
                }else {
                    sealCon.getSealAuth().setAuthStatus(1);
                }
                String name = sealUserService.getUserByUserId(sealCon.getSealCreater()).getUserName();
                sealCon.setSealCreater(name);
            }
            return ResponseResult.getSuccessResult(sealConList);
        }else {
            return ResponseResult.getSuccessResult("failed");
        }
    }

    /**
     * 获取被授权的印章的数量
     * @param authSealUser
     * @return
     */
    @ResponseBody
    @GetMapping("api/getAuthSealNum")
    public ResponseResult<Object> getAuthSealNum(@RequestParam String authSealUser){
        int result = sealConService.getSealAuthNum(authSealUser);
        return ResponseResult.getSuccessResult(result);
    }

    /**
     * 删除印章
     * @param sealId
     * @return
     */
    @ResponseBody
    @DeleteMapping("api/delSeal")
    public ResponseResult<Object> delSeal(@NotNull @RequestParam String sealId){
        int result1 = sealConService.delBySealId(sealId);
        int result2 = sealDetailService.delBySealId(sealId);
        int result3 = sealAuthService.deleteSealAuthBySealId(sealId);
        if(result1 == 1){
            return ResponseResult.getSuccessResult("success");
        }else {
            return ResponseResult.getErrorResult("400","databaseError");
        }
    }


}
