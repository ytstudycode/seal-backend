package com.upc.sealback.controller;

import com.aliyun.oss.common.utils.StringUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.upc.sealback.bean.SealAntiFake;
import com.upc.sealback.bean.SealSign;
import com.upc.sealback.bean.SealSigner;
import com.upc.sealback.bean.SealUser;
import com.upc.sealback.config.ResponseResult;
import com.upc.sealback.service.SealSignService;
import com.upc.sealback.service.SealSignerService;
import com.upc.sealback.service.SealUserService;
import com.upc.sealback.utils.FileOSS;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.DigestUtils;
import org.springframework.web.bind.annotation.*;

import javax.xml.crypto.Data;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import static com.upc.sealback.utils.DigitalCertificate.KeyStoreGenerate.signPdf;
import static com.upc.sealback.utils.FileUtils.deleteFile;
import static com.upc.sealback.utils.StringUtil.strIsEmpty;

/**
 * @description 文件签署控制类
 * @create zymac
 * @date 2022/3/27 20:51
 **/
@Controller
public class SealSignController {
    @Autowired
    SealSignService sealSignService;
    @Autowired
    SealUserService sealUserService;
    @Autowired
    SealSignerService sealSignerService;

    /**
     * 获取签署后的文件信息
     * @param pdfUrl pdf路径
     * @param sealAntiFake 加过水印的电子印章的信息
     * @return 签署后的文本信息
     * @throws Exception
     */
    @ResponseBody
    @GetMapping("api/getSignFile")
    public ResponseResult<Object> getSignFile(@RequestParam String pdfUrl,
                                              @RequestParam String sealAntiFake) throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        SealAntiFake sealAntiFake1 = objectMapper.readValue(sealAntiFake,SealAntiFake.class);
        String sealUrl = sealAntiFake1.getAntiFakePath();
        String pdfName = pdfUrl.split(".com/")[1]; //获取文件名
        String pdfPath = FileOSS.fileGetOss(pdfName).getResult().toString(); //从oss中下载文件到本地，获取本地路径
        String sealPath = System.getProperty("user.dir") + "/src/main/resources/static/" + sealUrl; //从oss中下载文件到本地，获取本地路径
        // 从数据库中获取该签署人的基础签署信息
        SealSigner sealSigner = sealSignerService.getSignerBySignerId(sealAntiFake1.getAntiFakeUser(),sealAntiFake1.getAntiFakeSignId());
        // 签署印章
        ResponseResult<Object> responseResult = signPdf(pdfPath,sealPath,sealSigner);

        sealSigner.setSignerAntiFakeSealId(sealAntiFake1.getAntiFakeSealId());
        sealSigner.setSignerSignTime(new Date());
        sealSigner.setUpdateTime(new Date());

        // 更新签署人签署信息
        Integer updateSignerResult = sealSignerService.updateSign(sealSigner);

        // 该文件是否全部签署
        boolean isAllSigned = true;

        // 根据签署ID获取签署人的信息
        List<SealSigner> sealSignerList = sealSignerService.getSealSignerBySignId(sealAntiFake1.getAntiFakeSignId());
        for(SealSigner sealSigner1 : sealSignerList){
            if (sealSigner1.getSignerSignStatus() != 2){
                isAllSigned = false;
            }
        }

        SealSign sealSign = new SealSign();
        sealSign.setSignId(sealAntiFake1.getAntiFakeSignId());
        sealSign.setSignFilePath(responseResult.getResult().toString());
        if(isAllSigned){
            // 如果全部签署人都签署该文件 则设置文件签署状态为 已完成
            sealSign.setSignEndTime(new Date());
            sealSign.setSignStatus(4);
        }else {
            sealSign.setSignStatus(3);
        }
        // 更新文件签署信息
        Integer updateResult = sealSignService.updateSealSign(sealSign);

        deleteFile(pdfPath); //删除本地pdf
        deleteFile(sealPath); //删除本地印章
        return responseResult;
    }

    /**
     * 新增文件签署信息
     * @param sealSign 文件信息
     * @return 是否新增成功的提示
     */
    @ResponseBody
    @PostMapping("api/addSealSign")
    public ResponseResult<Object> addSealSign(@RequestBody SealSign sealSign){
        Date date = new Date();
        String signId = DigestUtils.md5DigestAsHex(date.toString().getBytes(StandardCharsets.UTF_8)).substring(0,16);
        sealSign.setSignId(signId);
        sealSign.setCreateTime(date);
        sealSign.setSignStartTime(date);
        if(sealSign.getSignStatus() == 3){
            // 如果是直接发起，就在签署人表中保存签署人
            List<SealSigner> sealSignerList = sealSign.getSealSigner();
            for (SealSigner sealSigner : sealSignerList){
                sealSigner.setSignerSignStatus(0);
                sealSigner.setCreateTime(new Date());
                sealSigner.setSignerSignId(signId);
            }
            sealSignerService.addSealSigner(sealSignerList);
        }
        int result = sealSignService.addSealSign(sealSign);
        if(result == 1){
            return ResponseResult.getSuccessResult(signId);
        }else {
            return ResponseResult.getMysqlRUDResult(result);
        }

    }

    /**
     * 更新印章基础信息
     * @param sealSign
     * @return
     */
    @ResponseBody
    @PutMapping("api/updateSignBasic")
    public ResponseResult<Object> updateSignBasic(@RequestBody SealSign sealSign){
        if(sealSign.getSignStatus() == 3){
            sealSign.setSignStartTime(new Date());
        }
        Integer result = sealSignService.updateSignBasic(sealSign);
        return ResponseResult.getMysqlRUDResult(result);
    }

    /**
     * 根据参与人获取任务信息
     * @param current 当前索引
     * @param pageSize 分页大小
     * @param signParticipant 任务参与人
     * @return 任务信息
     */
    @ResponseBody
    @GetMapping("api/getReceiveSign")
    public ResponseResult<Object> getReceiveSign(@RequestParam Integer current,@RequestParam Integer pageSize,@RequestParam String signParticipant){
        List<SealSign> sealSignList = sealSignService.getSealSignBySignParticipant(new Page<>(current,pageSize),signParticipant);
        int flag = 0;
        for(SealSign sealSign : sealSignList){
            Date date = new Date();
            // 如果该任务未被签署且已经过期
            if(sealSign.getSignStatus() == 3 && date.compareTo(sealSign.getSignDeadline()) == 1){
                //在数据中更新任务状态
                sealSignService.updateSealSignStatus(sealSign.getSignId(),1,sealSign.getSignDeadline());
                flag = 1;
            }
        }
        if (flag == 1){
            sealSignList = sealSignService.getSealSignBySignParticipant(new Page<>(current,pageSize),signParticipant);
        }
        sealSignList = sealSignerService.getUserNameById(sealSignList);
        return ResponseResult.getSuccessResult(sealSignList);
    }

    /**
     * 根据参与人获取任务数量
     * @param signParticipant 任务参与人
     * @return 任务数量
     */
    @ResponseBody
    @GetMapping("api/getReceiveSignNum")
    public ResponseResult<Object> getReceiveSignNum(@RequestParam String signParticipant){
        Integer num = sealSignService.getSealSignNumBySignParticipant(signParticipant);
        return ResponseResult.getSuccessResult(num);
    }

    /**
     * 根据参与人或者任务信息搜索任务信息
     * @param current 分页
     * @param pageSize 分页
     * @param signParticipant 参与人
     * @param content 搜索内容
     * @return
     */
    @ResponseBody
    @GetMapping("api/getSealSignBySearchParticipant")
    public ResponseResult<Object> getSealSignBySearchParticipant(@RequestParam Integer current,
                                                                 @RequestParam Integer pageSize,
                                                                 @RequestParam String signParticipant,
                                                                 @RequestParam String content){
        SealUser sealUser = sealUserService.getUserByName(content);
        if(sealUser != null){
            content = sealUser.getUserId();
        }
        List<SealSign> sealSignList = sealSignService.getSealSignBySearchParticipant(new Page<>(current,pageSize),signParticipant,content);
        sealSignList = sealSignerService.getUserNameById(sealSignList);
        return ResponseResult.getSuccessResult(sealSignList);
    }

    /**
     * 根据参与人或者任务信息获取任务数量
     * @param signParticipant 参与人
     * @param content 搜索内容
     * @return
     */
    @ResponseBody
    @GetMapping("api/getSealSignNumBySearchParticipant")
    public ResponseResult<Object> getSealSignNumBySearchParticipant(@RequestParam String signParticipant,
                                                                    @RequestParam String content){
        SealUser sealUser = sealUserService.getUserByName(content);
        if(sealUser != null){
            content = sealUser.getUserId();
        }
        Integer num = sealSignService.getSealSignNumBySearchParticipant(signParticipant,content);
        return ResponseResult.getSuccessResult(num);
    }

    /**
     * 删除签署任务
     * @param signId
     * @return 是否删除成功的提示
     */
    @ResponseBody
    @DeleteMapping("api/deleteSign")
    public ResponseResult<Object> deleteSign(@RequestParam String signId){
        Integer result = sealSignService.deleteSign(signId);
        Integer result2 = sealSignerService.deleteSigner(signId);
        return ResponseResult.getMysqlRUDResult(result);
    }

    /**
     * 根据任务发起人获取任务信息
     * @param current 当前索引
     * @param pageSize 分页大小
     * @param signSponsor 任务发起人
     * @return 任务信息
     */
    @ResponseBody
    @GetMapping("api/getStartSign")
    public ResponseResult<Object> getStart(@RequestParam Integer current,@RequestParam Integer pageSize,@RequestParam String signSponsor){
        List<SealSign> sealSignList = sealSignService.getSealSignBySignSponsor(new Page<>(current,pageSize),signSponsor);
        int flag = 0;
        for(SealSign sealSign : sealSignList){
            Date date = new Date();
            // 如果该任务未被签署且已经过期
            if(sealSign.getSignStatus() == 3 && date.compareTo(sealSign.getSignDeadline()) == 1){
                //在数据中更新任务状态
                sealSignService.updateSealSignStatus(sealSign.getSignId(),1,sealSign.getSignDeadline());
                flag = 1;
            }
        }
        if (flag == 1){
            sealSignList =  sealSignService.getSealSignBySignSponsor(new Page<>(current,pageSize),signSponsor);
        }
        sealSignList = sealSignerService.getUserNameById(sealSignList);
        return ResponseResult.getSuccessResult(sealSignList);
    }

    /**
     * 根据任务发起人获取任务数量
     * @param signSponsor 任务发起人
     * @return 任务数量
     */
    @ResponseBody
    @GetMapping("api/getStartSignNum")
    public ResponseResult<Object> getStartSignNum(@RequestParam String signSponsor){
        int num = sealSignService.getSealSignNumBySignSponsor(signSponsor);
        return ResponseResult.getSuccessResult(num);
    }

    /**
     * 根据发起人或者任务信息搜索任务信息(不包含草稿)
     * @param current 分页
     * @param pageSize 分页
     * @param signSponsor 任务发起人
     * @param content 搜索内容
     * @return
     */
    @ResponseBody
    @GetMapping("api/getSealSignBySearchSignSponsor")
    public ResponseResult<Object> getSealSignBySearchSignSponsor(@RequestParam Integer current,
                                                                 @RequestParam Integer pageSize,
                                                                 @RequestParam String signSponsor,
                                                                 @RequestParam String content){
        SealUser sealUser = sealUserService.getUserByName(content);
        if(sealUser != null){
            content = sealUser.getUserId();
        }
        List<SealSign> sealSignList = sealSignService.getSealSignBySearchSignSponsor(new Page<>(current,pageSize),signSponsor,content);
        sealSignList = sealSignerService.getUserNameById(sealSignList);
        return ResponseResult.getSuccessResult(sealSignList);
    }

    /**
     * 根据发起人或者任务信息获取搜索到的任务数量
     * @param signSponsor 任务发起人
     * @param content 搜索内容
     * @return
     */
    @ResponseBody
    @GetMapping("api/getSealSignNumBySearchSignSponsor")
    public ResponseResult<Object> getSealSignNumBySearchSignSponsor(@RequestParam String signSponsor,
                                                                    @RequestParam String content){
        SealUser sealUser = sealUserService.getUserByName(content);
        if(sealUser != null){
            content = sealUser.getUserId();
        }
        Integer num = sealSignService.getSealSignNumBySearchSignSponsor(signSponsor,content);
        return ResponseResult.getSuccessResult(num);
    }

    /**
     * 根据签署任务的状态获取任务列表
     * @param current 分页
     * @param pageSize 分页
     * @param signSponsor 任务发起人
     * @param signStatus 任务状态
     * @return
     */
    @ResponseBody
    @GetMapping("api/getSealSignByStatus")
    public ResponseResult<Object> getSealSignByStatus(@RequestParam Integer current,
                                                      @RequestParam Integer pageSize,
                                                      @RequestParam String signSponsor,
                                                      @RequestParam Integer signStatus){
        List<SealSign> sealSignList = sealSignService.getSealSignByStatus(new Page<>(current,pageSize),signSponsor,signStatus);
        int flag = 0;
        for(SealSign sealSign : sealSignList){
            Date date = new Date();
            // 如果该任务未被签署且已经过期
            if(sealSign.getSignStatus() == 3 && date.compareTo(sealSign.getSignDeadline()) == 1){
                //在数据中更新任务状态
                sealSignService.updateSealSignStatus(sealSign.getSignId(),1,sealSign.getSignDeadline());
                flag = 1;
            }
        }
        if (flag == 1){
            sealSignList = sealSignService.getSealSignByStatus(new Page<>(current,pageSize),signSponsor,signStatus);
        }
        sealSignList = sealSignerService.getUserNameById(sealSignList);
        return ResponseResult.getSuccessResult(sealSignList);
    }

    /**
     * 根据签署任务的状态获取任务列表数量
     * @param signSponsor 任务发起人
     * @param signStatus 任务状态
     * @return
     */
    @ResponseBody
    @GetMapping("api/getSealSignNumByStatus")
    public ResponseResult<Object> getSealSignNumByStatus(@RequestParam String signSponsor,
                                                         @RequestParam Integer signStatus){
        Integer num = sealSignService.getSealSignNumByStatus(signSponsor,signStatus);
        return ResponseResult.getSuccessResult(num);
    }

    /**
     * 根据签署任务的状态搜索符合条件的任务列表
     * @param current
     * @param pageSize
     * @param signSponsor
     * @param signStatus
     * @param content
     * @return
     */
    @ResponseBody
    @GetMapping("api/getSealSignBySearchStatus")
    public ResponseResult<Object> getSealSignBySearchStatus(@RequestParam Integer current,
                                                            @RequestParam Integer pageSize,
                                                            @RequestParam String signSponsor,
                                                            @RequestParam Integer signStatus,
                                                            @RequestParam String content){
        SealUser sealUser = sealUserService.getUserByName(content);
        if(sealUser != null){
            content = sealUser.getUserId();
        }
        List<SealSign> sealSignList = sealSignService.getSealSignBySearchStatus(new Page<>(current,pageSize),signSponsor,signStatus,content);
        sealSignList = sealSignerService.getUserNameById(sealSignList);
        return ResponseResult.getSuccessResult(sealSignList);
    }

    /**
     * 根据签署任务的状态搜索符合条件的任务数量
     * @param signSponsor
     * @param signStatus
     * @param content
     * @return
     */
    @ResponseBody
    @GetMapping("api/getSealSignNumBySearchStatus")
    public ResponseResult<Object> getSealSignNumBySearchStatus(@RequestParam String signSponsor,
                                                               @RequestParam Integer signStatus,
                                                               @RequestParam String content){
        SealUser sealUser = sealUserService.getUserByName(content);
        if(sealUser != null){
            content = sealUser.getUserId();
        }
        Integer num = sealSignService.getSealSignNumBySearchStatus(signSponsor,signStatus,content);
        return ResponseResult.getSuccessResult(num);
    }

    /**
     * 获取待我操作的任务
     * @param current 分页
     * @param pageSize 分页
     * @param signParticipant 待操作人
     * @return
     */
    @ResponseBody
    @GetMapping("api/getSealSignOperatedByMe")
    public ResponseResult<Object> getSealSignOperatedByMe(@RequestParam Integer current,
                                                          @RequestParam Integer pageSize,
                                                          @RequestParam String signParticipant){
        List<SealSigner> sealSignerList = sealSignerService.getSealSignOperatedByMe(new Page<>(current,pageSize),signParticipant);
        int flag = 0;
        for(SealSigner sealSigner : sealSignerList){
            Date date = new Date();
            // 如果该任务未被签署且已经过期
            if(sealSigner.getSealSign().getSignStatus() == 3 && date.compareTo(sealSigner.getSealSign().getSignDeadline()) == 1){
                //在数据中更新任务状态
                sealSignService.updateSealSignStatus(sealSigner.getSealSign().getSignId(),1,sealSigner.getSealSign().getSignDeadline());
                flag = 1;
            }
        }
        if (flag == 1){
            sealSignerList = sealSignerService.getSealSignOperatedByMe(new Page<>(current,pageSize),signParticipant);
        }

        for (SealSigner sealSigner : sealSignerList){
            SealSign sealSign = sealSigner.getSealSign();
            // 将发起人id 替换成 姓名
            if(!strIsEmpty(sealSign.getSignSponsor())){
                sealSign.setSignSponsor(sealUserService.getUserByUserId(sealSign.getSignSponsor()).getUserName());
            }

            // 获取该签署信息的所有参与人信息
            List<SealSigner> sealSignerList1 = sealSignerService.getSealSignerBySignId(sealSign.getSignId());

            // 将参与人id 替换成 姓名
            for(SealSigner sealSigner1 : sealSignerList1){
                if(!strIsEmpty(sealSigner1.getSignerId())){
                    sealSigner1.setSignerId(sealUserService.getUserByUserId(sealSigner1.getSignerId()).getUserName());
                }
            }

            //将签署人信息放到签署文件信息中
            sealSign.setSealSigner(sealSignerList1);
        }
        return ResponseResult.getSuccessResult(sealSignerList);
    }

    /**
     * 获取待我操作的任务的数量
     * @param signParticipant 待操作人
     * @return
     */
    @ResponseBody
    @GetMapping("api/getSealSignNumOperatedByMe")
    public ResponseResult<Object> getSealSignNumOperatedByMe(@RequestParam String signParticipant){
        Integer num = sealSignerService.getSealSignNumOperatedByMe(signParticipant);
        return ResponseResult.getSuccessResult(num);
    }

    /**
     * 搜索待我操作的任务
     * @param current 分页
     * @param pageSize 分页
     * @param signParticipant 待操作人
     * @param content 搜索内容
     * @return
     */
    @ResponseBody
    @GetMapping("api/getSealSignOperatedByMeBySearch")
    public ResponseResult<Object> getSealSignOperatedByMeBySearch(@RequestParam Integer current,
                                                                  @RequestParam Integer pageSize,
                                                                  @RequestParam String signParticipant,
                                                                  @RequestParam String content){
        SealUser sealUser = sealUserService.getUserByName(content);
        if(sealUser != null){
            content = sealUser.getUserId();
        }
        List<SealSigner> sealSignerList = sealSignerService.getSealSignOperatedByMeBySearch(new Page<>(current,pageSize),signParticipant,content);
        for (SealSigner sealSigner : sealSignerList){
            SealSign sealSign = sealSigner.getSealSign();
            // 将发起人id 替换成 姓名
            if(!strIsEmpty(sealSign.getSignSponsor())){
                sealSign.setSignSponsor(sealUserService.getUserByUserId(sealSign.getSignSponsor()).getUserName());
            }

            // 获取该签署信息的所有参与人信息
            List<SealSigner> sealSignerList1 = sealSignerService.getSealSignerBySignId(sealSign.getSignId());

            // 将参与人id 替换成 姓名
            for(SealSigner sealSigner1 : sealSignerList1){
                if(!strIsEmpty(sealSigner1.getSignerId())){
                    sealSigner1.setSignerId(sealUserService.getUserByUserId(sealSigner1.getSignerId()).getUserName());
                }
            }

            //将签署人信息放到签署文件信息中
            sealSign.setSealSigner(sealSignerList1);
        }
        return ResponseResult.getSuccessResult(sealSignerList);
    }

    /**
     * 获取待我操作的任务的数量
     * @param signParticipant 待操作人
     * @param content 搜索内容
     * @return
     */
    @ResponseBody
    @GetMapping("api/getSealSignNumOperatedByMeBySearch")
    public ResponseResult<Object> getSealSignNumOperatedByMeBySearch(@RequestParam String signParticipant,
                                                                     @RequestParam String content){
        SealUser sealUser = sealUserService.getUserByName(content);
        if(sealUser != null){
            content = sealUser.getUserId();
        }
        Integer num = sealSignerService.getSealSignNumOperatedByMeBySearch(signParticipant,content);
        return ResponseResult.getSuccessResult(num);
    }

    /**
     * 获取草稿详情
     * @param signId
     * @return
     */
    @ResponseBody
    @GetMapping("api/getSealSignDraft")
    public ResponseResult<Object> getSealSignDraft(@RequestParam String signId){
        SealSign sealSign = sealSignService.getSealSignBySignId(signId);
        return ResponseResult.getSuccessResult(sealSign);
    }

    /**
     * 根据任务id获取签署信息详情
     * @param signId
     * @param userId
     * @return
     */
    @ResponseBody
    @GetMapping("api/getSealSignBySignId")
    public ResponseResult<Object> getSealSignBySignId(@RequestParam String signId,@RequestParam String userId){
        // 获取查看该文件的人的签署信息
        SealSigner tempSealSigner = sealSignerService.getSignerBySignerId(userId,signId);
        // 如果查看人是签署人
        if(tempSealSigner != null){
            if(tempSealSigner.getSignerSignStatus() == 0){ //如果没有查看，则更新查看信息
                tempSealSigner.setSignerSignId(signId);
                tempSealSigner.setSignerId(userId);
                tempSealSigner.setSignerViewTime(new Date());
                sealSignerService.updateSignerViewTime(tempSealSigner);
            }
        }
        SealSign sealSign = sealSignService.getSealSignBySignId(signId);
        // 将发起人id 替换成 姓名
        if(!strIsEmpty(sealSign.getSignSponsor())){
            sealSign.setSignSponsor(sealUserService.getUserByUserId(sealSign.getSignSponsor()).getUserName());
        }

        String[] signParticipant = sealSign.getSignParticipant().split(",");
        for (int i=0;i< signParticipant.length;i++){
            signParticipant[i] = sealUserService.getUserByUserId(signParticipant[i]).getUserName();
        }
        sealSign.setSignParticipant(StringUtils.join(",", signParticipant));

        // 获取该签署信息的所有参与人信息
        List<SealSigner> sealSignerList = sealSignerService.getSealSignerBySignId(sealSign.getSignId());

        // 将参与人id 替换成 姓名
        for(SealSigner sealSigner : sealSignerList){
            if(!strIsEmpty(sealSigner.getSignerId())){
                sealSigner.setSignerId(sealUserService.getUserByUserId(sealSigner.getSignerId()).getUserName());
            }
        }

        //将签署人信息放到签署文件信息中
        sealSign.setSealSigner(sealSignerList);
        return ResponseResult.getSuccessResult(sealSign);
    }


    /**
     * 更新印章签署人的签署印章位置
     * @param sealSignerList 各个签署人签署印章位置list
     * @return
     */
    @ResponseBody
    @PutMapping("api/updateSignPosition")
    public ResponseResult<Object> updateSignPosition(@RequestBody List<SealSigner> sealSignerList){
        int result = sealSignerService.updateSignPosition(sealSignerList);
        return ResponseResult.getMysqlRUDResult(result);
    }

    /**
     * 撤回签署文件
     * @param signId
     * @return
     */
    @ResponseBody
    @PutMapping("api/withdrawSign")
    public ResponseResult<Object> withdrawSign(@RequestBody String signId){
        JSONObject jsonObj = new JSONObject(signId);
        String signId1= (String) jsonObj.get("signId");
        Integer result = sealSignService.updateSealSignStatus(signId1,2,new Date());
        return ResponseResult.getMysqlRUDResult(result);
    }

    /**
     * 获取用户最近签署的5条数据
     * @param signParticipant 当前用户
     * @return
     */
    @ResponseBody
    @GetMapping("api/getSealSignLately")
    public ResponseResult<Object> getSealSignLately(@RequestParam String signParticipant){
        List<SealSign> sealSignList = sealSignService.getSealSignLately(signParticipant);
        sealSignList = sealSignerService.getUserNameById(sealSignList);
        return ResponseResult.getSuccessResult(sealSignList);
    }



}
