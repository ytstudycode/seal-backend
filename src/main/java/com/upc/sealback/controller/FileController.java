package com.upc.sealback.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.upc.sealback.config.ResponseResult;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.Objects;

/**
 * @description 文件控制类
 * @create zymac
 * @date 2022/5/7 21:21
 **/
@Controller
public class FileController {

    @Value("${upload-file.path}")
    private String filePath;
    /**
     * 上传图片
     * @param multipartFile
     * @param request
     * @return
     */
    @ResponseBody
    @RequestMapping("api/uploadFile")
    public String uploadFile(@RequestParam("myFile") MultipartFile multipartFile, HttpServletRequest request)
    {
        //获取文件在服务器的储存位置
        String property = System.getProperty("user.dir");
        //当获取存储位置为root则代表在服务器上，主要是方便部署与测试
        String path = "";
        if(Objects.equals(property, "/root"))
        {
            path = "/www/wwwroot/dist/upload/";
        }else {
            path = property + filePath;
        }
        //String path = "/www/wwwroot/dist/upload/";
        File filePath = new File(path);
        if (!filePath.exists() && !filePath.isDirectory()) {
            filePath.mkdir();
        }
        //获取原始文件名称(包含格式)
        String originalFileName = multipartFile.getOriginalFilename();
        //获取文件类型，以最后一个`.`为标识
        String type = originalFileName.substring(originalFileName.lastIndexOf(".") + 1);
        //获取文件名称（不包含格式）
        String name = originalFileName.substring(0, originalFileName.lastIndexOf("."));

        String filename = Calendar.getInstance().getTimeInMillis()+"."+type;

        //在指定路径下创建一个文件
        File targetFile = new File(path, filename);

        //将文件保存到服务器指定位置
        try {
            multipartFile.transferTo(targetFile);
            //
            String newFilePath = "/upload/"+filename;
            return newFilePath;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }


    /**
     * 删除多个文件
     * @param paths
     * @return
     */
    @ResponseBody
    @PostMapping("api/delfilearr")
    public ResponseResult<Object> delFileArray(@RequestBody String paths)
    {
        JSONObject jsonObject = JSONObject.parseObject(paths);
        JSONArray jsonArray = (JSONArray)jsonObject.get("paths");
        String resultInfo = null;
        for(int i=0;i<jsonArray.size();i++){
            //获取文件在服务器的储存位置
            String property = System.getProperty("user.dir");
            String NewPath= "";
            //获取文件在服务器的储存位置
            //获取存储位置为root则代表在服务器上，主要是方便部署与测试
            if(Objects.equals(property, "/root"))
            {
                NewPath = "/www/wwwroot/dist"+jsonArray.get(i).toString();
            }else {
                NewPath = property + "/src/main/static"+jsonArray.get(i).toString();
            }
            //String NewPath = property + "/src/main/static"+jsonArray.get(i).toString();
            File file = new File(NewPath);
            //System.out.println(file.exists());
            if (file.exists()) {//文件是否存在
                if (file.delete()) {//存在就删了，返回1
                    resultInfo =  "1";
                } else {
                    resultInfo =  "0";
                }
            } else {
                resultInfo = "文件不存在！";
            }
        }
        return ResponseResult.getSuccessResult(resultInfo);
    }
}
