package com.upc.sealback.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.upc.sealback.bean.PdfSignInfo;
import com.upc.sealback.bean.SealAntiFake;
import com.upc.sealback.bean.SealSign;
import com.upc.sealback.bean.SealSigner;
import com.upc.sealback.config.ResponseResult;
import com.upc.sealback.service.SealAntiFakeService;
import com.upc.sealback.service.SealSignerService;
import com.upc.sealback.service.SealUserService;
import com.upc.sealback.utils.BlindWatermark.BlindWatermark;
import com.upc.sealback.utils.FileOSS;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Objects;

import static com.upc.sealback.utils.DigitalCertificate.KeyStoreGenerate.getSignFromPdf;
import static com.upc.sealback.utils.StringUtil.strIsEmpty;


/**
 * @description 防伪电子印章控制类
 * @create zymac
 * @date 2022/5/6 14:29
 **/
@Controller
public class SealAntiFakeController {
    @Autowired
    SealAntiFakeService sealAntiFakeService;
    @Autowired
    SealUserService sealUserService;
    @Autowired
    SealSignerService sealSignerService;

    /**
     * 导出防伪电子印章
     * @param sealAntiFake
     * @return
     * @throws Exception
     */
    @ResponseBody
    @PostMapping("api/exportAntiFakeSeal")
    public ResponseResult<Object> exportAntiFakeSeal(@RequestBody SealAntiFake sealAntiFake) throws Exception {
        sealAntiFake.setAntiFakePurpose(0);
        ResponseResult<Object> responseResult = sealAntiFakeService.exportAntiFakeSeal(sealAntiFake);
        return responseResult;
    }

    /**
     * 生成加上水印的电子印章
     * @return
     */
    @ResponseBody
    @PostMapping("api/sealWatermark")
    public ResponseResult<Object> getSealWatermark(@RequestBody SealSign sealSign) throws Exception {
        SealAntiFake sealAntiFake = new SealAntiFake();
        sealAntiFake.setAntiFakeSealTemplateId(sealSign.getSignTemplateSealId());
        sealAntiFake.setAntiFakeUser(sealUserService.getUserByName(sealSign.getSignParticipant()).getUserId());
        sealAntiFake.setAntiFakePurpose(1);
        sealAntiFake.setAntiFakeSignId(sealSign.getSignId());
        sealAntiFakeService.exportAntiFakeSeal(sealAntiFake);
        return ResponseResult.getSuccessResult(sealAntiFake);
    }

    /**
     * 校验防伪电子印章
     * @param sealAntiFake
     * @return
     * @throws Exception
     */
    @ResponseBody
    @PostMapping("api/verifySeal")
    public ResponseResult<Object> verifySeal(@RequestBody SealAntiFake sealAntiFake) throws Exception {
        return sealAntiFakeService.checkAntiFakeSeal(sealAntiFake);
    }

    /**
     * 获取印章使用详情
     * @param current 当前索引
     * @param pageSize 分页大小
     * @param sealId 印章id
     * @param purpose 用途
     * @return
     */
    @ResponseBody
    @GetMapping("api/getAntiFakeSealListByTemplateId")
    public ResponseResult<Object> getAntiFakeSealListByTemplateId(@RequestParam Integer current,@RequestParam Integer pageSize,@RequestParam String sealId,@RequestParam Integer purpose){
        List<SealAntiFake> sealAntiFakeList = sealAntiFakeService.getAntiFakeSealListByTemplateId(new Page<>(current,pageSize),sealId,purpose);
        for (SealAntiFake sealAntiFake : sealAntiFakeList){
            if(!Objects.equals(sealAntiFake.getSealSign().getSignSponsor(), "")){
                sealAntiFake.getSealSign().setSignSponsor(sealUserService.getUserByUserId(sealAntiFake.getSealSign().getSignSponsor()).getUserName());
            }
            if(!Objects.equals(sealAntiFake.getAntiFakeUser(), "")){
                sealAntiFake.setAntiFakeUser(sealUserService.getUserByUserId(sealAntiFake.getAntiFakeUser()).getUserName());
            }
        }
        return ResponseResult.getSuccessResult(sealAntiFakeList);
    }

    /**
     * 根据模板印章id和使用用途获取使用数量
     * @param sealId
     * @param purpose
     * @return
     */
    @ResponseBody
    @GetMapping("api/getAntiFakeSealNumByTemplateId")
    public ResponseResult<Object> getAntiFakeSealNumByTemplateId(@RequestParam String sealId,@RequestParam Integer purpose){
        Integer num = sealAntiFakeService.getAntiFakeSealNumByTemplateId(sealId,purpose);
        return ResponseResult.getSuccessResult(num);
    }

    /**
     * 根据签署人获取签署信息
     * @return
     */
    @ResponseBody
    @GetMapping("api/getSignDetailByUser")
    public ResponseResult<Object> getSignDetailByUser(@RequestParam Integer current,@RequestParam Integer pageSize,@RequestParam String antiFakeUser,@RequestParam Integer purpose){
        List<SealAntiFake> sealAntiFakeList = sealAntiFakeService.getSignDetailByUser(new Page<>(current,pageSize),antiFakeUser, purpose);
        return ResponseResult.getSuccessResult(sealAntiFakeList);
    }

    /**
     * 根据签署人获取签署信息数量
     * @param antiFakeUser 用户
     * @param purpose 用途
     * @return
     */
    @ResponseBody
    @GetMapping("api/getSignDetailNumByUser")
    public ResponseResult<Object> getSignDetailNumByUser(@RequestParam String antiFakeUser,@RequestParam Integer purpose){
        Integer num = sealAntiFakeService.getSignDetailNumByUser(antiFakeUser,purpose);
        return ResponseResult.getSuccessResult(num);
    }

    /**
     * 根据id获取电子印章使用信息
     * @param antiFakeSealId
     * @param purpose
     * @return
     */
    @ResponseBody
    @GetMapping("api/getAntiFakeDetail")
    public ResponseResult<Object> getAntiFakeDetail(@RequestParam String antiFakeSealId,@RequestParam Integer purpose){
        SealAntiFake sealAntiFake = sealAntiFakeService.getAntiFakeDetail(antiFakeSealId,purpose);
        List<SealSigner> sealSignerList = sealSignerService.getSealSignerBySignId(sealAntiFake.getAntiFakeSignId());
        for (SealSigner sealSigner : sealSignerList){
            if(!strIsEmpty(sealSigner.getSignerId())){
                sealSigner.setSignerId(sealUserService.getUserByUserId(sealSigner.getSignerId()).getUserName());
            }
        }
        if(!strIsEmpty(sealAntiFake.getSealSign().getSignSponsor())){
            sealAntiFake.getSealSign().setSignSponsor(sealUserService.getUserByUserId(sealAntiFake.getSealSign().getSignSponsor()).getUserName());
        }
        sealAntiFake.setSealSigner(sealSignerList);
        return ResponseResult.getSuccessResult(sealAntiFake);
    }

    /**
     * 校验pdf数字签名并返回相关信息
     * @param pdfPath pdf路径
     * @return 相关信息
     */
    @ResponseBody
    @GetMapping("api/getPdfSignDetail")
    public ResponseResult<Object> getPdfSignDetail(@RequestParam String pdfPath){
        PdfSignInfo pdfSignInfo = getSignFromPdf(pdfPath);
        return ResponseResult.getSuccessResult(pdfSignInfo);
    }
}
