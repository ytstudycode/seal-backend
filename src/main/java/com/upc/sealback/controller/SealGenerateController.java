package com.upc.sealback.controller;

import com.upc.sealback.bean.PrivateSeal.PrivateSealDTO;
import com.upc.sealback.bean.PublicSeal.PublicSealDTO;
import com.upc.sealback.bean.SealAntiFake;
import com.upc.sealback.bean.SealDetail;
import com.upc.sealback.bean.SealSign;
import com.upc.sealback.config.ResponseResult;
import com.upc.sealback.service.*;
import com.upc.sealback.utils.BlindWatermark.BlindWatermark;
import com.upc.sealback.utils.BlindWatermark.Watermark;
import com.upc.sealback.utils.FileOSS;
import com.upc.sealback.utils.OCR_mix;
import com.upc.sealback.utils.PrivateSeal.GeneratePrivateSeal;
import com.upc.sealback.utils.PrivateSeal.rule.PrivateSealBuilder;
import com.upc.sealback.utils.PublicSeal.GeneratePublicSeal;
import com.upc.sealback.utils.PublicSeal.rule.PublicSealBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.DigestUtils;
import org.springframework.web.bind.annotation.*;

import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.*;

import static com.upc.sealback.utils.FileOSS.streamUploadOSS;
import static com.upc.sealback.utils.FileUtils.deleteFile;
import static com.upc.sealback.utils.FontUtils.SEAL_SAVE_PATH;

/**
 * @description 生成印章控制类
 * @create zymac
 **/
@Controller
public class SealGenerateController {
    @Autowired
    SealConService sealConService;
    @Autowired
    SealDetailService sealDetailService;
    @Autowired
    SealUserService sealUserService;
    @Autowired
    SealAntiFakeService sealAntiFakeService;
    @Autowired
    SealSignService sealSignService;

    /**
     * 生成企业印章模板
     * @return 存储印章的oss地址
     * @throws Exception
     */
    @ResponseBody
    @PostMapping("api/getPublicSeal")
    public ResponseResult<Object> getPublicSeal(@RequestBody SealDetail sealDetail) throws Exception {
        ResponseResult<Object> responseResult = GeneratePublicSeal.generatePublicSeal(sealDetail);
        Map<String,Object> map = (Map<String, Object>) responseResult.getResult();
        PublicSealBuilder psb = (PublicSealBuilder) map.get("psb");
        PublicSealDTO dto = (PublicSealDTO) map.get("dto");
        InputStream img = psb.handleToStream(dto);
        return streamUploadOSS(img);
    }

    /**
     * 生成私人印章模板
     * @param sealDetail 印章样式
     * @return 存储印章的oss地址
     * @throws Exception
     */
    @ResponseBody
    @PostMapping("api/getPrivateSeal")
    public ResponseResult<Object> getPrivateSeal(@RequestBody SealDetail sealDetail) throws Exception {
        ResponseResult<Object> responseResult = GeneratePrivateSeal.generatePrivateSeal(sealDetail);
        Map<String,Object> map = (Map<String, Object>) responseResult.getResult();
        PrivateSealBuilder psb = (PrivateSealBuilder) map.get("psb");
        PrivateSealDTO dto = (PrivateSealDTO) map.get("dto");
        InputStream img = psb.handleToStream(dto);
        return streamUploadOSS(img);
    }


    /**
     * 在oss中删除在预览时无用的图片
     * @param fileArray 图片集
     * @return
     */
    @ResponseBody
    @DeleteMapping("api/deleteFileArray")
    public ResponseResult<Object> deleteFileArray(@RequestParam String fileArray) throws Exception {
        List<String> fileList = List.of(fileArray.split(","));
        ResponseResult<Object> responseResult = FileOSS.delOSSFileArray(fileList);
        return responseResult;
    }
}
