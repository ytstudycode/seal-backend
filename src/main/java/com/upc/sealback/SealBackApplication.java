package com.upc.sealback;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SealBackApplication {

    public static void main(String[] args) {
        SpringApplication.run(SealBackApplication.class, args);
    }

}
