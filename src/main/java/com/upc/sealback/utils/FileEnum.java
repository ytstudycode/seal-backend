package com.upc.sealback.utils;

/**
 * @description 文件类型枚举
 */

public enum FileEnum {

    DOCX(".docx"),

    DOC(".DOC"),

    PDF(".pdf"),

    PNG(".png"),

    PFX(".pfx"),

    OFD(".ofd"),
    ;
    private String prefix;

    FileEnum(String preFix){
        this.prefix = preFix;
    }

    public String getPrefix() { return prefix; }

    public void setPrefix(String prefix) { this.prefix = prefix; }
}
