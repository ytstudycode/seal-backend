package com.upc.sealback.utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.upc.sealback.bean.SealUser;
import com.upc.sealback.config.ResponseResult;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.Map;

/**
 * 实名认证工具类
 */
public class Authentication {
    public static final String APP_CODE="";
    public static final String APP_SECRET="";

    public static ResponseResult<Object> Certificate(@NotNull SealUser sealUser){
        String host = "";
        String path = "";
        String method = "GET";
        String appcode = APP_CODE;
        Map<String, String> headers = new HashMap<String, String>();
        //最后在header中的格式(中间是英文空格)为Authorization:APPCODE 83359fd73fe94948385f570e3c139105
        headers.put("Authorization", "APPCODE " + appcode);
        Map<String, String> querys = new HashMap<String, String>();
        querys.put("certcode", sealUser.getUserIdCard());
        querys.put("mobile", sealUser.getUserId());
        querys.put("name", sealUser.getUserName());

        try {
            HttpResponse response = HttpUtils.doGet(host, path, method, headers, querys);
            //获取response的body
            String result = EntityUtils.toString(response.getEntity(),"utf-8");
            JSONObject jsonObject =  JSON.parseObject(result);
            Integer code= (Integer) jsonObject.get("code");
            String message = (String) jsonObject.get("message");
            if(code == 0){
                return ResponseResult.getSuccessResult("success");
            }else {
                return ResponseResult.getErrorResult(code.toString(),message);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseResult.getErrorResult("400",e.getMessage());
        }
    }
}
