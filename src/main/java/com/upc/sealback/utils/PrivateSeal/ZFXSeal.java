package com.upc.sealback.utils.PrivateSeal;


import com.upc.sealback.bean.PrivateSeal.PrivateSealDTO;
import com.upc.sealback.utils.PrivateSeal.reponsitory.ZFXRepository;
import com.upc.sealback.utils.PrivateSeal.rule.PrivateSealBuilder;
import com.upc.sealback.utils.ByteUtils;
import com.upc.sealback.utils.FileUtils;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;

import static com.upc.sealback.utils.FontUtils.SEAL_SAVE_PATH;
import static com.upc.sealback.utils.NumUtils.toFloat;

/**
 * @description 画正方形印章
 */
public class ZFXSeal extends PrivateSealBuilder {

    public final static int fixH = 18;
    public final static int fixW = 2;
    public static final int lineSize = 14;
    private static final int imageSize = 500;

    @Override
    public String handleToBase64(PrivateSealDTO dto) throws Exception {
        BufferedImage bi = toBufferedImage(dto, lineSize, imageSize, fixH, fixW);
        byte[] bs = ByteUtils.buildBytes(bi);
        String fullPath = ByteUtils.storeBytes(bs,SEAL_SAVE_PATH);
        String base64 = FileUtils.fileToBase64(new File(fullPath));
        return base64;
    }

    @Override
    public String handleToLocal(PrivateSealDTO dto) throws Exception {
        BufferedImage bi = toBufferedImage(dto, lineSize, imageSize, fixH, fixW);
        byte[] bs = ByteUtils.buildBytes(bi);
        String fullPath = ByteUtils.storeBytes(bs,SEAL_SAVE_PATH);
        return fullPath;
    }

    @Override
    public InputStream handleToStream(PrivateSealDTO dto) throws Exception{
        BufferedImage bi = toBufferedImage(dto, lineSize, imageSize, fixH, fixW);
        byte[] bs = ByteUtils.buildBytes(bi);
        return new ByteArrayInputStream(bs);
    }


    private BufferedImage toBufferedImage(PrivateSealDTO font, int lineSize, int imageSize, int fixH, int fixW) throws Exception {
        //font.setFontContent(font.getFontContent()+(font.getFontContent().length() == 2 ? "之印" : "印"));
        BufferedImage bi = null;
        if (font != null && font.getFontContent().length() >= 2 && font.getFontContent().length() <= 6) {
             bi = new BufferedImage(imageSize, imageSize / 2, 6);
        } else {
            throw new Exception("FontText.length illegal!");
        }
        Graphics2D g2d = bi.createGraphics();
        //设置不透明度
        double opacity = toFloat(font.getColorOpacity() ,100);
        int alpha = (int) Math.round(opacity * 255);
        Color color = font.getColorTag() == 1 ?
                new Color(254,0,0,alpha) : (font.getColorTag() == 2 ?
                new  Color(0,0,0,alpha) : new Color(0,0,254,alpha));
        g2d.setPaint(color);
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        if (font.getFontContent().length() == 2) {
            bi = ZFXRepository.drawTwoFont(bi, g2d, font.setFontContent(font.getFontContent() ), lineSize, imageSize, fixH, fixW, color);
        } else if (font.getFontContent().length() == 3) {
            if(font.getFontStyle() == 3 || font.getFontStyle() == 4){
                bi = ZFXRepository.drawThreeFontLeft(bi, g2d,font.setFontContent(font.getFontContent()), lineSize, imageSize, fixH, fixW, color);
            }else {
                bi = ZFXRepository.drawThreeFontRight(bi, g2d,font.setFontContent(font.getFontContent()), lineSize, imageSize, fixH, fixW, color);
            }
        } else if(font.getFontContent().length() == 4){
            bi = ZFXRepository.drawFourFont(bi, font, lineSize, imageSize, fixH, fixW, color);
        }else if(font.getFontContent().length() == 5){
            if(font.getFontStyle() == 3 || font.getFontStyle() == 4){
                bi = ZFXRepository.drawFiveFontLeft(bi, font, lineSize, imageSize, fixH, fixW, color);
            }else {
                bi = ZFXRepository.drawFiveFontRight(bi, font, lineSize, imageSize, fixH, fixW, color);
            }

        } else if(font.getFontContent().length() == 6){
            bi = ZFXRepository.drawSixFont(bi, font, lineSize, imageSize, fixH, fixW, color);
        }
        return bi;
    }

}
