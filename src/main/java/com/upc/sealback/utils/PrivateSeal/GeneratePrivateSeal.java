package com.upc.sealback.utils.PrivateSeal;

import com.upc.sealback.bean.PrivateSeal.PrivateSealDTO;
import com.upc.sealback.bean.SealDetail;
import com.upc.sealback.config.ResponseResult;
import com.upc.sealback.utils.PrivateSeal.rule.PrivateSealBuilder;
import org.jetbrains.annotations.NotNull;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import static com.upc.sealback.utils.FileOSS.streamUploadOSS;
import static com.upc.sealback.utils.FontUtils.Font_KaiTi;

/**
 * @description 创建私人印章工具类
 * @create zymac
 * @date 2022/4/4 16:58
 **/
public class GeneratePrivateSeal {
    /**
     * 生成私人印章的方法
     * @param sealDetail
     * @return 私人印章的oss地址
     * @throws Exception
     */
    public static ResponseResult<Object> generatePrivateSeal(@NotNull SealDetail sealDetail) throws Exception {
        if(sealDetail.getSealRule() == 2) {
            sealDetail.setSealContent(sealDetail.getSealContent()+"印");}
        else if(sealDetail.getSealRule() == 3) {
            sealDetail.setSealContent(sealDetail.getSealContent()+"之印");}
        PrivateSealDTO dto = new PrivateSealDTO()
                .setColorTag(sealDetail.getSealColor())
                .setFontContent(sealDetail.getSealContent())
                .setFontStyle(sealDetail.getSealStyle())
                .setFontSize(80)
                .setFontFamily(Font_KaiTi)
                .setFontSpace(3.0)
                .setColorOpacity(sealDetail.getSealOpacity());
        PrivateSealBuilder psb = null;
        if(sealDetail.getSealStyle() == 1 || sealDetail.getSealStyle() == 2) {psb = new CFXSeal();}
        else { psb = new ZFXSeal();}
        Map<String,Object> map = new HashMap<String,Object>();
        map.put("psb",psb);
        map.put("dto",dto);
//        InputStream img = psb.handleToStream(dto);
//        ResponseResult<Object> responseResult = streamUploadOSS(img);
        return ResponseResult.getSuccessResult(map);
    }
}
