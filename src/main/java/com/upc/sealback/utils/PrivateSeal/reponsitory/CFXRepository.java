package com.upc.sealback.utils.PrivateSeal.reponsitory;

import com.upc.sealback.bean.PrivateSeal.PrivateSealDTO;

import java.awt.*;
import java.awt.font.FontRenderContext;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;

/**
 * @description 画长方形印章
 */

public class CFXRepository {

    public final static int INIT_BEGIN = 10;

    /**
     * 画长方形2字
     *
     * @param bi
     * @param g2d
     * @param font
     * @param lineSize
     * @param imageSize
     * @param fixH
     * @param fixW
     * @param color
     * @return
     */
    public static BufferedImage drawTwoFont(BufferedImage bi, Graphics2D g2d, PrivateSealDTO font, int lineSize,
                                            int imageSize, int fixH, int fixW, Color color) {
        fixH -= 9;
        int marginW = fixW + lineSize;

        //设置字体
        Font f = new Font(font.getFontFamily(), Font.BOLD, font.getFontSize());
        g2d.setFont(f);
        FontRenderContext context = g2d.getFontRenderContext();
        //根据第一个字，画出一个框
        Rectangle2D rectangle = f.getStringBounds(font.getFontContent().substring(0, 1), context);
        //两个字+中间空格的长度
        double fontWidth = rectangle.getWidth() * 2 + (font.getFontSpace() == null ? INIT_BEGIN : font.getFontSpace());
        //字体的高度
        double fontHeight = rectangle.getHeight();
        //拉伸
        BufferedImage nbi = new BufferedImage(imageSize, imageSize, bi.getType());
        Graphics2D ng2d = nbi.createGraphics();
        ng2d.setPaint(color);
        ng2d.drawImage(bi, 0, 0, imageSize, imageSize, null);

        //画长方形,如果选择样式为1，则带边框，为2则不带
        if(font.getFontStyle() == 1){
            ng2d.setStroke(new BasicStroke(8));
            int width = (int) fontWidth + 20;//框的长
            int height = (int) fontHeight + 10;//框的高
            int x = (imageSize - width) / 2; //框距离左右的位置
            int y = (imageSize - height) / 2; //框距离上下的位置
            ng2d.drawRect(x, y, width, height);
            ng2d.dispose();
        }
        bi = nbi;

        g2d = bi.createGraphics();
        g2d.setPaint(color);
        g2d.setFont(f);
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        //开始画字
        //字体最下方到上端的距离
        int marginH = (imageSize) / 2 + (int) Math.abs(rectangle.getCenterY()) - 4;
        //画第一个字
        marginW = (imageSize - (int) fontWidth) / 2; //第一个字到左侧边缘线的距离
        //g2d.drawString 从左上角开始画
        g2d.drawString(font.getFontContent().substring(0, 1), marginW, marginH);
        //rectangle.getCenterX() 第一个字中心到该字体所形成的的框两侧的长度
        //Math.abs(rectangle.getCenterX()) * 2 + (font.getFontSpace() == null ? INIT_BEGIN : font.getFontSpace()); 获取第一个文字+中间空格的长度
        marginW += Math.abs(rectangle.getCenterX()) * 2 + (font.getFontSpace() == null ? INIT_BEGIN : font.getFontSpace());
        //画第二个字
        g2d.drawString(font.getFontContent().substring(1, 2), marginW, marginH);

        return bi;
    }

    /**
     * 画长方形3字
     *
     * @param bi
     * @param g2d
     * @param font
     * @param lineSize
     * @param imageSize
     * @param fixH
     * @param fixW
     * @param color
     * @return
     */
    public static BufferedImage drawThreeFont(BufferedImage bi, Graphics2D g2d, PrivateSealDTO font, int lineSize,
                                              int imageSize, int fixH, int fixW, Color color) {
        fixH -= 9;
        int marginW = fixW + lineSize;
        //设置字体
        Font f = new Font(font.getFontFamily(), Font.BOLD, 80);
        g2d.setFont(f);
        FontRenderContext context = g2d.getFontRenderContext();
        //根据第一个字，画出一个框
        Rectangle2D rectangle = f.getStringBounds(font.getFontContent().substring(0, 1), context);
        //三个字+中间空格的长度
        double fontWidth = rectangle.getWidth() * 3 + (font.getFontSpace() == null ? INIT_BEGIN : font.getFontSpace()) * 2;
        //字体的高度
        double fontHeight = rectangle.getHeight();
        //拉伸
        BufferedImage nbi = new BufferedImage(imageSize, imageSize, bi.getType());
        Graphics2D ng2d = nbi.createGraphics();
        ng2d.setPaint(color);
        ng2d.drawImage(bi, 0, 0, imageSize, imageSize, null);

        //画长方形,如果选择样式为1，则带边框，为2则不带
        if(font.getFontStyle() == 1) {
            ng2d.setStroke(new BasicStroke(8));
            int width = (int) fontWidth + 20;//框的长
            int height = (int) fontHeight + 10;//框的高
            int x = (imageSize - width) / 2; //框距离左右的位置
            int y = (imageSize - height) / 2; //框距离上下的位置
            ng2d.drawRect(x, y, width, height);
            ng2d.dispose();
        }
        bi = nbi;

        g2d = bi.createGraphics();
        g2d.setPaint(color);
        g2d.setFont(f);
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        //开始画字

        //字体最下方到上端的距离
        int marginH = (imageSize) / 2 + (int) Math.abs(rectangle.getCenterY()) - 4;
        marginW = (imageSize - (int) fontWidth) / 2; //第一个字到左侧边缘线的距离
        g2d.drawString(font.getFontContent().substring(0, 1), marginW, marginH);
        marginW += Math.abs(rectangle.getCenterX()) * 2 + (font.getFontSpace() == null ? INIT_BEGIN : font.getFontSpace());
        g2d.drawString(font.getFontContent().substring(1, 2), marginW, marginH);
        marginW += Math.abs(rectangle.getCenterX()) * 2 + (font.getFontSpace() == null ? INIT_BEGIN : font.getFontSpace());
        g2d.drawString(font.getFontContent().substring(2, 3), marginW, marginH);
        /*marginW += Math.abs(rectangle.getCenterX()) * 2 + (font.getFontSpace() == null ? INIT_BEGIN : font.getFontSpace());
        g2d.drawString(font.getFontText().substring(3, 4), marginW, marginH);*/
        return bi;
    }

    /**
     * 画长方形4字
     *
     * @param bi
     * @param g2d
     * @param font
     * @param lineSize
     * @param imageSize
     * @param fixH
     * @param fixW
     * @param color
     * @return
     */
    public static BufferedImage drawFourFont(BufferedImage bi, Graphics2D g2d, PrivateSealDTO font, int lineSize,
                                             int imageSize, int fixH, int fixW, Color color) {
        //fixH -= 9;
        int marginW = fixW + lineSize;
        //设置字体
        int length = font.getFontContent().length();
        int fontSize = 60;
        float marginH = 160;
        Font f = null;
        FontRenderContext context = null;
        Rectangle2D rectangle = null;
        //n个字+中间空格的长度
        double fontWidth = 0;
        //字体的高度
        double fontHeight = 0;

        switch (length) {
            case 4:
                f = new Font(font.getFontFamily(), Font.BOLD, fontSize);
                g2d.setFont(f);
                context = g2d.getFontRenderContext();
                rectangle = f.getStringBounds(font.getFontContent().substring(0, 1), context);
                //n个字+中间空格的长度
                fontWidth = (Math.abs(rectangle.getCenterX()) * 1.8)*length + (font.getFontSpace() == null ? INIT_BEGIN : font.getFontSpace()) * (length-1);
                //字体的高度
                fontHeight = rectangle.getHeight();
                //拉伸
                BufferedImage nbi4 = new BufferedImage(imageSize, imageSize, bi.getType());
                Graphics2D ng2d4 = nbi4.createGraphics();
                ng2d4.setPaint(color);
                ng2d4.drawImage(bi, 0, 0, imageSize, imageSize, null);
                //画长方形
                //画长方形,如果选择样式为1，则带边框，为2则不带
                if(font.getFontStyle() == 1) {
                    ng2d4.setStroke(new BasicStroke(8));
                    int tempWidth = (int) fontWidth + 20;
                    int width = Math.min(tempWidth, 280);//框的长
                    int height = (int) fontHeight + 20;//框的高
                    int x = (imageSize - width) / 2; //框距离左右的位置
                    int y = (imageSize - height) / 2; //框距离上下的位置
                    ng2d4.drawRect(x, y, width, height);
                    ng2d4.dispose();
                }
                bi = nbi4;
                break;
            case 5:
                marginH = 158;
                f = new Font(font.getFontFamily(), Font.BOLD, 43);
                g2d.setFont(f);
                context = g2d.getFontRenderContext();
                rectangle = f.getStringBounds(font.getFontContent().substring(0, 1), context);
                //n个字+中间空格的长度
                fontWidth = (Math.abs(rectangle.getCenterX()) * 1.8)*length + (font.getFontSpace() == null ? INIT_BEGIN : font.getFontSpace()) * (length-1);
                //字体的高度
                fontHeight = rectangle.getHeight();
                //拉伸
                BufferedImage nbi5 = new BufferedImage(imageSize, imageSize, bi.getType());
                Graphics2D ng2d5 = nbi5.createGraphics();
                ng2d5.setPaint(color);
                ng2d5.drawImage(bi, 0, 0, imageSize, imageSize, null);
                //画长方形
                if(font.getFontStyle() == 1) {
                    ng2d5.setStroke(new BasicStroke(8));
                    int tempWidth = (int) fontWidth + 20;
                    int width = Math.min(tempWidth, 280);//框的长
                    int height = (int) fontHeight + 20;//框的高
                    int x = (imageSize - width) / 2; //框距离左右的位置
                    int y = (imageSize - height) / 2; //框距离上下的位置
                    ng2d5.drawRect(x, y, width, height);
                    ng2d5.dispose();
                }
                bi = nbi5;
                break;
            case 6:
                marginH = 153;
                f = new Font(font.getFontFamily(), Font.BOLD, 34);
                g2d.setFont(f);
                context = g2d.getFontRenderContext();
                rectangle = f.getStringBounds(font.getFontContent().substring(0, 1), context);
                //n个字+中间空格的长度
                fontWidth = (Math.abs(rectangle.getCenterX()) * 1.8)*length + (font.getFontSpace() == null ? INIT_BEGIN : font.getFontSpace()) * (length-1);
                //字体的高度
                fontHeight = rectangle.getHeight();
                //拉伸
                BufferedImage nbi6 = new BufferedImage(imageSize, imageSize, bi.getType());
                Graphics2D ng2d6 = nbi6.createGraphics();
                ng2d6.setPaint(color);
                ng2d6.drawImage(bi, 0, 0, imageSize, imageSize, null);
                //画长方形
                if(font.getFontStyle() == 1) {
                    ng2d6.setStroke(new BasicStroke(8));
                    int tempWidth = (int) fontWidth + 20;
                    int width = Math.min(tempWidth, 280);//框的长
                    int height = (int) fontHeight + 20;//框的高
                    int x = (imageSize - width) / 2; //框距离左右的位置
                    int y = (imageSize - height) / 2; //框距离上下的位置
                    ng2d6.drawRect(x, y, width, height);
                    ng2d6.dispose();
                }
                bi = nbi6;
                break;

        }

        //拉伸
        g2d = bi.createGraphics();
        g2d.setPaint(color);
        g2d.setFont(f);
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        //开始画字

        //字体最下方到上端的距离
        marginH = (imageSize) / 2 + (int) Math.abs(rectangle.getCenterY()) - 4;
        marginW = (imageSize - (int) fontWidth) / 2 ; //第一个字到左侧边缘线的距离

        for (int i = 0; i < font.getFontContent().length(); i++) {
            int marginW2 = marginW;
            g2d.drawString(font.getFontContent().substring(i, i + 1), marginW2, marginH);
            marginW2 += Math.abs(rectangle.getCenterX()) * 1.8 + (font.getFontSpace() == null ? INIT_BEGIN : font.getFontSpace());
            marginW = marginW2;
        }

        return bi;
    }
}
