package com.upc.sealback.utils.PrivateSeal.reponsitory;



import com.upc.sealback.bean.PrivateSeal.PrivateSealDTO;

import java.awt.*;
import java.awt.font.FontRenderContext;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;

/**
 * @description 画正方形印章
 */

public class ZFXRepository {

    public static final int INIT_BEGIN = 10;


    /**
     * 画二字
     * @param bi
     * @param g2d
     * @param font
     * @param lineSize
     * @param imageSize
     * @param fixH
     * @param fixW
     * @param color
     * @return
     */
    public static BufferedImage drawTwoFont(BufferedImage bi, Graphics2D g2d, PrivateSealDTO font, int lineSize, int imageSize, int fixH, int fixW, Color color) {
        fixH -= 2;
        imageSize = 500;
        int marginW = fixW + lineSize;
        Font f = new Font(font.getFontFamily(), 1, 90);
        g2d.setFont(f);
        FontRenderContext context = g2d.getFontRenderContext();
        Rectangle2D rectangle = f.getStringBounds(font.getFontContent().substring(0, 1), context);

        int oldW = marginW;
        BufferedImage nbi = new BufferedImage(imageSize, imageSize, bi.getType());

            Graphics2D ng2d = nbi.createGraphics();
            ng2d.setPaint(color);
            ng2d.drawImage(bi, 0, 0, imageSize, imageSize, (ImageObserver) null);
        if(font.getFontStyle() == 3 || font.getFontStyle() == 5) {
            ng2d.setStroke(new BasicStroke((float) lineSize));
            ng2d.drawRect(135, 135, 230, 230);
            ng2d.dispose();
        }
        g2d = nbi.createGraphics();
        g2d.setPaint(color);
        g2d.setFont(f);
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        //两个字+中间空格的长度
        double fontWidth = rectangle.getWidth();
        //字体的高度
        double fontHeight = rectangle.getHeight();
        //画第一个字
        marginW = (imageSize - (int) fontWidth) / 2; //第一个字到左侧边缘线的距离
        //第一行字的底端距离顶部的距离
        float marginH = (float)(Math.abs(rectangle.getCenterY()) * 2.0D ) + (float)fixH + 150;
        //marginW = (int)((double)marginW + Math.abs(rectangle.getCenterX()) * 2.0D + (font.getFontSpace() == null ? 10.0D : font.getFontSpace()));
        g2d.drawString(font.getFontContent().substring(0, 1), (float)marginW, marginH);
        marginH = (float)((double)marginH + Math.abs(rectangle.getHeight()));
        /*g2d.drawString(font.getFontContent().substring(3, 4), (float)oldW, marginH);*/
        g2d.drawString(font.getFontContent().substring(1, 2), (float)marginW, marginH);
        return nbi;
    }


    /**
     * 画三字-大字在左
     * @param bi 图片
     * @param g2d 原画笔
     * @param font 字体对象
     * @param lineSize 线宽
     * @param imageSize 图片尺寸
     * @param fixH 修复高
     * @param fixW 修复宽
     * @param color
     * @return
     */
    public static BufferedImage drawThreeFontLeft(BufferedImage bi, Graphics2D g2d, PrivateSealDTO font, int lineSize, int imageSize, int fixH, int fixW, Color color) {
        fixH += 10;
        int marginW = fixW + lineSize;

        Font f1 = new Font("STKaitiSC-Bold", 1, 90);
        g2d.setFont(f1);
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        FontRenderContext context1 = g2d.getFontRenderContext();
        Rectangle2D rectangle1 = f1.getStringBounds(font.getFontContent().substring(2, 3), context1);

        float marginH = (float)(Math.abs(rectangle1.getCenterY()) * 2.0D + (double)marginW) + (float)fixH - 24 ;
        int oldW = marginW + 145;
        g2d.drawString(font.getFontContent().substring(2, 3), (float)oldW, marginH + 65);

        Font f = new Font(font.getFontFamily(), 1, 93);
        g2d.setFont(f);
        FontRenderContext context = g2d.getFontRenderContext();
        Rectangle2D rectangle = f.getStringBounds(font.getFontContent().substring(0, 1), context);

        BufferedImage nbi = new BufferedImage(imageSize, imageSize, bi.getType());

        Graphics2D ng2d = nbi.createGraphics();
        ng2d.setPaint(color);
        ng2d.drawImage(bi, 0, 0, imageSize, imageSize, (ImageObserver) null);
        if(font.getFontStyle() == 3 || font.getFontStyle() == 5) {
            ng2d.setStroke(new BasicStroke((float) lineSize));
            ng2d.drawRect(135, 135, 230, 230);
            ng2d.dispose();
        }

        g2d = nbi.createGraphics();
        g2d.setPaint(color);
        g2d.setFont(f);
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        //g2d.drawString(font.getFontContent().substring(2, 3), (float)marginW, marginH);
        //右侧字距左侧边缘的距离
        marginW = 365- (int)((double)marginW + Math.abs(rectangle.getCenterX()) * 2.0D );
        //第一行字的底端距离顶部的距离
        marginH = (float)(Math.abs(rectangle.getCenterY()) * 2.0D ) + (float)fixH + 132;
        g2d.drawString(font.getFontContent().substring(0, 1), (float)marginW, marginH);
        // 第二个字
        marginH = (float)((double)marginH + Math.abs(rectangle.getHeight()));
        g2d.drawString(font.getFontContent().substring(1, 2), (float)marginW, marginH);

        return nbi;
    }

    /**
     * 画三字-大字在右
     * @param bi 图片
     * @param g2d 原画笔
     * @param font 字体对象
     * @param lineSize 线宽
     * @param imageSize 图片尺寸
     * @param fixH 修复高
     * @param fixW 修复宽
     * @param color
     * @return
     */
    public static BufferedImage drawThreeFontRight(BufferedImage bi, Graphics2D g2d, PrivateSealDTO font, int lineSize, int imageSize, int fixH, int fixW, Color color) {
        fixH += 10;
        int marginW = fixW + lineSize;

        Font f1 = new Font("STKaitiSC-Bold", 1, 90);
        g2d.setFont(f1);
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        FontRenderContext context1 = g2d.getFontRenderContext();
        Rectangle2D rectangle1 = f1.getStringBounds(font.getFontContent().substring(0, 1), context1);

        float marginH = (float)(Math.abs(rectangle1.getCenterY()) * 2.0D + (double)marginW) + (float)fixH - 24;
        int oldW = marginW + 145;
        marginW = imageSize- (int)((double)marginW + Math.abs(rectangle1.getCenterX()) * 2.0D ) - 135;
        g2d.drawString(font.getFontContent().substring(0, 1), (float)marginW, marginH+65);

        Font f = new Font(font.getFontFamily(), 1, 93);
        g2d.setFont(f);
        FontRenderContext context = g2d.getFontRenderContext();
        Rectangle2D rectangle = f.getStringBounds(font.getFontContent().substring(0, 1), context);

        BufferedImage nbi = new BufferedImage(imageSize, imageSize, bi.getType());

        Graphics2D ng2d = nbi.createGraphics();
        ng2d.setPaint(color);
        ng2d.drawImage(bi, 0, 0, imageSize, imageSize, (ImageObserver) null);
        if(font.getFontStyle() == 3 || font.getFontStyle() == 5) {
            ng2d.setStroke(new BasicStroke((float) lineSize));
            ng2d.drawRect(135, 135, 230, 230);
            ng2d.dispose();
        }

        g2d = nbi.createGraphics();
        g2d.setPaint(color);
        g2d.setFont(f);
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        //g2d.drawString(font.getFontContent().substring(2, 3), (float)marginW, marginH);
        //右侧字距左侧边缘的距离
        marginW = imageSize- (int)((double)marginW + Math.abs(rectangle.getCenterX()) * 2.0D );
        //第一行字的底端距离顶部的距离
        marginH = (float)(Math.abs(rectangle.getCenterY()) * 2.0D ) + (float)fixH + 132;
        g2d.drawString(font.getFontContent().substring(1, 2), (float)oldW, marginH);
        // 第二个字
        marginH = (float)((double)marginH + Math.abs(rectangle.getHeight()));
        g2d.drawString(font.getFontContent().substring(2, 3), (float)oldW, marginH);

        return nbi;
    }

    /**
     * 画四字
     * @param bi
     * @param font
     * @param lineSize
     * @param imageSize
     * @param fixH
     * @param fixW
     * @param color
     * @return
     */
    public static BufferedImage drawFourFont(BufferedImage bi, PrivateSealDTO font, int lineSize, int imageSize, int fixH, int fixW, Color color) {
        fixH += 2;
        int marginW = fixW + lineSize;
        BufferedImage nbi = new BufferedImage(imageSize, imageSize, bi.getType());

        //画框
        Graphics2D ng2d = nbi.createGraphics();
        ng2d.setPaint(color);
        ng2d.drawImage(bi, 0, 0, imageSize, imageSize, (ImageObserver)null);
        if(font.getFontStyle() == 3 || font.getFontStyle() == 5){
            ng2d.setStroke(new BasicStroke(20));
            ng2d.drawRect(135, 135, 230, 230);
            ng2d.dispose();
        }
        Graphics2D g2d = nbi.createGraphics();
        g2d.setPaint(color);
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        FontRenderContext context = g2d.getFontRenderContext();
        Font f = new Font(font.getFontFamily(), 1, 100);
        g2d.setFont(f);

        //开始画字
        Rectangle2D rectangle = f.getStringBounds(font.getFontContent().substring(0, 1), context);
        //第一行字的底端距离顶部的距离
        float marginH = (float)(Math.abs(rectangle.getCenterY()) * 2.0D ) + (float)fixH + 135;
        double fontWidth = rectangle.getWidth() * 2 + (font.getFontSpace() == null ? INIT_BEGIN : font.getFontSpace());
        marginW = (imageSize - (int) fontWidth) / 2; //第一个字到左侧边缘线的距离
        //marginW += Math.abs(rectangle.getCenterX()) * 2 + (font.getFontSpace() == null ? INIT_BEGIN : font.getFontSpace());
        g2d.drawString(font.getFontContent().substring(2, 3), (float)marginW, marginH);
        int oldW = marginW;
        marginW = (int)((double)marginW + Math.abs(rectangle.getCenterX()) * 2.0D + (font.getFontSpace() == null ? 10.0D : font.getFontSpace()));
        g2d.drawString(font.getFontContent().substring(0, 1), (float)marginW, marginH);
        marginH = (float)((double)marginH + Math.abs(rectangle.getHeight()));
        g2d.drawString(font.getFontContent().substring(3, 4), (float)oldW, marginH);
        g2d.drawString(font.getFontContent().substring(1, 2), (float)marginW, marginH);
        return nbi;
    }

    /**
     * 画五字-两字在左
     * @param bi
     * @param font
     * @param lineSize
     * @param imageSize
     * @param fixH
     * @param fixW
     * @param color
     * @return
     */
    public static BufferedImage drawFiveFontLeft(BufferedImage bi, PrivateSealDTO font, int lineSize, int imageSize, int fixH, int fixW, Color color) {
        fixH += 2;
        int marginW = fixW + lineSize;
        BufferedImage nbi = new BufferedImage(imageSize, imageSize, bi.getType());
        //画框
        Graphics2D ng2d = nbi.createGraphics();
        ng2d.setPaint(color);
        ng2d.drawImage(bi, 0, 0, imageSize, imageSize, (ImageObserver)null);
        if(font.getFontStyle() == 3 || font.getFontStyle() == 5){
            ng2d.setStroke(new BasicStroke(20));
            ng2d.drawRect(135, 135, 230, 230);
            ng2d.dispose();
        }
        Graphics2D g2d = nbi.createGraphics();
        g2d.setPaint(color);
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        FontRenderContext context = g2d.getFontRenderContext();
        Font f = new Font(font.getFontFamily(), 1, 70);
        g2d.setFont(f);

        //开始画字
        Rectangle2D rectangle = f.getStringBounds(font.getFontContent().substring(0, 1), context);
        //第一行字的底端距离顶部的距离
        float marginH = (float)(Math.abs(rectangle.getCenterY()) * 2.0D ) + (float)fixH - 2 + 135;
        double fontWidth = rectangle.getWidth() * 2 + (font.getFontSpace() == null ? INIT_BEGIN : font.getFontSpace());

        //marginW += Math.abs(rectangle.getCenterX()) * 2 + (font.getFontSpace() == null ? INIT_BEGIN : font.getFontSpace());

        //右侧三个字（一列）
        marginW = 2 * (imageSize - (int) fontWidth) / 3 - 45; //第一个字到左侧边缘线的距离
        int oldW = marginW;
        marginW = (int)((double)marginW + Math.abs(rectangle.getCenterX()) * 2.0D + (font.getFontSpace() == null ? 10.0D : font.getFontSpace()));
        g2d.drawString(font.getFontContent().substring(0, 1), (float)marginW, marginH);
        float marginH1 = (float)((double)marginH + Math.abs(rectangle.getHeight())) - 2;
        g2d.drawString(font.getFontContent().substring(1, 2), (float)marginW, marginH1);
       // g2d.drawString(font.getFontContent().substring(4, 5), (float)oldW, marginH);
        float marginH2 = (float)((double)marginH1 + Math.abs(rectangle.getHeight())) - 2;
        g2d.drawString(font.getFontContent().substring(2, 3), (float)marginW, marginH2);


        Font f1 = new Font(font.getFontFamily(), 1, 90);
        g2d.setFont(f1);
        Rectangle2D rectangle1 = f1.getStringBounds(font.getFontContent().substring(3, 4), context);
        marginH1 = (float)(Math.abs(rectangle1.getCenterY()) * 2.0D ) + (float) (imageSize - 4 * Math.abs(rectangle1.getCenterY()))/3 + 40;
        g2d.drawString(font.getFontContent().substring(3, 4), (float)oldW - 30, marginH1);
        marginH2 = (float)((double)marginH1 + Math.abs(rectangle.getHeight())) + 10;
        g2d.drawString(font.getFontContent().substring(4, 5), (float)oldW - 30, marginH2);
        return nbi;
    }


    /**
     * 画五字-两字在右
     * @param bi
     * @param font
     * @param lineSize
     * @param imageSize
     * @param fixH
     * @param fixW
     * @param color
     * @return
     */
    public static BufferedImage drawFiveFontRight(BufferedImage bi, PrivateSealDTO font, int lineSize, int imageSize, int fixH, int fixW, Color color) {
        fixH += 2;
        int marginW = fixW + lineSize;
        BufferedImage nbi = new BufferedImage(imageSize, imageSize, bi.getType());
        //画框
        Graphics2D ng2d = nbi.createGraphics();
        ng2d.setPaint(color);
        ng2d.drawImage(bi, 0, 0, imageSize, imageSize, (ImageObserver)null);
        if(font.getFontStyle() == 3 || font.getFontStyle() == 5){
            ng2d.setStroke(new BasicStroke(20));
            ng2d.drawRect(135, 135, 230, 230);
            ng2d.dispose();
        }
        Graphics2D g2d = nbi.createGraphics();
        g2d.setPaint(color);
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        FontRenderContext context = g2d.getFontRenderContext();
        Font f = new Font(font.getFontFamily(), 1, 70);
        g2d.setFont(f);

        //开始画字
        Rectangle2D rectangle = f.getStringBounds(font.getFontContent().substring(0, 1), context);
        //第一行字的底端距离顶部的距离
        float marginH = (float)(Math.abs(rectangle.getCenterY()) * 2.0D ) + (float)fixH - 2 + 135;
        double fontWidth = rectangle.getWidth() * 2 + (font.getFontSpace() == null ? INIT_BEGIN : font.getFontSpace());

        //marginW += Math.abs(rectangle.getCenterX()) * 2 + (font.getFontSpace() == null ? INIT_BEGIN : font.getFontSpace());

        //右侧三个字（一列）
        marginW =  (imageSize - (int) fontWidth) / 2; //第一个字到左侧边缘线的距离
        int oldW = marginW - 10;
        marginW = (int)((double)marginW + Math.abs(rectangle.getCenterX()) * 2.0D + (font.getFontSpace() == null ? 10.0D : font.getFontSpace()));
        g2d.drawString(font.getFontContent().substring(2, 3), (float)oldW, marginH);
        float marginH1 = (float)((double)marginH + Math.abs(rectangle.getHeight())) - 2;
        g2d.drawString(font.getFontContent().substring(3, 4), (float)oldW, marginH1);
        // g2d.drawString(font.getFontContent().substring(4, 5), (float)oldW, marginH);
        float marginH2 = (float)((double)marginH1 + Math.abs(rectangle.getHeight())) - 2;
        g2d.drawString(font.getFontContent().substring(4, 5), (float)oldW, marginH2);


        Font f1 = new Font(font.getFontFamily(), 1, 90);
        g2d.setFont(f1);
        Rectangle2D rectangle1 = f1.getStringBounds(font.getFontContent().substring(3, 4), context);
        marginH1 = (float)(Math.abs(rectangle1.getCenterY()) * 2.0D ) + (float) (imageSize - 4 * Math.abs(rectangle1.getCenterY()))/3 + 45;
        g2d.drawString(font.getFontContent().substring(0, 1), (float)marginW - 5, marginH1);
        marginH2 = (float)((double)marginH1 + Math.abs(rectangle.getHeight())) + 10;
        g2d.drawString(font.getFontContent().substring(1, 2), (float)marginW - 5, marginH2);
        return nbi;
    }

    /**
     * 画六字
     * @param bi
     * @param font
     * @param lineSize
     * @param imageSize
     * @param fixH
     * @param fixW
     * @param color
     * @return
     */
    public static BufferedImage drawSixFont(BufferedImage bi, PrivateSealDTO font, int lineSize, int imageSize, int fixH, int fixW, Color color) {
        fixH += 2;
        int marginW = fixW + lineSize;
        BufferedImage nbi = new BufferedImage(imageSize, imageSize, bi.getType());
        //画框
        Graphics2D ng2d = nbi.createGraphics();
        ng2d.setPaint(color);
        ng2d.drawImage(bi, 0, 0, imageSize, imageSize, (ImageObserver)null);
        if(font.getFontStyle() == 3 || font.getFontStyle() == 5){
            ng2d.setStroke(new BasicStroke(20));
            ng2d.drawRect(135, 135, 230, 230);
            ng2d.dispose();
        }
        Graphics2D g2d = nbi.createGraphics();
        g2d.setPaint(color);
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        FontRenderContext context = g2d.getFontRenderContext();
        Font f = new Font(font.getFontFamily(), 1, 70);
        g2d.setFont(f);

        //开始画字
        Rectangle2D rectangle = f.getStringBounds(font.getFontContent().substring(0, 1), context);
        //第一行字的底端距离顶部的距离
        float marginH = (float)(Math.abs(rectangle.getCenterY()) * 2.0D ) + (float)fixH - 2 + 135;
        double fontWidth = rectangle.getWidth() * 2 + (font.getFontSpace() == null ? INIT_BEGIN : font.getFontSpace());

        //marginW += Math.abs(rectangle.getCenterX()) * 2 + (font.getFontSpace() == null ? INIT_BEGIN : font.getFontSpace());
        marginW = (230 - (int) fontWidth) / 3 ; //第一个字到左侧边缘线的距离
        //marginW = 2 * (imageSize - (int) fontWidth) / 3; //第一个字到左侧边缘线的距离
        int oldW = marginW + 135;
        marginW = (int)((double)marginW + Math.abs(rectangle.getCenterX()) * 2.0D + oldW);
        g2d.drawString(font.getFontContent().substring(0, 1), (float)marginW, marginH);
        g2d.drawString(font.getFontContent().substring(3, 4), (float)oldW, marginH);
        marginH = (float)((double)marginH + Math.abs(rectangle.getHeight())) - 2;
        g2d.drawString(font.getFontContent().substring(1, 2), (float)marginW, marginH);
        g2d.drawString(font.getFontContent().substring(4, 5), (float)oldW, marginH);
        marginH = (float)((double)marginH + Math.abs(rectangle.getHeight())) - 2;
        g2d.drawString(font.getFontContent().substring(2, 3), (float)marginW, marginH);
        g2d.drawString(font.getFontContent().substring(5, 6), (float)oldW, marginH);


        return nbi;
    }
}
