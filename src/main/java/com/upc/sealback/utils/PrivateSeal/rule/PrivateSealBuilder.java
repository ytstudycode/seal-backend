package com.upc.sealback.utils.PrivateSeal.rule;

import com.upc.sealback.bean.PrivateSeal.PrivateSealDTO;

import java.io.InputStream;

/**
 * @author mqz
 * @description
 * @abount https://github.com/DemoMeng
 * @since 2020/10/16
 */
public abstract class PrivateSealBuilder {


    /**
     * 处理并返回base64
     * @param dto
     * @return
     * @throws Exception
     */
    public abstract String handleToBase64(PrivateSealDTO dto) throws Exception;


    /**
     * 处理并返回图片本地地址
     * @param dto
     * @return
     * @throws Exception
     */
    public abstract String handleToLocal(PrivateSealDTO dto) throws Exception;

    /**
     * 生成字节数组
     * @param dto 印章设置
     * @return 字节数组
     * @throws Exception
     */
    public abstract InputStream handleToStream(PrivateSealDTO dto) throws Exception;

}
