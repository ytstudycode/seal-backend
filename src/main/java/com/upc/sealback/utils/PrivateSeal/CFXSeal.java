package com.upc.sealback.utils.PrivateSeal;

import com.upc.sealback.bean.PrivateSeal.PrivateSealDTO;
import com.upc.sealback.utils.PrivateSeal.reponsitory.CFXRepository;
import com.upc.sealback.utils.PrivateSeal.rule.PrivateSealBuilder;
import com.upc.sealback.utils.ByteUtils;
import com.upc.sealback.utils.FileUtils;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;

import static com.upc.sealback.utils.FontUtils.SEAL_SAVE_PATH;
import static com.upc.sealback.utils.NumUtils.toFloat;

/**
 * @description 画长方形印章
 */
public class CFXSeal extends PrivateSealBuilder {

    public final static int fixH = 18;//字体距上侧距离修正值
    public final static int fixW = 6;//字体距左侧距离修正值
    public static final int lineSize = 16;
    private static final int imageSize = 500;//生成图片的大小（300*300）

    /**
     * 生成base64并存储本地
     * @param dto 印章设置
     * @return base64
     * @throws Exception
     */
    @Override
    public String handleToBase64(PrivateSealDTO dto) throws Exception {
        BufferedImage bi = toBufferedImage(dto, lineSize, imageSize, fixH, fixW);
        byte[] bs = ByteUtils.buildBytes(bi);
        String fullPath = ByteUtils.storeBytes(bs,SEAL_SAVE_PATH);
        String base64 = FileUtils.fileToBase64(new File(fullPath));
        return base64;
    }

    /**
     * 生成并存储本地
     * @param dto 印章设置
     * @return 存储到本地的地址
     * @throws Exception
     */
    @Override
    public String handleToLocal(PrivateSealDTO dto) throws Exception {
        BufferedImage bi = toBufferedImage(dto, lineSize, imageSize, fixH, fixW);
        byte[] bs = ByteUtils.buildBytes(bi);
        String fullPath = ByteUtils.storeBytes(bs,SEAL_SAVE_PATH);
        return fullPath;
    }

    /**
     * 生成字节数组
     * @param dto 印章设置
     * @return 字节数组
     * @throws Exception
     */
    @Override
    public InputStream handleToStream(PrivateSealDTO dto) throws Exception{
        BufferedImage bi = toBufferedImage(dto, lineSize, imageSize, fixH, fixW);
        byte[] bs = ByteUtils.buildBytes(bi);
        return new ByteArrayInputStream(bs);
    }


    /**
     * 生成印章图像
     * @param dto 印章设置
     * @param lineSize
     * @param imageSize 图像大小
     * @param fixH
     * @param fixW
     * @return
     * @throws Exception
     */
    private BufferedImage toBufferedImage(PrivateSealDTO dto, int lineSize, int imageSize, int fixH, int fixW) throws Exception {
        //1.生成画布
        if (dto == null || dto.getFontContent().length() < 2 || dto.getFontContent().length() >= 16) {
            throw new Exception("FontText.length illegal!");
        }
        BufferedImage bi = new BufferedImage(imageSize, imageSize / 2, BufferedImage.TYPE_4BYTE_ABGR);
        Graphics2D g2d = bi.createGraphics();
        //设置不透明度
        double opacity = toFloat(dto.getColorOpacity() ,100);
        int alpha = (int) Math.round(opacity * 255);
        Color color = dto.getColorTag() == 1 ?
                new Color(254,0,0, alpha) : (dto.getColorTag() == 2 ?
                new Color(0,0,0, alpha) : new Color(0,0,254, alpha));
        //2.1设置画笔颜色
        g2d.setPaint(color);
        //2.2抗锯齿设置`
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        if (dto.getFontContent().length() == 2) {
            bi = CFXRepository.drawTwoFont(bi, g2d, dto, lineSize, imageSize, fixH, fixW, color);
        } else if (dto.getFontContent().length() == 3) {
            bi = CFXRepository.drawThreeFont(bi, g2d, dto, lineSize, imageSize, fixH, fixW, color);
        } else {
            bi = CFXRepository.drawFourFont(bi, g2d, dto, lineSize, imageSize, fixH, fixW, color);
        }
        return bi;
    }


}
