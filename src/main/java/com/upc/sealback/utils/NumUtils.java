package com.upc.sealback.utils;

import java.text.DecimalFormat;

public class NumUtils {
    /**
     * 除法运算，保留小数
     * @param numerator 被除数 分子
     * @param denominator 除数 分母
     * @return 商
     */
    public static Double toFloat(int numerator, int denominator) {
        DecimalFormat df=new DecimalFormat("0.00");//设置保留位数
        return Double.valueOf(df.format((float)numerator/denominator));
    }
}
