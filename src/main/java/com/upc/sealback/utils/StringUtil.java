package com.upc.sealback.utils;

/**
 * @description 字符串工具类
 * @create zymac
 * @date 2022/5/9 21:58
 **/
public class StringUtil {

    /**
     * 判断字符串是否为空
     * @param str 字符串
     * @return
     */
    public static boolean strIsEmpty(String str){
        if (str == null || str.length()<=0){
            return true;
        }
        return false;
    }
}
