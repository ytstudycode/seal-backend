package com.upc.sealback.utils;
// This file is auto-generated, don't edit it. Thanks.
import com.aliyun.dysmsapi20170525.Client;

import com.aliyun.dysmsapi20170525.models.*;
import com.aliyun.teaopenapi.models.*;
import com.upc.sealback.config.ResponseResult;
import org.json.JSONObject;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Random;

/**
 * 短信验证码工具类
 */
public class MsCode {

    private static final String ACCESS_KEY_ID = "";
    private static final String ACCESS_KEY_SECRET = "";

    /**
     * 使用AK&SK初始化账号Client
     * @param accessKeyId
     * @param accessKeySecret
     * @return Client
     * @throws Exception
     */
    public static Client createClient(String accessKeyId, String accessKeySecret) throws Exception {
        Config config = new Config()
                // 您的AccessKey ID
                .setAccessKeyId(accessKeyId)
                // 您的AccessKey Secret
                .setAccessKeySecret(accessKeySecret);
        // 访问的域名
        config.endpoint = "dysmsapi.aliyuncs.com";
        return new Client(config);
    }

    public static ResponseResult<Object> sendMs(HttpServletRequest request, String phoneNum) throws Exception {
        Client client = MsCode.createClient(ACCESS_KEY_ID, ACCESS_KEY_SECRET);
        //随机生成四位验证码的工具类
        String randomCode = keyUtils();
        try {
            SendSmsRequest sendSmsRequest = new SendSmsRequest()
                    .setSignName("阿里云短信测试")
                    .setTemplateCode("SMS_154950909")
                    .setPhoneNumbers(phoneNum)
                    .setTemplateParam("{\"code\":"+randomCode+"}");
            // 复制代码运行请自行打印 API 的返回值
            client.sendSms(sendSmsRequest);
            System.out.println(randomCode);
            // 签名参数未提供或者为空时，会使用默认签名发送短信
            HttpSession session = request.getSession();
            //JSONObject存入数据
            JSONObject json = new JSONObject();
            json.put("Code", randomCode);//存入验证码
            json.put("createTime", System.currentTimeMillis());//存入发送短信验证码的时间
            // 将验证码和短信发送时间码存入SESSION
            session.setAttribute("MsCode", json);
            return ResponseResult.getSuccessResult("success");
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseResult.getErrorResult("500",e.getMessage());
        }
    }

    public static String keyUtils() {
        String str="0123456789";
        StringBuilder st=new StringBuilder(4);
        for(int i=0;i<4;i++){
            char ch=str.charAt(new Random().nextInt(str.length()));
            st.append(ch);
        }
        String findkey=st.toString().toLowerCase();
        return findkey;
    }
}