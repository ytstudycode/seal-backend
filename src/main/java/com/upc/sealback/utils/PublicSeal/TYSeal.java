package com.upc.sealback.utils.PublicSeal;

import com.upc.sealback.bean.PublicSeal.PublicSealDTO;
import com.upc.sealback.bean.PublicSeal.SealCircle;
import com.upc.sealback.bean.PublicSeal.SealConfiguration;
import com.upc.sealback.bean.PublicSeal.SealFont;
import com.upc.sealback.utils.PublicSeal.repository.SealRepository;
import com.upc.sealback.utils.PublicSeal.rule.PublicSealBuilder;
import com.upc.sealback.utils.ByteUtils;
import org.springframework.util.StringUtils;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.InputStream;

import static com.upc.sealback.utils.NumUtils.toFloat;

/**
 * @description 画椭圆
 */
public class TYSeal extends PublicSealBuilder {

    /***
     * 构建签章配置类
     * @param dto
     * @return
     */
    private SealConfiguration initConfiguration(PublicSealDTO dto){
        /** 初始化配置值，添加默认参数什么的 */
        dto = PublicSealDTO.initDefault(dto);

        /** 构建配置类 */
        SealConfiguration configuration = new SealConfiguration();

        /** 签章主字体 (椭圆上圆弧形文字) */
        SealFont mainFont = new SealFont()
                    .setIsBold(true)
                    .setFontFamily(dto.getFontFamily())
                    .setMarginSize(15)
                    .setFontText(dto.getCompanyName())
                    .setFontSize(30)
                    .setFontSpace(10.0D);
        if (dto.getCompanyName().length() > 14) {
            mainFont.setFontSize(20);
            mainFont.setFontSpace(8.0D);
        }
        configuration.setMainFont(mainFont);

        /** 横向数字字体 */
        SealFont numberFont;
        if (!StringUtils.isEmpty(dto.getNumber())) {
            numberFont = new SealFont()
                    .setIsBold(true)
                    .setFontFamily(dto.getFontFamily())
                    .setMarginSize(5)
                    .setFontText(dto.getNumber())
                    .setFontSize(28)
                    .setFontSpace(12.0D);
            configuration.setViceFont(numberFont);
        }
        if (!StringUtils.isEmpty(dto.getNumber())) {
            numberFont = new SealFont()
                    .setIsBold(true)
                    .setFontFamily(dto.getFontFamily())
                    .setFontSize(22);
            if (dto.getNumber().length() > 14) {
                numberFont.setFontSize(20);
            }
            numberFont.setFontText(dto.getNumber());
            numberFont.setMarginSize(68);
            numberFont.setMarginSize(27);
            configuration.setNumberFont(numberFont);
        }

        /** 标题字体 */
        SealFont titleFont;
        if (!StringUtils.isEmpty(dto.getNo())) {
            titleFont = new SealFont()
                        .setIsBold(true)
                        .setFontFamily(dto.getFontFamily())
                        .setMarginSize(5)
                        .setFontText(dto.getNo())
                        .setFontSize(28)
                        .setFontSpace(12.0D);
            configuration.setViceFont(titleFont);
        }

        /** 下行横向字体 */
        if (!StringUtils.isEmpty(dto.getTitle())) {
            titleFont = new SealFont()
                .setIsBold(true)
                .setFontFamily(dto.getFontFamily())
                .setFontSize(28);
            if (dto.getCompanyName().length() > 14) {
                titleFont.setFontSize(20);
            }
            titleFont.setFontText(dto.getTitle());
            titleFont.setMarginSize(68);
            titleFont.setMarginSize(20);
            configuration.setTitleFont(titleFont);
        }

        /** 其他配置 */
        configuration.setImageSize(500);
        //设置不透明度
        double opacity = toFloat(dto.getColorOpacity() ,100);
        int alpha = (int) Math.round(opacity * 255);
        Color color = dto.getColorTag() == 1 ?
                new Color(254,0,0, alpha) : (dto.getColorTag() == 2 ?
                new Color(0,0,0, alpha) : new Color(0,0,254, alpha));
        configuration.setBackgroundColor(color);
        configuration.setBorderCircle(new SealCircle(10, 135, 90));
        configuration.setFontType(dto.getFontType());
        return configuration;

    }


    /**
     * 根据要求画出不同类型的印章
     * @param conf
     * @return
     * @throws Exception
     */
    public static BufferedImage buildSeal(SealConfiguration conf) throws Exception {
        BufferedImage bi = new BufferedImage(conf.getImageSize(), conf.getImageSize(), 6);
        Graphics2D g2d = bi.createGraphics();
        RenderingHints hints = new RenderingHints(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_OFF);
        hints.put(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2d.setRenderingHints(hints);
        g2d.setComposite(AlphaComposite.getInstance(2, 0.0F));
        g2d.fillRect(0, 0, conf.getImageSize(), conf.getImageSize());
        g2d.setComposite(AlphaComposite.getInstance(2, 1.0F));
        g2d.setPaint(conf.getBackgroundColor());
        if (conf.getBorderCircle() != null) {

            int borderCircleWidth = conf.getBorderCircle().getWidth();
            int borderCircleHeight = conf.getBorderCircle().getHeight();

            if(borderCircleHeight != borderCircleWidth){
                SealRepository.drawCircle(g2d, conf.getBorderCircle(), -10, 35);
            }else {
                SealRepository.drawCircle(g2d, conf.getBorderCircle(), 10, 10);
            }

            if (borderCircleHeight != borderCircleWidth) {
                SealRepository.drawArcFont4Oval(g2d, conf.getBorderCircle(), conf.getMainFont(), true);
            } else {
                SealRepository.drawArcFont4Circle(g2d, borderCircleHeight, conf.getMainFont(), true);
            }

            if(conf.getFontType() != 3) {
                if (borderCircleHeight != borderCircleWidth) {
                    SealRepository.drawArcFont4Oval(g2d, conf.getBorderCircle(), conf.getViceFont(), false);
                } else {
                    SealRepository.drawArcFont4Circle(g2d, borderCircleHeight, conf.getViceFont(), false);
                }
            }

            /*发票专用章*/
            if(conf.getFontType() == 3){
                SealRepository.drawFont(g2d, (borderCircleWidth + 10) * 2, (borderCircleHeight + 10) * 2, conf.getCenterFont());
                if(borderCircleHeight != borderCircleWidth){
                    SealRepository.drawFont(g2d, (borderCircleWidth + 10) * 2 - 40, (borderCircleHeight + 10) * 2 +110, conf.getTitleFont());
                }else {
                    SealRepository.drawFont(g2d, (borderCircleWidth + 10) * 2, (borderCircleHeight + 10) * 2 - 20, conf.getTitleFont());
                }
            }
            /*其他椭圆章*/
            else{
                SealRepository.drawFont(g2d, (borderCircleWidth + 10) * 2, (borderCircleHeight + 10) * 2, conf.getCenterFont());

                if(borderCircleHeight != borderCircleWidth){
                    SealRepository.drawFont(g2d, (borderCircleWidth + 10) * 2 - 40, (borderCircleHeight + 10) * 2 + 100, conf.getTitleFont());
                }else {
                    SealRepository.drawFont(g2d, (borderCircleWidth + 10) * 2, (borderCircleHeight + 10) * 2 - 20, conf.getTitleFont());
                }
            }
            if(conf.getFontType() == 3) {
                SealRepository.drawNumFont(g2d, (borderCircleWidth + 10) * 2, (borderCircleHeight + 10) * 2, conf.getCenterFont());

                if (borderCircleHeight != borderCircleWidth) {
                        SealRepository.drawNumFont(g2d, (borderCircleWidth + 10) * 2 + 195, (borderCircleHeight + 10) * 2 + 265, conf.getNumberFont());
                } else {
                    SealRepository.drawNumFont(g2d, (borderCircleWidth + 10) * 2, (borderCircleHeight + 10) * 2 - 20, conf.getNumberFont());
                }
            }

            g2d.dispose();
            return bi;
        } else {
            throw new Exception("BorderCircle can not null！");
        }
    }

    @Override
    public String handleToBase64(PublicSealDTO dto) throws Exception {
        SealConfiguration configuration = initConfiguration(dto);
        String base64 = SealRepository.dealWith(configuration);
        return base64;
    }

    @Override
    public String handleToLocal(PublicSealDTO dto) throws Exception {
        SealConfiguration configuration = initConfiguration(dto);
        String path = SealRepository.dealWithLocal(configuration);
        return path;
    }


    @Override
    public InputStream handleToStream(PublicSealDTO dto) throws Exception{
        SealConfiguration configuration = initConfiguration(dto);
        BufferedImage bi = buildSeal(configuration);
        byte[] bs = ByteUtils.buildBytes(bi);
        return new ByteArrayInputStream(bs);
    }
}
