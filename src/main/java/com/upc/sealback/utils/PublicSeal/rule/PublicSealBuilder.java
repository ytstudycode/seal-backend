package com.upc.sealback.utils.PublicSeal.rule;

import com.upc.sealback.bean.PublicSeal.PublicSealDTO;

import java.io.InputStream;

/**
 * @author mqz
 * @description
 * @abount https://github.com/DemoMeng
 * @since 2020/10/19
 */
public abstract class PublicSealBuilder {

    /**
     * 返回base64
     * @param dto
     * @return
     * @throws Exception
     */
    public abstract String handleToBase64(PublicSealDTO dto) throws Exception;



    public abstract String handleToLocal(PublicSealDTO dto) throws Exception;

    /**
     * 生成字节数组
     * @param dto 印章设置
     * @return 字节数组
     * @throws Exception
     */
    public abstract InputStream handleToStream(PublicSealDTO dto) throws Exception;
}
