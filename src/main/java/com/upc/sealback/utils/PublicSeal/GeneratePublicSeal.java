package com.upc.sealback.utils.PublicSeal;

import com.upc.sealback.bean.PublicSeal.PublicSealDTO;
import com.upc.sealback.bean.SealDetail;
import com.upc.sealback.config.ResponseResult;
import com.upc.sealback.utils.PublicSeal.rule.PublicSealBuilder;
import org.jetbrains.annotations.NotNull;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import static com.upc.sealback.utils.FileOSS.streamUploadOSS;
import static com.upc.sealback.utils.FontUtils.Font_FangSong;
import static com.upc.sealback.utils.FontUtils.Font_SongTi;

/**
 * @description 企业印章生成类
 * @create zymac
 * @date 2022/4/4 17:01
 **/
public class GeneratePublicSeal {

    public static ResponseResult<Object> generatePublicSeal(@NotNull SealDetail sealDetail) throws Exception {
        PublicSealBuilder psb = null;
        PublicSealDTO dto = null;
        Integer star = 0;
        if(sealDetail.getSealStyle() == 12 || sealDetail.getSealStyle() == 22 ||
                sealDetail.getSealStyle() == 42 || sealDetail.getSealStyle() == 52) {
            star = 1;
        }

        /**==============================================================
         * ============================【公章】============================
         * ==============================================================**/
        if(sealDetail.getSealType() == 1)
        {
            /* 椭圆章 */
            if(sealDetail.getSealStyle() == 13){
                psb = new TYSeal();
                dto = new PublicSealDTO()
                        .setColorTag(sealDetail.getSealColor())
                        .setColorOpacity(sealDetail.getSealOpacity())
                        .setCompanyName(sealDetail.getSealContent())
                        .setFontFamily(Font_SongTi)
                        .setFontType(sealDetail.getSealType())
                        .setNo(sealDetail.getSealLower())
                        .setTitle(sealDetail.getSealTransverse());
            }
            /* 圆形章 */
            else{
                psb = new YXSeal();
                dto = new PublicSealDTO()
                        .setColorTag(sealDetail.getSealColor())
                        .setColorOpacity(sealDetail.getSealOpacity())
                        .setCompanyName(sealDetail.getSealContent())
                        .setFontFamily(Font_SongTi)
                        .setFontType(sealDetail.getSealType())
                        .setStar(star)
                        .setNo(sealDetail.getSealLower())
                        .setTitle(sealDetail.getSealTransverse());
            }
        }

        /**==============================================================
         * ============================【合同专用章】=======================
         * ==============================================================**/
        else if(sealDetail.getSealType() == 2)
        {
            /* 圆形章 */
            psb = new YXSeal();
            dto = new PublicSealDTO()
                    .setColorTag(sealDetail.getSealColor())
                    .setColorOpacity(sealDetail.getSealOpacity())
                    .setCompanyName(sealDetail.getSealContent())
                    .setFontFamily(Font_SongTi)
                    .setFontType(sealDetail.getSealType())
                    .setStar(star)
                    .setNo(sealDetail.getSealLower())
                    .setTitle(sealDetail.getSealTransverse());
        }

        /**==============================================================
         * ============================【发票专用章】=======================
         * ==============================================================**/
        else if(sealDetail.getSealType() == 3)
        {
            /* 椭圆章 */
            psb = new TYSeal();
            dto = new PublicSealDTO()
                    .setColorTag(sealDetail.getSealColor())
                    .setColorOpacity(sealDetail.getSealOpacity())
                    .setCompanyName(sealDetail.getSealContent())
                    .setFontFamily(Font_FangSong)
                    .setNo(sealDetail.getSealLower())
                    .setTitle(sealDetail.getSealTransverse())
                    .setFontType(sealDetail.getSealType())
                    .setNumber(sealDetail.getSealNumber());
        }

        /**==============================================================
         * ============================【财务专用章】=======================
         * ==============================================================**/
        else if(sealDetail.getSealType() == 4)
        {
            /* 圆形章 */
            if(sealDetail.getSealStyle() == 41 || sealDetail.getSealStyle() == 42) {
                psb = new YXSeal();
                dto = new PublicSealDTO()
                        .setColorTag(sealDetail.getSealColor())
                        .setColorOpacity(sealDetail.getSealOpacity())
                        .setCompanyName(sealDetail.getSealContent())
                        .setFontFamily(Font_SongTi)
                        .setFontType(sealDetail.getSealType())
                        .setStar(star)
                        .setNo(sealDetail.getSealLower())
                        .setTitle(sealDetail.getSealTransverse());
            }
            /* 椭圆章 */
            else if(sealDetail.getSealStyle() == 43) {
                psb = new TYSeal();
                dto = new PublicSealDTO()
                        .setColorTag(sealDetail.getSealColor())
                        .setColorOpacity(sealDetail.getSealOpacity())
                        .setCompanyName(sealDetail.getSealContent())
                        .setFontFamily(Font_SongTi)
                        .setFontType(sealDetail.getSealType())
                        .setNo(sealDetail.getSealLower())
                        .setTitle(sealDetail.getSealTransverse());
            }
        }

        /**==============================================================
         * ============================【人事专用章】=======================
         * ==============================================================**/
        else if(sealDetail.getSealType() == 5) {
            /* 圆形章 */
            if (sealDetail.getSealStyle() == 51 || sealDetail.getSealStyle() == 52) {
                psb = new YXSeal();
                dto = new PublicSealDTO()
                        .setColorTag(sealDetail.getSealColor())
                        .setColorOpacity(sealDetail.getSealOpacity())
                        .setCompanyName(sealDetail.getSealContent())
                        .setFontFamily(Font_SongTi)
                        .setFontType(sealDetail.getSealType())
                        .setStar(star)
                        .setNo(sealDetail.getSealLower())
                        .setTitle(sealDetail.getSealTransverse());
            }
            /* 椭圆章 */
            else if (sealDetail.getSealStyle() == 53) {
                psb = new TYSeal();
                dto = new PublicSealDTO()
                        .setColorTag(sealDetail.getSealColor())
                        .setColorOpacity(sealDetail.getSealOpacity())
                        .setCompanyName(sealDetail.getSealContent())
                        .setFontFamily(Font_SongTi)
                        .setFontType(sealDetail.getSealType())
                        .setNo(sealDetail.getSealLower())
                        .setTitle(sealDetail.getSealTransverse());
            }
        }
        assert psb != null;
        Map<String,Object> map = new HashMap<String,Object>();
        map.put("psb",psb);
        map.put("dto",dto);
//        InputStream inputStream = psb.handleToStream(dto);
//        ResponseResult<Object> responseResult = streamUploadOSS(inputStream);
        return ResponseResult.getSuccessResult(map);
    }
}
