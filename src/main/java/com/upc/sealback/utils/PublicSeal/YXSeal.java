package com.upc.sealback.utils.PublicSeal;


import com.upc.sealback.bean.PublicSeal.PublicSealDTO;
import com.upc.sealback.bean.PublicSeal.SealCircle;
import com.upc.sealback.bean.PublicSeal.SealConfiguration;
import com.upc.sealback.bean.PublicSeal.SealFont;
import com.upc.sealback.utils.PublicSeal.repository.SealRepository;
import com.upc.sealback.utils.PublicSeal.rule.PublicSealBuilder;
import com.upc.sealback.utils.ByteUtils;
import org.springframework.util.StringUtils;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.InputStream;

import static com.upc.sealback.utils.NumUtils.toFloat;

/**
 * @author mqz
 * @description
 * @abount https://github.com/DemoMeng
 * @since 2020/10/19
 */
public class YXSeal extends PublicSealBuilder {


    /***
     * 构建签章配置类
     * @param dto
     * @return
     */
    private SealConfiguration initConfiguration(PublicSealDTO dto){
        dto = PublicSealDTO.initDefault(dto);
        /** 签章配置初始化 */
        SealConfiguration configuration = new SealConfiguration();
        /** 企业签章设置主字体 */
        SealFont mainFont = new SealFont()
                .setIsBold(true)
                .setFontFamily("STSongti-SC-Bold")
                .setMarginSize(1)
                .setFontText(dto.getCompanyName())
                .setFontSize(30)
                .setFontSpace(30.0D);
        if (dto.getCompanyName().length() > 14) {
            mainFont.setFontSize(20);
            mainFont.setFontSpace(16.0D);
        }
        configuration.setMainFont(mainFont);

        /** 设置中间字体 */
        SealFont centerFont;
        if (!StringUtils.isEmpty(dto.getNo())) {
            centerFont = new SealFont()
                    .setFontFamily(dto.getFontFamily())
                    .setMarginSize(-5)
                    .setFontText(dto.getNo())
                    .setIsBold(false)
                    .setFontSize(13)
                    .setFontSpace(10.0D);
            configuration.setViceFont(centerFont);
        }

        if(dto.getStar() == 1){
            centerFont = new SealFont()
                    .setIsBold(true)
                    .setFontFamily("STSongti-SC-Bold")
                    .setFontText("★")
                    .setFontSize(90);
            configuration.setCenterFont(centerFont);
        }


        /** 设置标题 */
        if (!StringUtils.isEmpty(dto.getTitle())) {
            SealFont titleFont = new SealFont()
                    .setIsBold(true)
                    .setFontFamily(dto.getFontFamily())
                    .setFontSize(26)
                    .setFontText(dto.getTitle())
                    .setMarginSize(70);
            configuration.setTitleFont(titleFont);
        }
        /** 其他设置 */
        configuration.setImageSize(500);
        //设置不透明度
        double opacity = toFloat(dto.getColorOpacity() ,100);
        int alpha = (int) Math.round(opacity * 255);
        Color color = dto.getColorTag() == 1 ?
                new Color(254,0,0, alpha) : (dto.getColorTag() == 2 ?
                new Color(0,0,0, alpha) : new Color(0,0,254, alpha));
        configuration.setBackgroundColor(color);
        configuration.setBorderCircle(new SealCircle(8, 115, 115));
        configuration.setFontType(dto.getFontType());
        return configuration;
    }


    @Override
    public String handleToBase64(PublicSealDTO dto) throws Exception {
        SealConfiguration configuration = initConfiguration(dto);
        String base64 = SealRepository.dealWith(configuration);
        return base64;
    }

    @Override
    public String handleToLocal(PublicSealDTO dto) throws Exception {
        SealConfiguration configuration = initConfiguration(dto);
        String path = SealRepository.dealWithLocal(configuration);
        return path;
    }

    @Override
    public InputStream handleToStream(PublicSealDTO dto) throws Exception{
        SealConfiguration configuration = initConfiguration(dto);
        BufferedImage bi = TYSeal.buildSeal(configuration);
        byte[] bs = ByteUtils.buildBytes(bi);
        return new ByteArrayInputStream(bs);
    }
}
