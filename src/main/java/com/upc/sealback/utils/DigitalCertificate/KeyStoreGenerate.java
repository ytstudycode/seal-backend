package com.upc.sealback.utils.DigitalCertificate;

import com.alibaba.fastjson.JSON;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.*;
import com.itextpdf.text.pdf.parser.ContentByteUtils;
import com.itextpdf.text.pdf.parser.PdfContentStreamProcessor;
import com.itextpdf.text.pdf.parser.PdfReaderContentParser;
import com.itextpdf.text.pdf.security.*;
import com.upc.sealback.bean.PdfSignInfo;
import com.upc.sealback.bean.SealSigner;
import com.upc.sealback.config.ResponseResult;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.util.encoders.Base64Encoder;
import sun.security.tools.keytool.CertAndKeyGen;
import sun.security.x509.X500Name;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.lang.reflect.Field;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.*;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.*;
import java.util.List;

import static com.upc.sealback.utils.FileOSS.*;
import static com.upc.sealback.utils.FileUtils.deleteFile;
import static com.upc.sealback.utils.FileUtils.fileToBase64;

/**
 * @author zymac
 */
public class KeyStoreGenerate {

    /** 证书容器地址 **/
    private static String KEYSTORE_PATH = "./data/demo.ks";

    /** 密码 **/
    private static String KEYSTORE_PASSWORD = "111111";

    /** 密钥库格式 **/
    private static String KEYSTORE_TYPE = "PKCS12";

    /** pdf路径 **/
    private static String PDF_PATH = "./data/test2.pdf";

    /** 签署成功pdf路径 **/
    private static String PDF_SIGNED = "./data/success.pdf";

    /** 签章图片 **/
    //private static String SIGN_IMG = "./data/test.png";
    //private static String SIGN_IMG = "./data/mm.png";
    private static String SIGN_IMG = "./data/test1.png";

    /** 带有数字签名和时间戳的pdf **/
    private static String CHK_PDF = "./data/chk.pdf";

    //这里是防止报下面这个异常 java.security.NoSuchProviderException: no such provider: BC
    static {
        Security.addProvider(new BouncyCastleProvider());
    }


    public static void main(String[] args) throws Exception {

        //coverTokeyStore();

//        // 待加密码的文件
//        PdfReader reader1 = new PdfReader("./data/1652334243443.pdf");
//        // 加完密码的文件
//        PdfStamper stamper1 = new PdfStamper(reader1, new FileOutputStream(
//                "./data/test2.pdf"));
//        // 设置密码文件打开密码文件编辑密码
//        String password = "1111";
//        int permission = 2;
//        stamper1.setEncryption(password.getBytes(),password.getBytes(),0,false);
//        stamper1.close();
        // 生成keystore容器并生成一张证书
        // 到keystore目录下面输入 keytool -list -v -keystore demo.ks 输入密码就可以看到keystore里面所有的证书
        //generateKeyStore();

        //安装第二张证书
        //installSencondCert();

        //pdf证书签署
        //signPdf("123456");
        // signPdf("/Users/zymac/Desktop/SealBack/data/my.pdf","/Users/zymac/Desktop/SealBack/data/img.png");
        //验签
        // yanqian();
        getSignFromPdf("/Users/zymac/Desktop/SealBack/data/su.pdf");
       // changeImgColor("/Users/zymac/Desktop/SealBack/data/test/su/138.png");
        //骑缝章

    }


    /**
     * pdf证书签署
     * @throws Exception
     */
    public static ResponseResult<Object> signPdf(String pdfPath, String sealPath, SealSigner sealSigner) throws Exception {
        BouncyCastleProvider provider = new BouncyCastleProvider();
        Security.addProvider(provider);
        String pfxFilePath = "./data/" + sealSigner.getSignerId() + ".pfx";
        InputStream inputStream = new FileInputStream(pfxFilePath);
        //KeyStore keyStore = KeyStore.getInstance(KEYSTORE_TYPE);
        KeyStore keyStore = KeyStore.getInstance("PKCS12");
        keyStore.load(inputStream,KEYSTORE_PASSWORD.toCharArray());
        String alias = (String) keyStore.aliases().nextElement();
        //证书的私钥
        //PrivateKey privateKey = (PrivateKey) keyStore.getKey(alias, KEYSTORE_PASSWORD.toCharArray());
        PrivateKey privateKey = GetPvkformPfx(pfxFilePath,"111111");
        // System.out.println(privateKey);
        //证书链
        Certificate[] chain = keyStore.getCertificateChain(alias);

        String digestAlgorithm = DigestAlgorithms.SHA256;
        MakeSignature.CryptoStandard subfilter = MakeSignature.CryptoStandard.CMS;

        // Creating the reader and the stamper
        // System.out.println(pdfPath);
        PdfReader reader = new PdfReader(pdfPath);
        Rectangle pageSize = reader.getPageSize(1);
        FileOutputStream os = new FileOutputStream(new File(PDF_SIGNED));
        //签署需要提供一个临时的目录
        PdfStamper stamper =
                PdfStamper.createSignature(reader, os, '\0', new File("/tmp/pdf"),true);

        //System.out.println("页数是："+reader.getNumberOfPages());
        // Creating the appearance
        PdfSignatureAppearance appearance = stamper.getSignatureAppearance();
        appearance.setReason("小周毕业设计专用");
        appearance.setLocation("小周毕业设计专用");

        //设置签名的位置，页码，签名域名称，多次追加签名的时候，签名域名称不能一样
        //签名的位置，是图章相对于pdf页面的位置坐标，原点为pdf页面左下角
        //四个参数的分别是，图章左下角x，图章左下角y，图章右上角x，图章右上角y

        int signPage = sealSigner.getSignerSignPage();
        int llx = (int) (sealSigner.getSignerSignX() - 40);
        int lly = (int) (842 - sealSigner.getSignerSignY() + 40);
        int urx = llx + 150;
        int ury = lly + 150;
        appearance.setVisibleSignature(new Rectangle(llx, lly, urx, ury),
                signPage, System.currentTimeMillis()+"");
        //fileName: 随机作用域

        //签署图片地址
        Image image = Image.getInstance(sealPath);
        appearance.setSignatureGraphic(image);
        appearance.setCertificationLevel(PdfSignatureAppearance.NOT_CERTIFIED);
        //设置图章的显示方式，如下选择的是只显示图章（还有其他的模式，可以图章和签名描述一同显示）
        appearance.setRenderingMode(PdfSignatureAppearance.RenderingMode.GRAPHIC);

        // Creating the signature   签名算法
        ExternalSignature pks = new PrivateKeySignature(privateKey, digestAlgorithm, provider.getName());
        //摘要算法
        ExternalDigest digest = new BouncyCastleDigest();
        // 调用itext签名方法完成pdf签章
        MakeSignature.signDetached(appearance, digest, pks, chain,
                null, null, null, 0, subfilter);

        InputStream inputStream1 = new FileInputStream(PDF_SIGNED);
        return streamPdfUploadOSS(inputStream1);

    }

    /**
     * 从pdf文件中获取PrivateKey
     * @param strPfx
     * @param strPassword
     * @return
     */
    public static PrivateKey GetPvkformPfx(String strPfx, String strPassword) {
        try {
            BouncyCastleProvider provider = new BouncyCastleProvider();
            Security.addProvider(provider);
            KeyStore ks = KeyStore.getInstance("PKCS12");
            FileInputStream fis = new FileInputStream(strPfx);
            // If the keystore password is empty(""), then we have to set
            // to null, otherwise it won't work!!!
            char[] nPassword = null;
            if ((strPassword == null) || strPassword.trim().equals("")) {
                nPassword = null;
            } else {
                nPassword = strPassword.toCharArray();
            }
            ks.load(fis, nPassword);
            fis.close();
            System.out.println("keystore type=" + ks.getType());
            // Now we loop all the aliases, we need the alias to get keys.
            // It seems that this value is the "Friendly name" field in the
            // detals tab <-- Certificate window <-- view <-- Certificate
            // Button <-- Content tab <-- Internet Options <-- Tools menu
            // In MS IE 6.
            Enumeration enumas = ks.aliases();
            String keyAlias = null;
            if (enumas.hasMoreElements())// we are readin just one certificate.
            {
                keyAlias = (String) enumas.nextElement();
                System.out.println("alias=[" + keyAlias + "]");
            }
            // Now once we know the alias, we could get the keys.
            System.out.println("is key entry=" + ks.isKeyEntry(keyAlias));
            PrivateKey prikey = (PrivateKey) ks.getKey(keyAlias, nPassword);
            Certificate[] cert = ks.getCertificateChain(keyAlias);
            System.out.println("chain size " + cert.length);
            PublicKey pubkey = cert[0].getPublicKey();
            System.out.println("cert class = " + cert.getClass().getName());
            System.out.println("cert = " + cert);
            System.out.println("public key = " + pubkey);
            System.out.println("private key = " + prikey);

            return prikey;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 获取pdf中数字签名信息
     * @param path          pdf文件
     * @return              提取结果
     */
    public static PdfSignInfo getSignFromPdf(String path) {
        path =  System.getProperty("user.dir") +"/src/main/resources/static" + path;
        PdfSignInfo pdfSignInfo = new PdfSignInfo();
        String fileBasePath = System.getProperty("user.dir") + "/src/main/resources/static/upload/";
        try {
            File oldFile = new File(path);
            FileInputStream fis = new FileInputStream(path);
            PdfReader reader = new PdfReader(fis);

            String fileName = path.substring(path.lastIndexOf("/") + 1, path.lastIndexOf("."));
            if (!fileBasePath.endsWith("/")) {
                fileBasePath = fileBasePath + "/";
            }
//            fileBasePath = fileBasePath + fileName + "/";
//            File file = new File(fileBasePath);
//            file.mkdirs();

            pdfSignInfo.setPdfName(path.substring(path.lastIndexOf("/") + 1));
            pdfSignInfo.setPdfSize(String.valueOf(oldFile.length()));
            Field rsaDataField = PdfPKCS7.class.getDeclaredField("RSAdata");
            rsaDataField.setAccessible(true);

            int numberOfPages = reader.getNumberOfPages();
            // pdf 页码数量
            System.out.println("page size: " + numberOfPages);

            int xrefSize = reader.getXrefSize();

            ExtImageRenderListener listener = new ExtImageRenderListener();
            listener.setBasePath(fileBasePath);

            // 获取acro字段
            AcroFields fields = reader.getAcroFields();
            Rectangle pageSize = reader.getPageSize(numberOfPages );
            // pdf 作用域尺寸（全屏）
            System.out.println("pdf rectangle: " + pageSize.getLeft() + "," + pageSize.getBottom() + "," + pageSize.getRight() + "," + pageSize.getTop());
            // 获取签名名称
            ArrayList<String> signatureNames = fields.getSignatureNames();
            for (String name : signatureNames) {
                // 指定提供者 获取签名的pkcs7数据
                PdfPKCS7 pkcs7 = fields.verifySignature(name);
                X509Certificate certificate = pkcs7.getSigningCertificate();

                PdfSignInfo.SignatureDetail info = new PdfSignInfo.SignatureDetail();

                String[] appearanceStates = fields.getAppearanceStates(name);
                // 表单域的位置
                List<AcroFields.FieldPosition> fieldPositions = fields.getFieldPositions(name);
                for (AcroFields.FieldPosition fieldPosition : fieldPositions) {
                    Rectangle position = fieldPosition.position;
                    System.out.println("field " + name + " position: " + fieldPosition.page + ", " +
                            position.getLeft() + ", " + position.getBottom() + ", " + position.getRight() + ", " + position.getTop());
                    info.pageNum = fieldPosition.page;
                    info.llx = position.getLeft();
                    info.lly = position.getBottom();
                    info.urx = position.getRight();
                    info.ury = position.getTop();
                }

                AcroFields.Item fieldItem = fields.getFieldItem(name);
                int size = fieldItem.size();
                for (int i = 0; i < size; i++) {
                    PdfDictionary value = fieldItem.getValue(i);
                    Set<PdfName> keys = value.getKeys();
                    for (PdfName key : keys) {
                        String keyName = PdfName.decodeName(new String(key.getBytes()));
                        if ("Rect".equalsIgnoreCase(keyName)) {
                            System.out.println("签名域的坐标为：" + value.get(key));
                        }
                    }
                    PdfObject pdfObject = value.get(PdfName.AP);
                    if (pdfObject != null) {
                        PdfDictionary dictionary = (PdfDictionary) pdfObject;
                        PdfIndirectReference ref = (PdfIndirectReference) dictionary.get(PdfName.N);
                        int number = ref.getNumber();
                        System.out.println("fieldName: " + name + ", idx: " + number);

                        // 根据ap 获取签章图片
                        PdfObject pdfObjectRelease = reader.getPdfObject(number);
                        if (pdfObjectRelease instanceof PdfStream) {
                            PdfStream s = (PdfStream) pdfObjectRelease;
                            PdfDictionary resources = s.getAsDict(PdfName.RESOURCES);
                            listener.setI(number);
                            try {
                                PdfContentStreamProcessor processor = new PdfContentStreamProcessor(listener);
                                processor.processContent(ContentByteUtils.getContentBytesFromContentObject(s), resources);
                                changeImgColor(fileBasePath + number + ".png");
                                ResponseResult<Object> responseResult = fileUploadOSS(number + ".png");
                                info.filePath = responseResult.getResult().toString();
                                //info.sealBase64 = fileToBase64(new File(fileBasePath + number + ".png"));
                                deleteFile(fileBasePath + number + ".png");
                            } catch (Exception ignore) {}
                        }
                    }
                }

                // 签名信息
                info.signName = name;
                info.signTime = pkcs7.getSignDate().getTime();
                info.validStartTime = certificate.getNotBefore();
                info.validEndTime = certificate.getNotAfter();
                info.certName = certificate.getSubjectDN().getName();
                info.serialNumber = String.valueOf(certificate.getSerialNumber());
                Base64.Encoder encoder = Base64.getEncoder();
                info.publicKey = encoder.encodeToString(certificate.getPublicKey().getEncoded());
                info.pubKeyFormat = certificate.getPublicKey().getFormat();
                info.sigAlgName = certificate.getSigAlgName();
                info.userDnName = certificate.getIssuerDN().getName();
                info.reason = pkcs7.getReason();
                info.location = pkcs7.getLocation();
                info.validate = pkcs7.verify();
                Field digestAttrField = PdfPKCS7.class.getDeclaredField("digestAttr");
                digestAttrField.setAccessible(true);
                byte[] digestAttr = (byte[]) digestAttrField.get(pkcs7);
                info.fileDigest = encoder.encodeToString(digestAttr);
                pdfSignInfo.getSignatureDetails().add(info);
            }
            System.out.println(JSON.toJSONString(pdfSignInfo));
        } catch (Exception e) {
            e.printStackTrace();
        }
        deleteFile(path);
        return pdfSignInfo;
    }

    /**
     * 将背景替换为透明
     * @return
     * @throws IOException the io exception
     * @author Jack Que
     * @created 2021 -07-08 10:25:10 Change img color.
     */
    public static void changeImgColor(String path) throws IOException {

        File file = new File(path);
        String fileName = file.getName();
        BufferedImage bi =  ImageIO.read(file);
        BufferedImage image =  bi;
        //将原图片的二进制转化为ImageIcon
        ImageIcon imageIcon = new ImageIcon(image);
        int width = imageIcon.getIconWidth();
        int height = imageIcon.getIconHeight();
//
        //图片缓冲流
        BufferedImage bufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_4BYTE_ABGR);
        Graphics2D graphics2D = (Graphics2D) bufferedImage.getGraphics();
        graphics2D.drawImage(imageIcon.getImage(), 0, 0, imageIcon.getImageObserver());

        int alpha = 255;

        //这个背景底色的选择，我这里选择的是比较偏的位置，可以修改位置。背景色选择不知道有没有别的更优的方式（比如先过滤一遍获取颜色次数最多的，但是因为感觉做起来会比较复杂没去实现），如果有可以评论。
        int RGB=bufferedImage.getRGB(width-1, height-1);

        for(int i = bufferedImage.getMinX(); i < width; i++) {
            for(int j = bufferedImage.getMinY(); j < height; j++) {

                int rgb = bufferedImage.getRGB(i, j);

                int r = (rgb & 0xff0000) >>16;
                int g = (rgb & 0xff00) >> 8;
                int b = (rgb & 0xff);
                int R = (RGB & 0xff0000) >>16;
                int G = (RGB & 0xff00) >> 8;
                int B = (RGB & 0xff);
                //a为色差范围值，渐变色边缘处理，数值需要具体测试，50左右的效果比较可以
                int a = 45;
                if(Math.abs(R-r) < a && Math.abs(G-g) < a && Math.abs(B-b) < a ) {
                    alpha = 0;
                } else {
                    alpha = 255;
                }
                rgb = (alpha << 24)|(rgb & 0x00ffffff);
                bufferedImage.setRGB(i,j,rgb);
            }
        }

//        graphics2D.drawImage(bufferedImage, 0, 0, imageIcon.getImageObserver());

        //新建字节输出流，用来存放替换完背景的图片
//        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

        String[] split = fileName.split("\\.");
        fileName = split[0]+"(已转换)."+split[1];
        ImageIO.write(bufferedImage, "png", new File(path));
    }




    /**
     * 生成keystore容器
     * @throws Exception
     */
    public static void generateKeyStore() throws Exception {

        KeyStore keyStore = KeyStore.getInstance(KEYSTORE_TYPE);
        keyStore.load(null,KEYSTORE_PASSWORD.toCharArray());
        CertAndKeyGen keyGen = new CertAndKeyGen("RSA","SHA1WithRSA",null);
        //CN 姓名  OU 组织单位名称 O 组织名称 L xx ST 省市区名称 C 国家
        X500Name x500Name = new X500Name("小周",  "XXX","CCST","QingDao", "ShanDong", "CHN");
        //设置加密算法长度
        keyGen.generate(2048);
        PrivateKey privateKey = keyGen.getPrivateKey();
        X509Certificate [] chain =  new X509Certificate[1];
        //设置keystore的有效期限
        chain [0] = keyGen.getSelfCertificate(x500Name,new Date(),1096 * 24 * 60 *60);

        FileOutputStream fileOutputStream = new FileOutputStream(KEYSTORE_PATH);
        //设置第一张初始化证书
        keyStore.setKeyEntry("123456",privateKey,KEYSTORE_PASSWORD.toCharArray(),chain);
        keyStore.store(fileOutputStream,KEYSTORE_PASSWORD.toCharArray());
        fileOutputStream.close();
    }

    /**
     * 安装第二张证书
     */
    public static void installSencondCert() throws Exception {
        InputStream inputStream = new FileInputStream(KEYSTORE_PATH);
        KeyStore keyStore = KeyStore.getInstance(KEYSTORE_TYPE);
        keyStore.load(inputStream,KEYSTORE_PASSWORD.toCharArray());

        CertAndKeyGen keyGen = new CertAndKeyGen("RSA","SHA1WithRSA",null);
        //CN 姓名  OU 组织单位名称 O 组织名称 L xx ST 省市区名称 C 国家
        X500Name x500Name = new X500Name("苏雨",  "567","897","HangZhou", "ZJ", "CHN");
        //设置加密算法长度
        keyGen.generate(2048);
        PrivateKey privateKey = keyGen.getPrivateKey();
        X509Certificate [] chain =  new X509Certificate[1];
        //设置keystore的有效期限
        chain [0] = keyGen.getSelfCertificate(x500Name,new Date(),1096 * 24 * 60 *60);

        FileOutputStream fileOutputStream = new FileOutputStream(KEYSTORE_PATH);
        //设置第二张证书
        keyStore.setKeyEntry("888888",privateKey,KEYSTORE_PASSWORD.toCharArray(),chain);
        keyStore.store(fileOutputStream,KEYSTORE_PASSWORD.toCharArray());
        fileOutputStream.close();

    }

    /**
     * 验证pdf证书信息
     * @throws IOException
     * @throws GeneralSecurityException
     */
    public static void yanqian() throws IOException, GeneralSecurityException {

        PdfReader pdfReader = new PdfReader("./data/success.pdf");
        AcroFields acroFields = pdfReader.getAcroFields();
        List<String> names = acroFields.getSignatureNames();


        for (String name : names) {
            PdfDictionary signatureDict = acroFields.getSignatureDictionary(name);
            //时间戳
            String timestrap = signatureDict.getAsString(PdfName.M).toString().replace("D:","").substring(0,12);

            PdfPKCS7 pdfPKCS7 = acroFields.verifySignature(name);

            X509Certificate x509Certificate = pdfPKCS7.getSigningCertificate();
            Principal principal = x509Certificate.getIssuerDN();
            //证书颁发机构
            String s = principal.toString().split("CN")[2].replace("=","");
            //时间戳有效性
            boolean flag = pdfPKCS7.verifyTimestampImprint();
            //签署人姓名
            String signerName = CertificateInfo.getSubjectFields(pdfPKCS7.getSigningCertificate()).getField("CN");
            //文档是否被修改
            boolean isChange = pdfPKCS7.verify();

            System.out.println(signerName + "\t时间戳是否有效:" +flag + "\t" + timestrap + "\t颁发机构:" +s+ "\t是否被篡改:"+isChange);
        }
    }

    /**
     * 骑缝章签署
     * @param alias
     * @throws IOException
     * @throws DocumentException
     * @throws GeneralSecurityException
     */
    public static void qfz(String alias) throws IOException, DocumentException, GeneralSecurityException {
        //选择需要印章的pdf
        PdfReader reader = new PdfReader(PDF_PATH);
        //获得第一页
        Rectangle pageSize = reader.getPageSize(1);
        float height = pageSize.getHeight();
        float width = pageSize.getWidth();
        //pdf页数
        int nums = reader.getNumberOfPages();
        //生成骑缝章切割图片
        Image[] images = ImageUtils.subImages(SIGN_IMG, nums);

        InputStream inputStream = new FileInputStream(KEYSTORE_PATH);
        KeyStore keyStore = KeyStore.getInstance(KEYSTORE_TYPE);
        keyStore.load(inputStream,KEYSTORE_PASSWORD.toCharArray());
        //证书的私钥
        PrivateKey privateKey = (PrivateKey) keyStore.getKey(alias, KEYSTORE_PASSWORD.toCharArray());
        //证书链
        Certificate[] chain = keyStore.getCertificateChain(alias);

        String digestAlgorithm = DigestAlgorithms.SHA256;
        MakeSignature.CryptoStandard subfilter = MakeSignature.CryptoStandard.CMS;

        ExternalSignature pks = new PrivateKeySignature(privateKey, digestAlgorithm, "BC");
        //摘要算法
        ExternalDigest digest = new BouncyCastleDigest();

        String path = PDF_PATH;
        // Creating the signature   签名算法
        int i= 1;
        for(Image image : images) {

            //选择需要印章的pdf
            reader = new PdfReader(path);
            path = "./data/"+new Random().nextInt(1000)+".pdf";

            FileOutputStream os = new FileOutputStream(new File(path));
            //签署需要提供一个临时的目录
            PdfStamper stamper =
                    PdfStamper.createSignature(reader, os, '\0', new File(PDF_SIGNED), true);
            // Creating the appearance
            PdfSignatureAppearance appearance = stamper.getSignatureAppearance();

            appearance.setReason("签署理由");
            appearance.setLocation("签署位置");

            appearance.setCertificationLevel(PdfSignatureAppearance.NOT_CERTIFIED);
            //设置图章的显示方式，如下选择的是只显示图章（还有其他的模式，可以图章和签名描述一同显示）
            appearance.setRenderingMode(PdfSignatureAppearance.RenderingMode.GRAPHIC);
            //设置签名的位置，页码，签名域名称，多次追加签名的时候，签名域名称不能一样
            //签名的位置，是图章相对于pdf页面的位置坐标，原点为pdf页面左下角
            //四个参数的分别是，图章左下角x，图章左下角y，图章右上角x，图章右上角y
            appearance.setVisibleSignature(new Rectangle(width-20, height/2 , width, height/2 + 60),
                    i, System.currentTimeMillis()+"");
            //fileName: 随机作用域

            //签署图片地址
            appearance.setSignatureGraphic(image);
            // 调用itext签名方法完成pdf签章
            MakeSignature.signDetached(appearance, digest, pks, chain,
                    null, null, null, 0, subfilter);
            i++;
            Files.copy(Paths.get(path),new FileOutputStream (new File (PDF_SIGNED)));
        }

    }
}
