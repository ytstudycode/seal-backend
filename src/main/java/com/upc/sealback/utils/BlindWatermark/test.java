package com.upc.sealback.utils.BlindWatermark;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * @description 测试
 * @create zymac
 * @date 2022/5/2 00:06
 **/
public class test {
    public static void main(String[] args) {
        Process p;
        String[] cmds ={"/bin/bash","-c","blind_watermark --embed --pwd 1234 /Users/zymac/Desktop/SealBack/src/main/java/com/upc/sealback/utils/BlindWatermark/2.png '87fd7b5ee182f6a558f471c3a89e29f01' /Users/zymac/Desktop/SealBack/src/main/java/com/upc/sealback/utils/BlindWatermark/embedded.png"};
        String[] cmds2 ={"/bin/bash","-c","blind_watermark --extract --pwd 1234 --wm_shape 318 /Users/zymac/Desktop/SealBack/src/main/java/com/upc/sealback/utils/BlindWatermark/1651932011202.png"};
       // String cmds = "/bin/bash -c blind_watermark --embed --pwd 1234 /Users/zymac/Desktop/SealBack/src/main/java/com/upc/sealback/utils/BlindWatermark/2.png 'watermark text' /Users/zymac/Desktop/SealBack/src/main/java/com/upc/sealback/utils/BlindWatermark/embedded1.png";
        //String cmds = "ipconfig /all";
        try {
            //执行命令
            p = Runtime.getRuntime().exec(cmds2);
            //获取输出流，并包装到BufferedReader中
            BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream()));
            List<String> lines = new ArrayList<String>();
            String line = null;
            while((line = br.readLine()) != null) {
                lines.add(line);
                System.out.println(line);
            }
            int exitValue = p.waitFor();
            System.out.println("进程返回值：" + exitValue);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

}
