/*
 * Copyright (c) 2020 ww23(https://github.com/ww23/BlindWatermark).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.upc.sealback.utils.BlindWatermark.dencoder;
import com.upc.sealback.config.ResponseResult;
import com.upc.sealback.utils.BlindWatermark.converter.Converter;
import com.upc.sealback.utils.BlindWatermark.util.Utils;
import com.upc.sealback.utils.FileOSS;
import org.opencv.core.Mat;
import org.opencv.core.Rect;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import static org.opencv.core.Core.*;
import static org.opencv.core.CvType.CV_8S;
import static org.opencv.imgcodecs.Imgcodecs.imwrite;

/**
 * @author ww23
 */
public abstract class Encoder {

    Converter converter;

    Encoder(Converter converter) {
        this.converter = converter;
    }

    public Converter getConverter() {
        return converter;
    }

    public void setConverter(Converter converter) {
        this.converter = converter;
    }

    public ResponseResult<Object> encode(String image, String watermark) throws Exception {
        Mat src = Utils.read(image, CV_8S);
        List<Mat> channel = new ArrayList<>(4);
        List<Mat> newChannel = new ArrayList<>(4);
        split(src, channel);
        for (int i = 0; i < 3; i++) {
            Mat com = this.converter.start(channel.get(i)).clone();
            imwrite("com" + i + ".jpg", com);
            this.addWatermark(com, watermark);
            imwrite("newCom" + i + ".jpg", com);
            this.converter.inverse(com);
            newChannel.add(i, com);
            if(i == 2){
                newChannel.add(3,com);//增加透明背景
            }
            imwrite("channel" + i + ".jpg", com);
        }
        Mat res = new Mat();
        merge(newChannel, res);
        imwrite("src.jpg", src);
        if (res.rows() != src.rows() || res.cols() != src.cols()) {
            res = new Mat(res, new Rect(0, 0, src.width(), src.height()));
        }
        InputStream inputStream = Utils.handleToStream(res);
        return FileOSS.streamUploadOSS(inputStream);
    }

    public abstract void addWatermark(Mat com, String watermark);
}
