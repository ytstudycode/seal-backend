package com.upc.sealback.utils.BlindWatermark;

import com.upc.sealback.config.ResponseResult;
import com.upc.sealback.utils.BlindWatermark.converter.Converter;
import com.upc.sealback.utils.BlindWatermark.converter.DctConverter;
import com.upc.sealback.utils.BlindWatermark.converter.DftConverter;
import com.upc.sealback.utils.BlindWatermark.dencoder.Decoder;
import com.upc.sealback.utils.BlindWatermark.dencoder.Encoder;
import com.upc.sealback.utils.BlindWatermark.dencoder.ImageEncoder;
import com.upc.sealback.utils.BlindWatermark.dencoder.TextEncoder;
import org.bytedeco.javacpp.Loader;
import org.bytedeco.opencv.opencv_java;

/**
 * @description 生成盲水印与校验盲水印类
 * 原理:
 *     Encode:
 *         原图 --- 变换 ---> 变换域 + 水印 --- 逆变换 ---> 带水印图
 *     Decode:
 *         带水印图 --- 变换 ---> 变换域
 * @create zymac
 * @date 2022/4/6 13:08
 **/
public class Watermark {
    public static String commands =
            "   commands: \n" +
            "       encode <option> <original image> <watermark(水印)> <embedded image>\n" +
            "       decode <option> <original image> <embedded image>\n" +
            "   encode options: \n" +
            "       -c discrete cosine transform(离散余弦变换)\n" +
            "       -f discrete fourier transform (Deprecated)(离散傅里叶变换（已弃用）)\n" +
            "       -i image watermark(图像水印)\n" +
            "       -t text  watermark(文本水印)\n" +
            "   decode options: \n" +
            "       -c discrete cosine transform(离散余弦变换)\n" +
            "       -f discrete fourier transform (Deprecated)(离散傅里叶变换（已弃用）)\n" +
            "   example: \n" +
            "       encode -ct foo.png test bar.png" +
            "       decode -c  foo.png bar.png";

    private static final String FOURIER = "f";
    private static final String COSINE = "c";
    private static final String IMAGE = "i";
    private static final String TEXT = "t";

    /**
     * 生成盲水印
     * @param option 操作
     * @param fromPath 需要加水印的图片的路径
     * @param marks 水印内容
     * @return
     */
    // public static ResponseResult<Object> encodeWatermark(String fromPath, String marks) throws Exception {
     public static void main(String[] args) throws Exception {
        String option = "-ct";
        String fromPath = "/Users/zymac/Desktop/SealBack/src/main/java/com/upc/sealback/utils/BlindWatermark/1654169865334.png";
        String marks = "";
        String outPath = "/Users/zymac/Desktop/SealBack/src/main/java/com/upc/sealback/utils/BlindWatermark/middle.png";
        Loader.load(opencv_java.class);
        Converter converter = null;
        if (option.contains(FOURIER)) {
            converter = new DftConverter();
        } else if (option.contains(COSINE)) {
            converter = new DctConverter();
        }else {
            System.out.println(commands);
        }

        Encoder encoder = null;
        if (option.contains(IMAGE)) {
            encoder = new ImageEncoder(converter);
        } else if (option.contains(TEXT)) {
            encoder = new TextEncoder(converter);
        } else {
            System.out.println(commands);
        }
        assert encoder != null;
         encoder.encode(fromPath,marks);
    }

    /**
     * 获取盲水印中的相关信息
     * @param option
     * @param fromPath
     * @param outPath
     * @return
     */
    public static ResponseResult<Object> decodeWatermark(String option,String fromPath,String outPath){
    //public static void main(String[] args) {
        Loader.load(opencv_java.class);
        Converter converter = null;
        if (option.contains(FOURIER)) {
            converter = new DftConverter();
        } else if (option.contains(COSINE)) {
            converter = new DctConverter();
        }else {
            System.out.println(commands);
        }
        Decoder decoder = new Decoder(converter);
        decoder.decode(fromPath,outPath);
        return ResponseResult.getSuccessResult(outPath);
    }


}
