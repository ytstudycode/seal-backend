package com.upc.sealback.utils.BlindWatermark;

import com.upc.sealback.config.ResponseResult;
import com.upc.sealback.utils.FileOSS;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.upc.sealback.utils.FileUtils.deleteFile;

/**
 * @description 基于频域的数字盲水印
 * @create zymac
 * @date 2022/5/2 13:43
 **/
public class BlindWatermark {

    /**
     * 为图片增加盲水印
     * @param password 水印加密密码
     * @param inputFile 原图片路径
     * @param watermark 水印内容
     * @param outputFile 加水印后图片输出路径
     * @return
     */
    public static Map<String,String> addWatermark(String password,String inputFile,String watermark,String outputFile){
        Process p;
        String command = "blind_watermark --embed --pwd " +
                password +" "+
                inputFile +" "+
                watermark +" "+
                outputFile;
        String[] cmds ={"/bin/bash","-c",command};
        //String[] cmds2 ={"/bin/bash","-c","blind_watermark --extract --pwd 1234 --wm_shape 206 /Users/zymac/Desktop/SealBack/src/main/java/com/upc/sealback/utils/BlindWatermark/embedded.png"};
        try {
            //执行命令
            p = Runtime.getRuntime().exec(cmds);
            //获取输出流，并包装到BufferedReader中
            BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream()));
            List<String> lines = new ArrayList<String>();
            String line = null;
            while((line = br.readLine()) != null) {
                lines.add(line);
            }
            String size = lines.get(lines.size()-1).split(": ")[1];
            String filePath = outputFile.split("/static")[1];
            Map<String,String> map = new HashMap<String,String>();
            map.put("fileUrl",filePath);
            map.put("size",size);
            return map;
        } catch (IOException e) {
            e.printStackTrace();
        }  catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 获取图片的盲水印信息
     * @param password 解密密码
     * @param size
     * @param filePath
     * @return
     */
    public static ResponseResult<Object> decodeWatermark(String password,String size,String filePath){
        Process p;
        String command = "blind_watermark --extract --pwd " +
                password +" "+
                "--wm_shape " +
                size+" "+
                filePath;
        String[] cmds ={"/bin/bash","-c",command};
        //String[] cmds2 ={"/bin/bash","-c","blind_watermark --extract --pwd 1234 --wm_shape 206 /Users/zymac/Desktop/SealBack/src/main/java/com/upc/sealback/utils/BlindWatermark/embedded.png"};
        try {
            //执行命令
            p = Runtime.getRuntime().exec(cmds);
            //获取输出流，并包装到BufferedReader中
            BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream()));
            List<String> lines = new ArrayList<String>();
            String line = null;
            while((line = br.readLine()) != null) {
                lines.add(line);
            }
            deleteFile(filePath);
            int exitValue = p.waitFor();
            return ResponseResult.getSuccessResult(lines.get(lines.size()-1));
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }
}
