package com.upc.sealback.utils;

import java.util.UUID;

/**
 * @description
 */
public class UUIDUtils {

    public static String getUUID() {
        return UUID.randomUUID().toString().replaceAll("-","");
    }

}
