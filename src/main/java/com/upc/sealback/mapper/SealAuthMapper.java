package com.upc.sealback.mapper;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.upc.sealback.bean.SealAuth;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
* @author zymac
* @description 针对表【seal_auth(印章授权表)】的数据库操作Mapper
* @Entity sealback.bean.SealAuth
*/
@Mapper
public interface SealAuthMapper extends BaseMapper<SealAuth> {

    /**
     * 新增授权用户
     * @param sealAuth 授权用户信息
     * @return 是否新增成功的提示
     */
    public Integer addSealAuth(SealAuth sealAuth);

    /**
     * 根据uuid获取授权信息
     * @param uuid 自增主键
     * @return 授权信息
     */
    public SealAuth getSealAuthById(Integer uuid);

    /**
     * 根据授权印章id获取授权印章列表
     * @param authSealId 授权印章id
     * @return 授权印章列表
     */
    public List<SealAuth> getSealAuthBySealId(Page page, String authSealId);

    /**
     * 根据id获取授权印章数量
     * @param authSealId 授权印章id
     * @return
     */
    public Integer getSealAuthNumBySealId(String authSealId);

    /**
     * 根据uuid更新印章授权信息
     * @param sealAuth 自增主键
     * @return 是否更新成功的提示
     */
    public Integer updateSealAuthById(SealAuth sealAuth);

    /**
     * 删除印章授权信息
     * @param uuid 自增主键
     * @return 是否删除成功的提示
     */
    public Integer deleteSealAuth(Integer uuid);

    /**
     * 根据印章id删除授权信息
     * @param sealId 印章id
     * @return
     */
    public Integer deleteSealAuthBySealId(String sealId);


}




