package com.upc.sealback.mapper;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.upc.sealback.bean.SealAuth;
import com.upc.sealback.bean.SealCon;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
* @author zymac
* @description 针对表【seal_con(保存印章的使用情况)】的数据库操作Mapper
* @Entity com.upc.sealback.bean.SealCon
*/
@Mapper
public interface SealConMapper extends BaseMapper<SealCon> {

    /**
     * 增加一个印章
     * @param sealCon 新增的印章信息
     * @return 是否增加成功的提示
     */
    Integer addSealCon(SealCon sealCon);

    /**
     * 根据创建者获取印章
     * @param page 分页
     * @param sealCreater 印章创建人
     * @return 符合条件的印章列表
     */
    List<SealCon> getAllBySealCreater(Page page,String sealCreater);

    /**
     * 获取印章数量
     * @param sealCreater 印章创建人
     * @return
     */
    Integer getSealNum(String sealCreater);

    /**
     * 根据印章id删除印章
     * @param sealId 印章id
     * @return
     */
    Integer delBySealId(String sealId);

    /**
     * 更新印章授权数量
     * @param sealId 印章id
     * @return 是否更新成功的提示
     */
    Integer updateSealAuthorizeNum(String sealId,Integer sealAuthorizeNum);

    /**
     * 获取印章授权数量
     * @param sealId
     * @return
     */
    Integer getSealAuthorizeNumBySealId(String sealId);

    /**
     * 根据用户 分页联查 被授权的印章 table[seal_con,seal_auth]
     * @param authSealUser 被授权的用户
     * @return
     */
    List<SealCon> getSealAuth(Page page,String authSealUser);

    /**
     * 获取被授权的印章的数量
     * @param authSealUser
     * @return
     */
    Integer getSealAuthNum(String authSealUser);

    /**
     * 根据印章id更新印章使用
     * @param sealId
     * @param sealUseNum
     * @return
     */
    Integer updateSealUseNum(String sealId,Integer sealUseNum);

    /**
     * 根据id获取印章信息
     * @param sealId
     * @return
     */
    SealCon getSealById(String sealId);

}




