package com.upc.sealback.mapper;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.upc.sealback.bean.SealAntiFake;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
* @author zymac
* @description 针对表【seal_anti_fake(保存生成的防伪电子印章详情)】的数据库操作Mapper
* @createDate 2022-05-03 20:41:49
* @Entity com.upc.sealback.bean.SealAntiFake
*/
@Mapper
public interface SealAntiFakeMapper extends BaseMapper<SealAntiFake> {

    /**
     * 新增防伪电子印章
     * @param sealAntiFake 防伪电子印章详情
     * @return 是否新增成功的提示
     */
    Integer addSealAntiFake(SealAntiFake sealAntiFake);

    /**
     * 根据防伪电子印章id获取电子印章信息
     * @param antiFakeSealId
     * @return
     */
    SealAntiFake getAntiFakeSealById(String antiFakeSealId);

    /**
     * 根据模板印章id和使用用途获取使用列表
     * @param page 分页
     * @param antiFakeSealTemplateId 模板印章id
     * @param antiFakePurpose 使用用途
     * @return
     */
    List<SealAntiFake> getAntiFakeSealListByTemplateId(Page page, String antiFakeSealTemplateId, Integer antiFakePurpose);

    /**
     * 根据模板印章id和使用用途获取使用数量
     * @param antiFakeSealTemplateId
     * @param antiFakePurpose
     * @return
     */
    Integer getAntiFakeSealNumByTemplateId(String antiFakeSealTemplateId, Integer antiFakePurpose);

    /**
     * 根据签署人获取签署信息
     * @return
     */
    List<SealAntiFake> getSignDetailByUser(Page page,String antiFakeUser,Integer antiFakePurpose);

    /**
     * 根据签署人获取签署信息数量
     * @param antiFakeUser 用户
     * @param antiFakePurpose 用途
     * @return
     */
    Integer getSignDetailNumByUser(String antiFakeUser,Integer antiFakePurpose);


    /**
     * 根据id获取电子印章使用信息
     * @param antiFakeSealId
     * @param antiFakePurpose
     * @return
     */
    SealAntiFake getAntiFakeDetail(String antiFakeSealId,Integer antiFakePurpose);

}




