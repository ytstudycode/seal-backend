package com.upc.sealback.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.upc.sealback.bean.SealUser;
import org.apache.ibatis.annotations.Mapper;

import java.util.Date;
import java.util.List;

/**
 * @author zymac
 * @description 针对表【seal_user】的数据库操作Mapper
 * @createDate 2022-04-02 00:55:50
 * @Entity com.upc.sealback.bean.SealUser
 */
@Mapper
public interface SealUserMapper extends BaseMapper<SealUser> {

    /**
     * 新增用户（注册用户）
     * @param sealUser 用户信息
     * @return 是否注册成功的提示
     */
    Integer addUser(SealUser sealUser);

    /**
     * 根据用户id获取用户信息
     * @param userId 用户Id
     * @return 用户信息
     */
    SealUser getUserByUserId(String userId);

    /**
     * 根据用户名获取用户信息
     * @param userName 用户名
     * @return 用户信息
     */
    SealUser getUserByName(String userName);

    /**
     * 更新用户信息
     * @param userId 用户id
     * @param userPassword 用户信息
     * @return 是否更新成功的提示
     */
    Integer updatePassword(String userId, String userPassword, Date updateTime);

    /**
     * 更新用户的邮箱
     * @param userId 用户ID
     * @param userEmail 用户邮箱
     * @param updateTime 更新时间
     * @return 是否更新成功的提示
     */
    Integer updateUserEmail(String userId, String userEmail, Date updateTime);

    /**
     * 更新用户信息
     * @return 是否更新成功的提示
     */
    Integer updateUser(SealUser sealUser);

    /**
     * 通过用户名或者手机号搜索用户信息
     * @param content 用户名
     * @return 符合条件的用户信息
     */
    List<SealUser> getSealUserBySearch(Page page,String content);
}




