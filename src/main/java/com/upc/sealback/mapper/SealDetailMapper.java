package com.upc.sealback.mapper;

import com.upc.sealback.bean.SealDetail;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
* @author zymac
* @description 针对表【seal_detail(保存每一个印章的详情)】的数据库操作Mapper
* @Entity com.upc.sealback.bean.SealDetail
*/
@Mapper
public interface SealDetailMapper extends BaseMapper<SealDetail> {

    /**
     * 新增印章详细信息
     * @param sealDetail 印章信息
     * @return 是否新增成功的提示
     */
    public Integer addSealDetail(SealDetail sealDetail);

    /**
     * 根据印章id删除印章
     * @param sealId 印章id
     * @return
     */
    public Integer delBySealId(String sealId);

    /**
     * 根据ID获取印章详细信息
     * @param sealId 印章id
     * @return 印章信息
     */
    public SealDetail getSealDetailById(String sealId);


}




