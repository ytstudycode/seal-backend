package com.upc.sealback.service;

import com.upc.sealback.bean.SealDetail;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author zymac
* @description 针对表【seal_detail(保存每一个印章的详情)】的数据库操作Service
* @createDate 2022-04-03 17:03:51
*/
public interface SealDetailService extends IService<SealDetail> {

    /**
     * 新增印章详细信息
     * @param sealDetail 印章信息
     * @return 是否新增成功的提示
     */
    Integer addSealDetail(SealDetail sealDetail);

    /**
     * 根据印章id删除印章
     * @param sealId 印章id
     * @return
     */
    Integer delBySealId(String sealId);

    /**
     * 根据ID获取印章详细信息
     * @param sealId 印章id
     * @return 印章信息
     */
    SealDetail getSealDetailById(String sealId);

    /**
     * 根据模板印章生成新的印章
     * @param templateSealId
     * @return
     * @throws Exception
     */
    String getSealByTemplateSeal(String templateSealId) throws Exception;
}
