package com.upc.sealback.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.upc.sealback.bean.SealSign;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Date;
import java.util.List;

/**
* @author zymac
* @description 针对表【seal_sign】的数据库操作Service
* @createDate 2022-04-13 22:59:32
*/
public interface SealSignService extends IService<SealSign> {

    /**
     * 新增文件签署信息
     * @param sealSign 文件信息
     * @return 是否新增成功的提示
     */
    Integer addSealSign(SealSign sealSign);


    /**
     * 根据任务发起人获取任务信息
     * @param page 分页
     * @param signSponsor 任务发起人
     * @return 任务信息
     */
    List<SealSign> getSealSignBySignSponsor(Page page, String signSponsor);

    /**
     * 根据任务发起人获取任务数量
     * @param signSponsor 任务发起人
     * @return 任务数量
     */
    Integer getSealSignNumBySignSponsor(String signSponsor);

    /**
     * 根据发起人或者任务信息搜索任务信息(不包含草稿)
     * @param page 分页
     * @param signSponsor 任务发起人
     * @param content 搜索内容
     * @return
     */
    List<SealSign> getSealSignBySearchSignSponsor(Page page,String signSponsor,String content);

    /**
     * 根据发起人或者任务信息获取搜索到的任务数量
     * @param signSponsor 任务发起人
     * @param content 搜索内容
     * @return
     */
    Integer getSealSignNumBySearchSignSponsor(String signSponsor,String content);

    /**
     * 根据参与人获取任务信息
     * @param page 分页
     * @param signParticipant 任务参与人
     * @return 任务信息
     */
    List<SealSign> getSealSignBySignParticipant(Page page, String signParticipant);

    /**
     * 根据参与人获取任务数量
     * @param signParticipant 任务参与人
     * @return 任务数量
     */
    Integer getSealSignNumBySignParticipant(String signParticipant);

    /**
     * 根据参与人或者任务信息搜索任务信息
     * @param page 分页
     * @param signParticipant 参与人
     * @param content 搜索内容
     * @return
     */
    List<SealSign> getSealSignBySearchParticipant(Page page, String signParticipant, String content);

    /**
     * 根据参与人或者任务信息获取任务数量
     * @param signParticipant 参与人
     * @param content 搜索内容
     * @return
     */
    Integer getSealSignNumBySearchParticipant(String signParticipant, String content);

    /**
     * 删除签署任务
     * @param signId
     * @return 是否删除成功的提示
     */
    Integer deleteSign(String signId);

    /**
     * 根据签署任务的状态获取任务列表
     * @param page 分页
     * @param signSponsor 任务发起人
     * @param signStatus 任务状态
     * @return
     */
    List<SealSign> getSealSignByStatus(Page page,String signSponsor,Integer signStatus);

    /**
     * 根据签署任务的状态获取任务列表数量
     * @param signSponsor 任务发起人
     * @param signStatus 任务状态
     * @return
     */
    Integer getSealSignNumByStatus(String signSponsor,Integer signStatus);

    /**
     * 根据签署任务的状态搜索符合条件的任务列表
     * @param page
     * @param signSponsor
     * @param signStatus
     * @param content
     * @return
     */
    List<SealSign> getSealSignBySearchStatus(Page page,String signSponsor,Integer signStatus,String content);

    /**
     * 根据签署任务的状态搜索符合条件的任务数量
     * @param signSponsor
     * @param signStatus
     * @param content
     * @return
     */
    Integer getSealSignNumBySearchStatus(String signSponsor,Integer signStatus,String content);

    /**
     * 获取待我操作的任务
     * @param page 分页
     * @param signParticipant 待操作人
     * @return
     */
    List<SealSign> getSealSignOperatedByMe(Page page,String signParticipant);

    /**
     * 获取待我操作的任务的数量
     * @param signParticipant 待操作人
     * @return
     */
    Integer getSealSignNumOperatedByMe(String signParticipant);

    /**
     * 搜索待我操作的任务
     * @param page 分页
     * @param signParticipant 待操作人
     * @param content 搜索内容
     * @return
     */
    List<SealSign> getSealSignOperatedByMeBySearch(Page page,String signParticipant,String content);

    /**
     * 获取待我操作的任务的数量
     * @param signParticipant 待操作人
     * @param content 搜索内容
     * @return
     */
    Integer getSealSignNumOperatedByMeBySearch(String signParticipant,String content);

    /**
     * 根据id获取签署信息详情
     * @param uuid
     * @return
     */
    SealSign getSealSignDetail(Integer uuid);

    /**
     * 根据文件获取签署详情
     * @param signFilePath 签署文件路径
     * @return
     */
    SealSign getSealSignByFileUrl(String signFilePath);

    /**
     * 更新印章签署信息
     * @param sealSign
     * @return
     */
    Integer updateSealSign(SealSign sealSign);

    /**
     * 根据任务id获取任务详情
     * @param signId
     * @return
     */
    SealSign getSealSignBySignId(String signId);

    /**
     * 更新印章签署状态
     * @param signId
     * @param status
     * @return
     */
    Integer updateSealSignStatus(String signId, Integer status, Date date);

    /**
     * 更新印章基础信息
     * @param sealSign
     * @return
     */
    Integer updateSignBasic(SealSign sealSign);

    /**
     * 获取用户最近签署的5条数据
     * @param signParticipant 当前用户
     * @return
     */
    List<SealSign> getSealSignLately(String signParticipant);
}
