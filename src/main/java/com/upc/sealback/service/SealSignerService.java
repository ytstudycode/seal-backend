package com.upc.sealback.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.upc.sealback.bean.SealSign;
import com.upc.sealback.bean.SealSigner;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
* @author zymac
* @description 针对表【seal_signer(文件签署-签署人表)】的数据库操作Service
* @createDate 2022-05-09 15:55:06
*/
public interface SealSignerService extends IService<SealSigner> {

    /**
     * 新增签署人
     * @param sealSignerList 签署人列表
     * @return
     */
    Integer addSealSigner(List<SealSigner> sealSignerList);

    /**
     * 根据签署信息获取签署人
     * @param signerSignId
     * @return
     */
    List<SealSigner> getSealSignerBySignId(String signerSignId);


    /**
     * 将签署人信息放到签署文件信息中
     * @param sealSignList
     * @return
     */
    List<SealSign> getUserNameById(List<SealSign> sealSignList);

    /**
     * 根据签署人和签署文件id获取签署状态信息
     * @param signerId 签署人
     * @param signerSignId 签署id
     * @return
     */
    SealSigner getSignerBySignerId(String signerId,String signerSignId);

    /**
     * 更新查看时间
     * @param sealSigner
     * @return
     */
    Integer updateSignerViewTime(SealSigner sealSigner);

    /**
     * 更新签署信息
     * @param sealSigner
     * @return
     */
    Integer updateSign(SealSigner sealSigner);

    /**
     * 批量更新用户签署位置信息
     * @param sealSignerList
     * @return
     */
    Integer updateSignPosition(List<SealSigner> sealSignerList);

    /**
     * 获取待我操作的
     * @param sealSignerId 操作人id
     * @return
     */
    List<SealSigner> getSealSignOperatedByMe(Page page,String sealSignerId);

    /**
     * 获取待我操作的签署文件的数量
     * @param sealSignerId
     * @return
     */
    Integer getSealSignNumOperatedByMe(String sealSignerId);

    /**
     * 搜索待我操作的任务
     * @param page 分页
     * @param signParticipant 待操作人
     * @param content 搜索内容
     * @return
     */
    List<SealSigner> getSealSignOperatedByMeBySearch(Page page, String signParticipant, String content);

    /**
     * 获取待我操作的任务的数量
     * @param signParticipant 待操作人
     * @param content 搜索内容
     * @return
     */
    Integer getSealSignNumOperatedByMeBySearch(String signParticipant,String content);

    /**
     * 删除该任务的所有签署人信息
     * @param signerSignId 任务id
     * @return
     */
    Integer deleteSigner(String signerSignId);
}
