package com.upc.sealback.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.upc.sealback.bean.SealAuth;
import com.upc.sealback.service.SealAuthService;
import com.upc.sealback.mapper.SealAuthMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
* @author zymac
* @description 针对表【seal_auth(印章授权表)】的数据库操作Service实现
* @createDate 2022-04-09 15:05:39
*/
@Service
public class SealAuthServiceImpl extends ServiceImpl<SealAuthMapper, SealAuth>
    implements SealAuthService{
    @Autowired(required=false)
    SealAuthMapper sealAuthMapper;

    @Override
    public Integer addSealAuth(SealAuth sealAuth) {
        return sealAuthMapper.addSealAuth(sealAuth);
    }

    @Override
    public SealAuth getSealAuthById(Integer uuid) {
        return sealAuthMapper.getSealAuthById(uuid);
    }

    @Override
    public List<SealAuth> getSealAuthBySealId(Page page,String authSealId) {
        return sealAuthMapper.getSealAuthBySealId(page,authSealId);
    }

    @Override
    public Integer getSealAuthNumBySealId(String authSealId) {
        return sealAuthMapper.getSealAuthNumBySealId(authSealId);
    }

    @Override
    public Integer updateSealAuthById(SealAuth sealAuth) {
        return sealAuthMapper.updateSealAuthById(sealAuth);
    }

    @Override
    public Integer deleteSealAuth(Integer uuid) {
        return sealAuthMapper.deleteSealAuth(uuid);
    }

    @Override
    public Integer deleteSealAuthBySealId(String sealId) {
        return sealAuthMapper.deleteSealAuthBySealId(sealId);
    }
}




