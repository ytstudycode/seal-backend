package com.upc.sealback.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.upc.sealback.bean.PrivateSeal.PrivateSealDTO;
import com.upc.sealback.bean.PublicSeal.PublicSealDTO;
import com.upc.sealback.bean.SealDetail;
import com.upc.sealback.config.ResponseResult;
import com.upc.sealback.service.SealDetailService;
import com.upc.sealback.mapper.SealDetailMapper;
import com.upc.sealback.utils.PrivateSeal.GeneratePrivateSeal;
import com.upc.sealback.utils.PrivateSeal.rule.PrivateSealBuilder;
import com.upc.sealback.utils.PublicSeal.GeneratePublicSeal;
import com.upc.sealback.utils.PublicSeal.rule.PublicSealBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

import static com.upc.sealback.utils.FontUtils.SEAL_SAVE_PATH;

/**
* @author zymac
* @description 针对表【seal_detail(保存每一个印章的详情)】的数据库操作Service实现
* @createDate 2022-04-03 17:03:51
*/
@Service
public class SealDetailServiceImpl extends ServiceImpl<SealDetailMapper, SealDetail>
    implements SealDetailService{
    @Autowired(required=false)
    SealDetailMapper sealDetailMapper;

    @Override
    public Integer addSealDetail(SealDetail sealDetail) {
        return sealDetailMapper.addSealDetail(sealDetail);
    }

    @Override
    public Integer delBySealId(String sealId) {
        return sealDetailMapper.delBySealId(sealId);
    }

    @Override
    public SealDetail getSealDetailById(String sealId) {
        return sealDetailMapper.getSealDetailById(sealId);
    }

    /**
     * 根据模板印章生成新的印章
     *
     * @param templateSealId
     * @return
     * @throws Exception
     */
    @Override
    public String getSealByTemplateSeal(String templateSealId) throws Exception {
        //按已生成的印章模板，从数据库中查找该模板的印章属性，以生成一个新的印章
        SealDetail sealDetail = sealDetailMapper.getSealDetailById(templateSealId);
        String path = "";
        if(sealDetail.getSealType() == 0){ //个人印章
            ResponseResult<Object> responseResult = GeneratePrivateSeal.generatePrivateSeal(sealDetail);
            Map<String,Object> map = (Map<String, Object>) responseResult.getResult();
            PrivateSealBuilder psb = (PrivateSealBuilder) map.get("psb");
            PrivateSealDTO dto = (PrivateSealDTO) map.get("dto");
            path = psb.handleToLocal(dto); //生成印章并存储到本地
        }else { //企业印章
            ResponseResult<Object> responseResult = GeneratePublicSeal.generatePublicSeal(sealDetail);
            Map<String,Object> map = (Map<String, Object>) responseResult.getResult();
            PublicSealBuilder psb = (PublicSealBuilder) map.get("psb");
            PublicSealDTO dto = (PublicSealDTO) map.get("dto");
            path = psb.handleToLocal(dto); //生成印章并存储到本地
        }
        String fullPath = SEAL_SAVE_PATH + path; //获得完整路径
        return fullPath;
    }
}




