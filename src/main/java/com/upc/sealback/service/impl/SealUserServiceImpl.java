package com.upc.sealback.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.upc.sealback.bean.SealUser;
import com.upc.sealback.mapper.SealUserMapper;
import com.upc.sealback.service.SealUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * @author zymac
 * @description 针对表【seal_user】的数据库操作Service实现
 * @createDate 2022-04-02 00:55:50
 */
@Service
public class SealUserServiceImpl extends ServiceImpl<SealUserMapper, SealUser> implements SealUserService {
    @Autowired(required=false)
    SealUserMapper sealUserMapper;


    @Override
    public Integer addUser(SealUser sealUser) {
        return sealUserMapper.addUser(sealUser);
    }

    @Override
    public SealUser getUserByUserId(String userId) {
        return sealUserMapper.getUserByUserId(userId);
    }

    @Override
    public SealUser getUserByName(String userName) {
        return sealUserMapper.getUserByName(userName);
    }

    @Override
    public Integer updatePassword(String userId, String userPassword, Date updateTime) {
        return sealUserMapper.updatePassword(userId, userPassword,updateTime);
    }

    @Override
    public Integer updateUserEmail(String userId, String userEmail, Date updateTime) {
        return sealUserMapper.updateUserEmail(userId,userEmail,updateTime);
    }

    @Override
    public Integer updateUser(SealUser sealUser) {
        return sealUserMapper.updateUser(sealUser);
    }

    @Override
    public List<SealUser> getSealUserBySearch(Page page,String content) {
        return sealUserMapper.getSealUserBySearch(page,content);
    }

}




