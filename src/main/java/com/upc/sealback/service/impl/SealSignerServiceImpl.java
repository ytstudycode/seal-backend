package com.upc.sealback.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.upc.sealback.bean.SealSign;
import com.upc.sealback.bean.SealSigner;
import com.upc.sealback.service.SealSignerService;
import com.upc.sealback.mapper.SealSignerMapper;
import com.upc.sealback.service.SealUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

import static com.upc.sealback.utils.StringUtil.strIsEmpty;

/**
* @author zymac
* @description 针对表【seal_signer(文件签署-签署人表)】的数据库操作Service实现
* @createDate 2022-05-09 15:55:06
*/
@Service
public class SealSignerServiceImpl extends ServiceImpl<SealSignerMapper, SealSigner>
    implements SealSignerService{
    @Autowired(required=false)
    SealSignerMapper sealSignerMapper;

    @Autowired
    SealUserService sealUserService;


    /**
     * 新增签署人
     *
     * @param sealSignerList 签署人列表
     * @return
     */
    @Override
    public Integer addSealSigner(List<SealSigner> sealSignerList) {
        return sealSignerMapper.addSealSigner(sealSignerList);
    }

    /**
     * 根据签署信息获取签署人
     *
     * @param signerSignId
     * @return
     */
    @Override
    public List<SealSigner> getSealSignerBySignId(String signerSignId) {
        return sealSignerMapper.getSealSignerBySignId(signerSignId);
    }

    /**
     * 将签署人信息放到签署文件信息中
     *
     * @param sealSignList
     * @return
     */
    @Override
    public List<SealSign> getUserNameById(List<SealSign> sealSignList) {
        for (SealSign sealSign : sealSignList){
            // 将发起人id 替换成 姓名
            if(!strIsEmpty(sealSign.getSignSponsor())){
                sealSign.setSignSponsor(sealUserService.getUserByUserId(sealSign.getSignSponsor()).getUserName());
            }

            // 获取该签署信息的所有参与人信息
            List<SealSigner> sealSignerList = sealSignerMapper.getSealSignerBySignId(sealSign.getSignId());

            // 将参与人id 替换成 姓名
            for(SealSigner sealSigner : sealSignerList){
                if(!strIsEmpty(sealSigner.getSignerId())){
                    sealSigner.setSignerId(sealUserService.getUserByUserId(sealSigner.getSignerId()).getUserName());
                }
            }

            //将签署人信息放到签署文件信息中
            sealSign.setSealSigner(sealSignerList);
        }
        return sealSignList;
    }

    /**
     * 根据签署人和签署文件id获取签署状态信息
     *
     * @param signerId     签署人
     * @param signerSignId 签署id
     * @return
     */
    @Override
    public SealSigner getSignerBySignerId(String signerId, String signerSignId) {
        return sealSignerMapper.getSignerBySignerId(signerId,signerSignId);
    }

    /**
     * 更新查看时间
     *
     * @param sealSigner
     * @return
     */
    @Override
    public Integer updateSignerViewTime(SealSigner sealSigner) {
        sealSigner.setUpdateTime(new Date());
        return sealSignerMapper.updateSignerViewTime(sealSigner);
    }

    /**
     * 更新签署信息
     *
     * @param sealSigner
     * @return
     */
    @Override
    public Integer updateSign(SealSigner sealSigner) {
        sealSigner.setUpdateTime(new Date());
        return sealSignerMapper.updateSign(sealSigner);
    }

    /**
     * 批量更新用户签署位置信息
     *
     * @param sealSignerList
     * @return
     */
    @Override
    public Integer updateSignPosition(List<SealSigner> sealSignerList) {
        for (SealSigner sealSigner: sealSignerList){
            sealSigner.setUpdateTime(new Date());
            sealSigner.setSignerId(sealUserService.getUserByName(sealSigner.getSignerId()).getUserId());
        }
        return sealSignerMapper.updateSignPosition(sealSignerList);
    }

    /**
     * 获取待我操作的
     *
     * @param sealSignerId 操作人id
     * @return
     */
    @Override
    public List<SealSigner> getSealSignOperatedByMe(Page page, String sealSignerId) {
        return sealSignerMapper.getSealSignOperatedByMe(page,sealSignerId);
    }

    /**
     * 获取待我操作的签署文件的数量
     *
     * @param sealSignerId
     * @return
     */
    @Override
    public Integer getSealSignNumOperatedByMe(String sealSignerId) {
        return sealSignerMapper.getSealSignNumOperatedByMe(sealSignerId);
    }

    /**
     * 搜索待我操作的任务
     *
     * @param page            分页
     * @param signParticipant 待操作人
     * @param content         搜索内容
     * @return
     */
    @Override
    public List<SealSigner> getSealSignOperatedByMeBySearch(Page page, String signParticipant, String content) {
        return sealSignerMapper.getSealSignOperatedByMeBySearch(page,signParticipant,content);
    }

    /**
     * 获取待我操作的任务的数量
     *
     * @param signParticipant 待操作人
     * @param content         搜索内容
     * @return
     */
    @Override
    public Integer getSealSignNumOperatedByMeBySearch(String signParticipant, String content) {
        return sealSignerMapper.getSealSignNumOperatedByMeBySearch(signParticipant,content);
    }

    /**
     * 删除该任务的所有签署人信息
     *
     * @param signerSignId 任务id
     * @return
     */
    @Override
    public Integer deleteSigner(String signerSignId) {
        return sealSignerMapper.deleteSigner(signerSignId);
    }
}




