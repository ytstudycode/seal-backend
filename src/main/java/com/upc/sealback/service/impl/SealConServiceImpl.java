package com.upc.sealback.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.upc.sealback.bean.SealAuth;
import com.upc.sealback.bean.SealCon;
import com.upc.sealback.service.SealConService;
import com.upc.sealback.mapper.SealConMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
* @author zymac
* @description 针对表【seal_con(保存印章的使用情况)】的数据库操作Service实现
* @createDate 2022-04-03 17:00:26
*/
@Service
public class SealConServiceImpl extends ServiceImpl<SealConMapper, SealCon>
    implements SealConService{
    @Autowired(required=false)
    SealConMapper sealConMapper;

    @Override
    public Integer addSealCon(SealCon sealCon) {
        return sealConMapper.addSealCon(sealCon);
    }

    @Override
    public List<SealCon> getAllBySealCreater(Page page, String sealCreater) {
        return sealConMapper.getAllBySealCreater(page,sealCreater);
    }

    @Override
    public Integer getSealNum(String sealCreater) {
        return sealConMapper.getSealNum(sealCreater);
    }

    @Override
    public Integer delBySealId(String sealId) {
        return sealConMapper.delBySealId(sealId);
    }

    @Override
    public Integer updateSealAuthorizeNum(String sealId,Integer sealAuthorizeNum) {
        return sealConMapper.updateSealAuthorizeNum(sealId,sealAuthorizeNum);
    }

    @Override
    public Integer getSealAuthorizeNumBySealId(String sealId) {
        return sealConMapper.getSealAuthorizeNumBySealId(sealId);
    }

    @Override
    public List<SealCon> getSealAuth(Page page,String authSealUser) {
        return sealConMapper.getSealAuth(page,authSealUser);
    }

    @Override
    public Integer getSealAuthNum(String authSealUser) {
        return sealConMapper.getSealAuthNum(authSealUser);
    }

    @Override
    public Integer updateSealUseNum(String sealId, Integer sealUseNum) {
        return sealConMapper.updateSealUseNum(sealId,sealUseNum);
    }

    @Override
    public SealCon getSealById(String sealId) {
        return sealConMapper.getSealById(sealId);
    }
}




