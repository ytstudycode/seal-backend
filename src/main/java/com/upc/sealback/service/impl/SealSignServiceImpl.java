package com.upc.sealback.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.upc.sealback.bean.SealSign;
import com.upc.sealback.bean.SealSigner;
import com.upc.sealback.service.SealSignService;
import com.upc.sealback.mapper.SealSignMapper;
import com.upc.sealback.service.SealUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

import static com.upc.sealback.utils.StringUtil.strIsEmpty;

/**
* @author zymac
* @description 针对表【seal_sign】的数据库操作Service实现
* @createDate 2022-04-13 22:59:31
*/
@Service
public class SealSignServiceImpl extends ServiceImpl<SealSignMapper, SealSign>
    implements SealSignService{
    @Autowired(required=false)
    SealSignMapper sealSignMapper;

    @Override
    public Integer addSealSign(SealSign sealSign) {
        return sealSignMapper.addSealSign(sealSign);
    }

    @Override
    public List<SealSign> getSealSignBySignSponsor(Page page, String signSponsor) {
        return sealSignMapper.getSealSignBySignSponsor(page,signSponsor);
    }

    @Override
    public Integer getSealSignNumBySignSponsor(String signSponsor) {
        return sealSignMapper.getSealSignNumBySignSponsor(signSponsor);
    }

    @Override
    public List<SealSign> getSealSignBySearchSignSponsor(Page page, String signSponsor, String content) {
        return sealSignMapper.getSealSignBySearchSignSponsor(page,signSponsor,content);
    }

    @Override
    public Integer getSealSignNumBySearchSignSponsor(String signSponsor, String content) {
        return sealSignMapper.getSealSignNumBySearchSignSponsor(signSponsor,content);
    }

    @Override
    public List<SealSign> getSealSignBySignParticipant(Page page, String signParticipant) {
        return sealSignMapper.getSealSignBySignParticipant(page,signParticipant);
    }

    @Override
    public Integer getSealSignNumBySignParticipant(String signParticipant) {
        return sealSignMapper.getSealSignNumBySignParticipant(signParticipant);
    }

    @Override
    public List<SealSign> getSealSignBySearchParticipant(Page page, String signParticipant, String content) {
        return sealSignMapper.getSealSignBySearchParticipant(page,signParticipant,content);
    }

    @Override
    public Integer getSealSignNumBySearchParticipant(String signParticipant, String content) {
        return sealSignMapper.getSealSignNumBySearchParticipant(signParticipant,content);
    }

    @Override
    public Integer deleteSign(String uuid) {
        return sealSignMapper.deleteSign(uuid);
    }

    @Override
    public List<SealSign> getSealSignByStatus(Page page, String signSponsor, Integer signStatus) {
        return sealSignMapper.getSealSignByStatus(page,signSponsor,signStatus);
    }

    @Override
    public Integer getSealSignNumByStatus(String signSponsor, Integer signStatus) {
        return sealSignMapper.getSealSignNumByStatus(signSponsor,signStatus);
    }

    @Override
    public List<SealSign> getSealSignBySearchStatus(Page page, String signSponsor, Integer signStatus, String content) {
        return sealSignMapper.getSealSignBySearchStatus(page,signSponsor,signStatus,content);
    }

    @Override
    public Integer getSealSignNumBySearchStatus(String signSponsor, Integer signStatus, String content) {
        return sealSignMapper.getSealSignNumBySearchStatus(signSponsor,signStatus,content);
    }

    @Override
    public List<SealSign> getSealSignOperatedByMe(Page page, String signParticipant) {
        return sealSignMapper.getSealSignOperatedByMe(page,signParticipant);
    }

    @Override
    public Integer getSealSignNumOperatedByMe(String signParticipant) {
        return sealSignMapper.getSealSignNumOperatedByMe(signParticipant);
    }

    @Override
    public List<SealSign> getSealSignOperatedByMeBySearch(Page page, String signParticipant, String content) {
        return sealSignMapper.getSealSignOperatedByMeBySearch(page,signParticipant,content);
    }

    @Override
    public Integer getSealSignNumOperatedByMeBySearch(String signParticipant, String content) {
        return sealSignMapper.getSealSignNumOperatedByMeBySearch(signParticipant,content);
    }

    @Override
    public SealSign getSealSignDetail(Integer uuid) {
        return sealSignMapper.getSealSignDetail(uuid);
    }

    @Override
    public SealSign getSealSignByFileUrl(String signFilePath) {
        return sealSignMapper.getSealSignByFileUrl(signFilePath);
    }

    @Override
    public Integer updateSealSign(SealSign sealSign) {
        sealSign.setUpdateTime(new Date());
        return sealSignMapper.updateSealSign(sealSign);
    }

    @Override
    public SealSign getSealSignBySignId(String signId) {
        return sealSignMapper.getSealSignBySignId(signId);
    }

    @Override
    public Integer updateSealSignStatus(String signId, Integer status,Date date) {
        return sealSignMapper.updateSealSignStatus(signId,status,date);
    }

    @Override
    public Integer updateSignBasic(SealSign sealSign) {
        sealSign.setUpdateTime(new Date());
        return sealSignMapper.updateSignBasic(sealSign);
    }

    @Override
    public List<SealSign> getSealSignLately(String signParticipant) {
        return sealSignMapper.getSealSignLately(signParticipant);
    }
}




