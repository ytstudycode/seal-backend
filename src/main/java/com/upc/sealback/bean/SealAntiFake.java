package com.upc.sealback.bean;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

import lombok.Data;

/**
 * 保存生成的防伪电子印章详情
 * @TableName seal_anti_fake
 */
@TableName(value ="seal_anti_fake")
@Data
public class SealAntiFake implements Serializable {

    private List<SealSigner> sealSigner;

    private SealCon sealCon;

    private SealSign sealSign;

    /**
     * 自增主键
     */
    @TableId(value = "uuid", type = IdType.AUTO)
    private Integer uuid;

    /**
     * 防伪电子印章id
     */
    @TableField(value = "anti_fake_seal_id")
    private String antiFakeSealId;

    /**
     * 生成电子印章的模板id
     */
    @TableField(value = "anti_fake_seal_template_id")
    private String antiFakeSealTemplateId;

    /**
     * 使用该电子印章的任务id
     */
    @TableField(value = "anti_fake_sign_id")
    private String antiFakeSignId;

    /**
     * 生成电子印章的用户
     */
    @TableField(value = "anti_fake_user")
    private String antiFakeUser;

    /**
     * 对该电子印章盲水印加密和解密的密码
     */
    @TableField(value = "anti_fake_password")
    private String antiFakePassword;

    /**
     * 生成的盲水印大小
     */
    @TableField(value = "anti_fake_size")
    private Integer antiFakeSize;

    /**
     * 生成的电子印章路径
     */
    @TableField(value = "anti_fake_path")
    private String antiFakePath;

    /**
     * 生成电子印章用途(0:导出电子印章;1:签署文件)
     */
    @TableField(value = "anti_fake_purpose")
    private Integer antiFakePurpose;

    /**
     * 生成电子印章的说明
     */
    @TableField(value = "anti_fake_introduction")
    private String antiFakeIntroduction;

    /**
     * 生成电子印章的时间
     */
    @TableField(value = "anti_fake_time")
    private Date antiFakeTime;

    /**
     * 创建时间
     */
    @TableField(value = "create_time")
    private Date createTime;

    /**
     * 修改时间
     */
    @TableField(value = "update_time")
    private Date updateTime;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        SealAntiFake other = (SealAntiFake) that;
        return (this.getUuid() == null ? other.getUuid() == null : this.getUuid().equals(other.getUuid()))
            && (this.getAntiFakeSealId() == null ? other.getAntiFakeSealId() == null : this.getAntiFakeSealId().equals(other.getAntiFakeSealId()))
            && (this.getAntiFakeSealTemplateId() == null ? other.getAntiFakeSealTemplateId() == null : this.getAntiFakeSealTemplateId().equals(other.getAntiFakeSealTemplateId()))
            && (this.getAntiFakeSignId() == null ? other.getAntiFakeSignId() == null : this.getAntiFakeSignId().equals(other.getAntiFakeSignId()))
            && (this.getAntiFakeUser() == null ? other.getAntiFakeUser() == null : this.getAntiFakeUser().equals(other.getAntiFakeUser()))
            && (this.getAntiFakePassword() == null ? other.getAntiFakePassword() == null : this.getAntiFakePassword().equals(other.getAntiFakePassword()))
            && (this.getAntiFakeSize() == null ? other.getAntiFakeSize() == null : this.getAntiFakeSize().equals(other.getAntiFakeSize()))
            && (this.getAntiFakePath() == null ? other.getAntiFakePath() == null : this.getAntiFakePath().equals(other.getAntiFakePath()))
            && (this.getAntiFakePurpose() == null ? other.getAntiFakePurpose() == null : this.getAntiFakePurpose().equals(other.getAntiFakePurpose()))
            && (this.getAntiFakeIntroduction() == null ? other.getAntiFakeIntroduction() == null : this.getAntiFakeIntroduction().equals(other.getAntiFakeIntroduction()))
            && (this.getAntiFakeTime() == null ? other.getAntiFakeTime() == null : this.getAntiFakeTime().equals(other.getAntiFakeTime()))
            && (this.getCreateTime() == null ? other.getCreateTime() == null : this.getCreateTime().equals(other.getCreateTime()))
            && (this.getUpdateTime() == null ? other.getUpdateTime() == null : this.getUpdateTime().equals(other.getUpdateTime()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getUuid() == null) ? 0 : getUuid().hashCode());
        result = prime * result + ((getAntiFakeSealId() == null) ? 0 : getAntiFakeSealId().hashCode());
        result = prime * result + ((getAntiFakeSealTemplateId() == null) ? 0 : getAntiFakeSealTemplateId().hashCode());
        result = prime * result + ((getAntiFakeSignId() == null) ? 0 : getAntiFakeSignId().hashCode());
        result = prime * result + ((getAntiFakeUser() == null) ? 0 : getAntiFakeUser().hashCode());
        result = prime * result + ((getAntiFakePassword() == null) ? 0 : getAntiFakePassword().hashCode());
        result = prime * result + ((getAntiFakeSize() == null) ? 0 : getAntiFakeSize().hashCode());
        result = prime * result + ((getAntiFakePath() == null) ? 0 : getAntiFakePath().hashCode());
        result = prime * result + ((getAntiFakePurpose() == null) ? 0 : getAntiFakePurpose().hashCode());
        result = prime * result + ((getAntiFakeIntroduction() == null) ? 0 : getAntiFakeIntroduction().hashCode());
        result = prime * result + ((getAntiFakeTime() == null) ? 0 : getAntiFakeTime().hashCode());
        result = prime * result + ((getCreateTime() == null) ? 0 : getCreateTime().hashCode());
        result = prime * result + ((getUpdateTime() == null) ? 0 : getUpdateTime().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", uuid=").append(uuid);
        sb.append(", antiFakeSealId=").append(antiFakeSealId);
        sb.append(", antiFakeSealTemplateId=").append(antiFakeSealTemplateId);
        sb.append(", antiFakeSignId=").append(antiFakeSignId);
        sb.append(", antiFakeUser=").append(antiFakeUser);
        sb.append(", antiFakePassword=").append(antiFakePassword);
        sb.append(", antiFakeSize=").append(antiFakeSize);
        sb.append(", antiFakePath=").append(antiFakePath);
        sb.append(", antiFakePurpose=").append(antiFakePurpose);
        sb.append(", antiFakeIntroduction=").append(antiFakeIntroduction);
        sb.append(", antiFakeTime=").append(antiFakeTime);
        sb.append(", createTime=").append(createTime);
        sb.append(", updateTime=").append(updateTime);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}