package com.upc.sealback.bean;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;

import com.upc.sealback.bean.SealSign;
import lombok.Data;

/**
 * 文件签署-签署人表
 * @author zymac
 * @TableName seal_signer
 */
@TableName(value ="seal_signer")
@Data
public class SealSigner implements Serializable {

    private SealSign sealSign;

    /**
     * 自增主键
     */
    @TableId(value = "uuid", type = IdType.AUTO)
    private Integer uuid;

    /**
     * 签署人id
     */
    @TableField(value = "signer_id")
    private String signerId;

    /**
     * 签署任务id
     */
    @TableField(value = "signer_sign_id")
    private String signerSignId;

    /**
     * 签署角色
     */
    @TableField(value = "signer_role")
    private String signerRole;

    /**
     * 签署人查看文件时间
     */
    @TableField(value = "signer_view_time")
    private Date signerViewTime;

    /**
     * 签署人使用防伪电子印章id
     */
    @TableField(value = "signer_anti_fake_seal_id")
    private String signerAntiFakeSealId;

    /**
     * 签署时间
     */
    @TableField(value = "signer_sign_time")
    private Date signerSignTime;

    /**
     * 签署状态(0：未查看；1:待签署;2:已签署)
     */
    @TableField(value = "signer_sign_status")
    private Integer signerSignStatus;

    /**
     * 创建时间
     */
    @TableField(value = "create_time")
    private Date createTime;

    /**
     * 修改时间
     */
    @TableField(value = "update_time")
    private Date updateTime;

    /**
     * 印章签署页面
     */
    @TableField(value = "signer_sign_page")
    private Integer signerSignPage;

    /**
     * 印章放置x坐标
     */
    @TableField(value = "signer_sign_x")
    private Double signerSignX;

    /**
     * 印章放置x坐标
     */
    @TableField(value = "signer_sign_y")
    private Double signerSignY;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        SealSigner other = (SealSigner) that;
        return (this.getUuid() == null ? other.getUuid() == null : this.getUuid().equals(other.getUuid()))
            && (this.getSignerId() == null ? other.getSignerId() == null : this.getSignerId().equals(other.getSignerId()))
            && (this.getSignerSignId() == null ? other.getSignerSignId() == null : this.getSignerSignId().equals(other.getSignerSignId()))
            && (this.getSignerRole() == null ? other.getSignerRole() == null : this.getSignerRole().equals(other.getSignerRole()))
            && (this.getSignerViewTime() == null ? other.getSignerViewTime() == null : this.getSignerViewTime().equals(other.getSignerViewTime()))
            && (this.getSignerAntiFakeSealId() == null ? other.getSignerAntiFakeSealId() == null : this.getSignerAntiFakeSealId().equals(other.getSignerAntiFakeSealId()))
            && (this.getSignerSignTime() == null ? other.getSignerSignTime() == null : this.getSignerSignTime().equals(other.getSignerSignTime()))
            && (this.getSignerSignStatus() == null ? other.getSignerSignStatus() == null : this.getSignerSignStatus().equals(other.getSignerSignStatus()))
            && (this.getCreateTime() == null ? other.getCreateTime() == null : this.getCreateTime().equals(other.getCreateTime()))
            && (this.getUpdateTime() == null ? other.getUpdateTime() == null : this.getUpdateTime().equals(other.getUpdateTime()))
            && (this.getSignerSignPage() == null ? other.getSignerSignPage() == null : this.getSignerSignPage().equals(other.getSignerSignPage()))
            && (this.getSignerSignX() == null ? other.getSignerSignX() == null : this.getSignerSignX().equals(other.getSignerSignX()))
            && (this.getSignerSignY() == null ? other.getSignerSignY() == null : this.getSignerSignY().equals(other.getSignerSignY()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getUuid() == null) ? 0 : getUuid().hashCode());
        result = prime * result + ((getSignerId() == null) ? 0 : getSignerId().hashCode());
        result = prime * result + ((getSignerSignId() == null) ? 0 : getSignerSignId().hashCode());
        result = prime * result + ((getSignerRole() == null) ? 0 : getSignerRole().hashCode());
        result = prime * result + ((getSignerViewTime() == null) ? 0 : getSignerViewTime().hashCode());
        result = prime * result + ((getSignerAntiFakeSealId() == null) ? 0 : getSignerAntiFakeSealId().hashCode());
        result = prime * result + ((getSignerSignTime() == null) ? 0 : getSignerSignTime().hashCode());
        result = prime * result + ((getSignerSignStatus() == null) ? 0 : getSignerSignStatus().hashCode());
        result = prime * result + ((getCreateTime() == null) ? 0 : getCreateTime().hashCode());
        result = prime * result + ((getUpdateTime() == null) ? 0 : getUpdateTime().hashCode());
        result = prime * result + ((getSignerSignPage() == null) ? 0 : getSignerSignPage().hashCode());
        result = prime * result + ((getSignerSignX() == null) ? 0 : getSignerSignX().hashCode());
        result = prime * result + ((getSignerSignY() == null) ? 0 : getSignerSignY().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", uuid=").append(uuid);
        sb.append(", signerId=").append(signerId);
        sb.append(", signerSignId=").append(signerSignId);
        sb.append(", signerRole=").append(signerRole);
        sb.append(", signerViewTime=").append(signerViewTime);
        sb.append(", signerAntiFakeSealId=").append(signerAntiFakeSealId);
        sb.append(", signerSignTime=").append(signerSignTime);
        sb.append(", signerSignStatus=").append(signerSignStatus);
        sb.append(", createTime=").append(createTime);
        sb.append(", updateTime=").append(updateTime);
        sb.append(", signerSignPage=").append(signerSignPage);
        sb.append(", signerSignX=").append(signerSignX);
        sb.append(", signerSignY=").append(signerSignY);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}