package com.upc.sealback.bean;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 印章授权表
 * @TableName seal_auth
 */
@TableName(value ="seal_auth")
@Data
public class SealAuth implements Serializable {
    /**
     * 自增主键
     */
    @TableId(value = "uuid", type = IdType.AUTO)
    private Integer uuid;

    /**
     * 授权印章id
     */
    @TableField(value = "auth_seal_id")
    private String authSealId;

    /**
     * 印章授权人
     */
    @TableField(value = "auth_seal_user")
    private String authSealUser;

    /**
     * 印章授权人姓名
     */
    private String authSealUserName;

    /**
     * 授权角色
     */
    @TableField(value = "auth_seal_role")
    private Integer authSealRole;

    /**
     * 印章授权范围
     */
    @TableField(value = "auth_seal_range")
    private Integer authSealRange;

    /**
     * 印章授权起始时间
     */
    @TableField(value = "auth_start_time")
    private Date authStartTime;

    /**
     * 印章授权结束时间
     */
    @TableField(value = "auth_end_time")
    private Date authEndTime;

    /**
     * 印章状态
     */
    @TableField(value = "auth_status")
    private Integer authStatus;

    /**
     * 该授权人使用印章的次数
     */
    @TableField(value = "auth_use_num")
    private Integer authUseNum;

    /**
     * 创建时间
     */
    @TableField(value = "create_time")
    private Date createTime;

    /**
     * 修改时间
     */
    @TableField(value = "update_time")
    private Date updateTime;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        SealAuth other = (SealAuth) that;
        return (this.getUuid() == null ? other.getUuid() == null : this.getUuid().equals(other.getUuid()))
            && (this.getAuthSealId() == null ? other.getAuthSealId() == null : this.getAuthSealId().equals(other.getAuthSealId()))
            && (this.getAuthSealUser() == null ? other.getAuthSealUser() == null : this.getAuthSealUser().equals(other.getAuthSealUser()))
            && (this.getAuthSealRole() == null ? other.getAuthSealRole() == null : this.getAuthSealRole().equals(other.getAuthSealRole()))
            && (this.getAuthSealRange() == null ? other.getAuthSealRange() == null : this.getAuthSealRange().equals(other.getAuthSealRange()))
            && (this.getAuthStartTime() == null ? other.getAuthStartTime() == null : this.getAuthStartTime().equals(other.getAuthStartTime()))
            && (this.getAuthEndTime() == null ? other.getAuthEndTime() == null : this.getAuthEndTime().equals(other.getAuthEndTime()))
            && (this.getAuthStatus() == null ? other.getAuthStatus() == null : this.getAuthStatus().equals(other.getAuthStatus()))
            && (this.getAuthUseNum() == null ? other.getAuthUseNum() == null : this.getAuthUseNum().equals(other.getAuthUseNum()))
            && (this.getCreateTime() == null ? other.getCreateTime() == null : this.getCreateTime().equals(other.getCreateTime()))
            && (this.getUpdateTime() == null ? other.getUpdateTime() == null : this.getUpdateTime().equals(other.getUpdateTime()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getUuid() == null) ? 0 : getUuid().hashCode());
        result = prime * result + ((getAuthSealId() == null) ? 0 : getAuthSealId().hashCode());
        result = prime * result + ((getAuthSealUser() == null) ? 0 : getAuthSealUser().hashCode());
        result = prime * result + ((getAuthSealRole() == null) ? 0 : getAuthSealRole().hashCode());
        result = prime * result + ((getAuthSealRange() == null) ? 0 : getAuthSealRange().hashCode());
        result = prime * result + ((getAuthStartTime() == null) ? 0 : getAuthStartTime().hashCode());
        result = prime * result + ((getAuthEndTime() == null) ? 0 : getAuthEndTime().hashCode());
        result = prime * result + ((getAuthStatus() == null) ? 0 : getAuthStatus().hashCode());
        result = prime * result + ((getAuthUseNum() == null) ? 0 : getAuthUseNum().hashCode());
        result = prime * result + ((getCreateTime() == null) ? 0 : getCreateTime().hashCode());
        result = prime * result + ((getUpdateTime() == null) ? 0 : getUpdateTime().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", uuid=").append(uuid);
        sb.append(", authSealId=").append(authSealId);
        sb.append(", authSealUser=").append(authSealUser);
        sb.append(", authSealRole=").append(authSealRole);
        sb.append(", authSealRange=").append(authSealRange);
        sb.append(", authStartTime=").append(authStartTime);
        sb.append(", authEndTime=").append(authEndTime);
        sb.append(", authStatus=").append(authStatus);
        sb.append(", authUseNum=").append(authUseNum);
        sb.append(", createTime=").append(createTime);
        sb.append(", updateTime=").append(updateTime);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}