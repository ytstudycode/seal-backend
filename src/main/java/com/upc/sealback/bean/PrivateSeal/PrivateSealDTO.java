package com.upc.sealback.bean.PrivateSeal;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @description 后端处理印章的bean类
 */
@Data
@Accessors(chain = true)
public class PrivateSealDTO {
    /**主体内容*/
    private String fontContent;
    /**文字大小*/
    private Integer fontSize;
    /**文字间距*/
    private Double fontSpace;
    /**文字距外边距*/
    private Integer marginSize;
    /**字体类别 1:方正黑体,2:仿宋 3:楷体*/
    private Integer fontFamilyTag;
    /**颜色类别 1:红色, 2:黑色, 3:蓝色*/
    private Integer colorTag;
    /** 具体字体名称 */
    private String fontFamily;
    /** 颜色透明度 */
    private Integer colorOpacity;
    /** 印章样式 1:矩形章-带框,2:矩形章-不带框;3:方形章-带框,4:方形章-不带框*/
    private Integer fontStyle;

    public PrivateSealDTO ifNullInitDefault(PrivateSealDTO dto){
        dto.setColorTag(dto.getColorTag()==null?1:dto.getColorTag());
        dto.setFontSize(dto.getFontSize()==null?120:dto.getFontSize());
        dto.setFontFamilyTag(dto.getFontFamilyTag()==null?1:dto.getFontFamilyTag());
        return dto;
    }

}
