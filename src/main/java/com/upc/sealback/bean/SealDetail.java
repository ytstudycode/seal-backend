package com.upc.sealback.bean;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 保存每一个印章的详情
 * @TableName seal_detail
 */
@TableName(value ="seal_detail")
@Data
public class SealDetail implements Serializable {
    /**
     * 自增主键
     */
    @TableId(value = "uuid")
    private Integer uuid;

    /**
     * 印章编号
     */
    @TableField(value = "seal_id")
    private String sealId;

    /**
     * 印章存储路径
     */
    @TableField(value = "seal_path")
    private String sealPath;

    /**
     * 印章名称
     */
    @TableField(value = "seal_name")
    private String sealName;

    /**
     * 印章主体内容
     */
    @TableField(value = "seal_content")
    private String sealContent;

    /**
     * 印章类型（0个人印章；1公章；2合同专用章；3发票专用章；4财务专用章；5人事专用章）
     */
    @TableField(value = "seal_type")
    private Integer sealType;

    /**
     * 印章规则（1:无;2:带”印”;3:带”之印”）
     */
    @TableField(value = "seal_rule")
    private Integer sealRule;

    /**
     * 印章样式（1:矩形章-带框；2矩形章-不带框；3方形左大字-带框；4方形左大字-不带框；5方形右大字-带框；6方形右大字-不带框；11圆形章-不带星；22圆形章-带星；13椭圆章；21圆形章-不带星；22圆形章-带星；31椭圆章；41圆形章-不带星；42圆形章-带星；51圆形章-不带星；52圆形章-带星；53椭圆章；）
     */
    @TableField(value = "seal_style")
    private Integer sealStyle;

    /**
     * 印章横向文
     */
    @TableField(value = "seal_transverse")
    private String sealTransverse;

    /**
     * 印章横向数字
     */
    @TableField(value = "seal_number")
    private String sealNumber;

    /**
     * 印章下弦文
     */
    @TableField(value = "seal_lower")
    private String sealLower;

    /**
     * 印章颜色（1代表红色，2代表黑色，3代表蓝色）
     */
    @TableField(value = "seal_color")
    private Integer sealColor;

    /**
     * 印章颜色透明度
     */
    @TableField(value = "seal_opacity")
    private Integer sealOpacity;

    /**
     * 创建时间
     */
    @TableField(value = "create_time")
    private Date createTime;

    /**
     * 修改时间
     */
    @TableField(value = "update_time")
    private Date updateTime;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        SealDetail other = (SealDetail) that;
        return (this.getUuid() == null ? other.getUuid() == null : this.getUuid().equals(other.getUuid()))
            && (this.getSealId() == null ? other.getSealId() == null : this.getSealId().equals(other.getSealId()))
            && (this.getSealPath() == null ? other.getSealPath() == null : this.getSealPath().equals(other.getSealPath()))
            && (this.getSealName() == null ? other.getSealName() == null : this.getSealName().equals(other.getSealName()))
            && (this.getSealContent() == null ? other.getSealContent() == null : this.getSealContent().equals(other.getSealContent()))
            && (this.getSealType() == null ? other.getSealType() == null : this.getSealType().equals(other.getSealType()))
            && (this.getSealRule() == null ? other.getSealRule() == null : this.getSealRule().equals(other.getSealRule()))
            && (this.getSealStyle() == null ? other.getSealStyle() == null : this.getSealStyle().equals(other.getSealStyle()))
            && (this.getSealTransverse() == null ? other.getSealTransverse() == null : this.getSealTransverse().equals(other.getSealTransverse()))
            && (this.getSealNumber() == null ? other.getSealNumber() == null : this.getSealNumber().equals(other.getSealNumber()))
            && (this.getSealLower() == null ? other.getSealLower() == null : this.getSealLower().equals(other.getSealLower()))
            && (this.getSealColor() == null ? other.getSealColor() == null : this.getSealColor().equals(other.getSealColor()))
            && (this.getSealOpacity() == null ? other.getSealOpacity() == null : this.getSealOpacity().equals(other.getSealOpacity()))
            && (this.getCreateTime() == null ? other.getCreateTime() == null : this.getCreateTime().equals(other.getCreateTime()))
            && (this.getUpdateTime() == null ? other.getUpdateTime() == null : this.getUpdateTime().equals(other.getUpdateTime()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getUuid() == null) ? 0 : getUuid().hashCode());
        result = prime * result + ((getSealId() == null) ? 0 : getSealId().hashCode());
        result = prime * result + ((getSealPath() == null) ? 0 : getSealPath().hashCode());
        result = prime * result + ((getSealName() == null) ? 0 : getSealName().hashCode());
        result = prime * result + ((getSealContent() == null) ? 0 : getSealContent().hashCode());
        result = prime * result + ((getSealType() == null) ? 0 : getSealType().hashCode());
        result = prime * result + ((getSealRule() == null) ? 0 : getSealRule().hashCode());
        result = prime * result + ((getSealStyle() == null) ? 0 : getSealStyle().hashCode());
        result = prime * result + ((getSealTransverse() == null) ? 0 : getSealTransverse().hashCode());
        result = prime * result + ((getSealNumber() == null) ? 0 : getSealNumber().hashCode());
        result = prime * result + ((getSealLower() == null) ? 0 : getSealLower().hashCode());
        result = prime * result + ((getSealColor() == null) ? 0 : getSealColor().hashCode());
        result = prime * result + ((getSealOpacity() == null) ? 0 : getSealOpacity().hashCode());
        result = prime * result + ((getCreateTime() == null) ? 0 : getCreateTime().hashCode());
        result = prime * result + ((getUpdateTime() == null) ? 0 : getUpdateTime().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", uuid=").append(uuid);
        sb.append(", sealId=").append(sealId);
        sb.append(", sealPath=").append(sealPath);
        sb.append(", sealName=").append(sealName);
        sb.append(", sealContent=").append(sealContent);
        sb.append(", sealType=").append(sealType);
        sb.append(", sealRule=").append(sealRule);
        sb.append(", sealStyle=").append(sealStyle);
        sb.append(", sealTransverse=").append(sealTransverse);
        sb.append(", sealNumber=").append(sealNumber);
        sb.append(", sealLower=").append(sealLower);
        sb.append(", sealColor=").append(sealColor);
        sb.append(", sealOpacity=").append(sealOpacity);
        sb.append(", createTime=").append(createTime);
        sb.append(", updateTime=").append(updateTime);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}