package com.upc.sealback.bean.PublicSeal;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author mqz
 * @description
 * @abount https://github.com/DemoMeng
 * @since 2020/10/19
 */
@Data
@Accessors(chain = true)
public class PublicSealDTO {
    private String companyName;
    private Integer color;
    private Integer font;
    private String no;//下弦文字内容
    private String title;//标题名称
    private String number;//标题名称
    /** 颜色透明度 */
    private Integer colorOpacity;
    /**颜色类别 1:红色, 2:黑色, 3:蓝色*/
    private Integer colorTag;
    /** 具体的字体 */
    private String fontFamily;

    private Integer fontType;

    /** 是否带星 */
    private Integer star;

    public static PublicSealDTO initDefault(PublicSealDTO dto){
        dto.setColorTag(dto.getColorTag()==null?1:dto.getColorTag());
        dto.setColor(dto.getColor()==null?0:dto.getColor());
        dto.setFont(dto.getFont()==null?0:dto.getFont());
        return dto;
    }

}
