package com.upc.sealback.bean;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.upc.sealback.bean.SealSigner;
import lombok.Data;

/**
 * 文件签署详情
 * @TableName seal_sign
 */
@TableName(value ="seal_sign")
@Data
public class SealSign implements Serializable {

    private List<SealSigner> sealSigner;
    /**
     * 自增主键
     */
    @TableId(value = "uuid", type = IdType.AUTO)
    private Integer uuid;

    /**
     * 任务ID
     */
    @TableField(value = "sign_id")
    private String signId;

    /**
     * 签署任务名称
     */
    @TableField(value = "sign_name")
    private String signName;

    /**
     * 签署文件路径
     */
    @TableField(value = "sign_file_path")
    private String signFilePath;

    /**
     * 签署文件名称
     */
    @TableField(value = "sign_file_name")
    private String signFileName;

    /**
     * 签署任务发起人
     */
    @TableField(value = "sign_sponsor")
    private String signSponsor;

    /**
     * 签署任务参与人
     */
    @TableField(value = "sign_participant")
    private String signParticipant;

    /**
     * 签署截止时间
     */
    @TableField(value = "sign_deadline")
    private Date signDeadline;

    /**
     * 任务发起时间
     */
    @TableField(value = "sign_start_time")
    private Date signStartTime;

    /**
     * 任务签署时间
     */
    @TableField(value = "sign_sign_time")
    private Date signSignTime;

    /**
     * 任务结束时间
     */
    @TableField(value = "sign_end_time")
    private Date signEndTime;

    /**
     * 任务状态(0:草稿；1:已过期；2:已撤销；3:进行中；4:已完成)
     */
    @TableField(value = "sign_status")
    private Integer signStatus;

    /**
     * 创建时间
     */
    @TableField(value = "create_time")
    private Date createTime;

    /**
     * 更新时间
     */
    @TableField(value = "update_time")
    private Date updateTime;

    /**
     * 生成的防伪电子印章id
     */
    @TableField(value = "sign_seal_id")
    private String signSealId;

    /**
     * 生成的防伪电子印章路径
     */
    @TableField(value = "sign_seal_path")
    private String signSealPath;

    /**
     * 使用的印章模板id
     */
    @TableField(value = "sign_template_seal_id")
    private String signTemplateSealId;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        SealSign other = (SealSign) that;
        return (this.getUuid() == null ? other.getUuid() == null : this.getUuid().equals(other.getUuid()))
            && (this.getSignId() == null ? other.getSignId() == null : this.getSignId().equals(other.getSignId()))
            && (this.getSignName() == null ? other.getSignName() == null : this.getSignName().equals(other.getSignName()))
            && (this.getSignFilePath() == null ? other.getSignFilePath() == null : this.getSignFilePath().equals(other.getSignFilePath()))
            && (this.getSignFileName() == null ? other.getSignFileName() == null : this.getSignFileName().equals(other.getSignFileName()))
            && (this.getSignSponsor() == null ? other.getSignSponsor() == null : this.getSignSponsor().equals(other.getSignSponsor()))
            && (this.getSignParticipant() == null ? other.getSignParticipant() == null : this.getSignParticipant().equals(other.getSignParticipant()))
            && (this.getSignDeadline() == null ? other.getSignDeadline() == null : this.getSignDeadline().equals(other.getSignDeadline()))
            && (this.getSignStartTime() == null ? other.getSignStartTime() == null : this.getSignStartTime().equals(other.getSignStartTime()))
            && (this.getSignSignTime() == null ? other.getSignSignTime() == null : this.getSignSignTime().equals(other.getSignSignTime()))
            && (this.getSignEndTime() == null ? other.getSignEndTime() == null : this.getSignEndTime().equals(other.getSignEndTime()))
            && (this.getSignStatus() == null ? other.getSignStatus() == null : this.getSignStatus().equals(other.getSignStatus()))
            && (this.getCreateTime() == null ? other.getCreateTime() == null : this.getCreateTime().equals(other.getCreateTime()))
            && (this.getUpdateTime() == null ? other.getUpdateTime() == null : this.getUpdateTime().equals(other.getUpdateTime()))
            && (this.getSignSealId() == null ? other.getSignSealId() == null : this.getSignSealId().equals(other.getSignSealId()))
            && (this.getSignSealPath() == null ? other.getSignSealPath() == null : this.getSignSealPath().equals(other.getSignSealPath()))
            && (this.getSignTemplateSealId() == null ? other.getSignTemplateSealId() == null : this.getSignTemplateSealId().equals(other.getSignTemplateSealId()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getUuid() == null) ? 0 : getUuid().hashCode());
        result = prime * result + ((getSignId() == null) ? 0 : getSignId().hashCode());
        result = prime * result + ((getSignName() == null) ? 0 : getSignName().hashCode());
        result = prime * result + ((getSignFilePath() == null) ? 0 : getSignFilePath().hashCode());
        result = prime * result + ((getSignFileName() == null) ? 0 : getSignFileName().hashCode());
        result = prime * result + ((getSignSponsor() == null) ? 0 : getSignSponsor().hashCode());
        result = prime * result + ((getSignParticipant() == null) ? 0 : getSignParticipant().hashCode());
        result = prime * result + ((getSignDeadline() == null) ? 0 : getSignDeadline().hashCode());
        result = prime * result + ((getSignStartTime() == null) ? 0 : getSignStartTime().hashCode());
        result = prime * result + ((getSignSignTime() == null) ? 0 : getSignSignTime().hashCode());
        result = prime * result + ((getSignEndTime() == null) ? 0 : getSignEndTime().hashCode());
        result = prime * result + ((getSignStatus() == null) ? 0 : getSignStatus().hashCode());
        result = prime * result + ((getCreateTime() == null) ? 0 : getCreateTime().hashCode());
        result = prime * result + ((getUpdateTime() == null) ? 0 : getUpdateTime().hashCode());
        result = prime * result + ((getSignSealId() == null) ? 0 : getSignSealId().hashCode());
        result = prime * result + ((getSignSealPath() == null) ? 0 : getSignSealPath().hashCode());
        result = prime * result + ((getSignTemplateSealId() == null) ? 0 : getSignTemplateSealId().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", uuid=").append(uuid);
        sb.append(", signId=").append(signId);
        sb.append(", signName=").append(signName);
        sb.append(", signFilePath=").append(signFilePath);
        sb.append(", signFileName=").append(signFileName);
        sb.append(", signSponsor=").append(signSponsor);
        sb.append(", signParticipant=").append(signParticipant);
        sb.append(", signDeadline=").append(signDeadline);
        sb.append(", signStartTime=").append(signStartTime);
        sb.append(", signSignTime=").append(signSignTime);
        sb.append(", signEndTime=").append(signEndTime);
        sb.append(", signStatus=").append(signStatus);
        sb.append(", createTime=").append(createTime);
        sb.append(", updateTime=").append(updateTime);
        sb.append(", signSealId=").append(signSealId);
        sb.append(", signSealPath=").append(signSealPath);
        sb.append(", signTemplateSealId=").append(signTemplateSealId);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}