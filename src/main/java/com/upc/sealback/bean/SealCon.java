package com.upc.sealback.bean;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

import lombok.Data;

/**
 * 保存印章的使用情况
 * @TableName seal_con
 */
@TableName(value ="seal_con")
@Data
public class SealCon implements Serializable {

    /**
     * 被授权的用印章信息
     */
    private SealAuth sealAuth;

    /**
     * 自增主键
     */
    @TableId(value = "uuid", type = IdType.AUTO)
    private Integer uuid;

    /**
     * 印章编号
     */
    @TableField(value = "seal_id")
    private String sealId;

    /**
     * 印章名称
     */
    @TableField(value = "seal_name")
    private String sealName;

    /**
     * 印章使用次数
     */
    @TableField(value = "seal_use_num")
    private Integer sealUseNum;

    /**
     * 印章图片
     */
    @TableField(value = "seal_path")
    private String sealPath;

    /**
     * 印章状态(0代表停用，1代表正常使用)
     */
    @TableField(value = "seal_status")
    private Integer sealStatus;

    /**
     * 印章创建人
     */
    @TableField(value = "seal_creater")
    private String sealCreater;

    /**
     * 印章授权数量
     */
    @TableField(value = "seal_authorize_num")
    private Integer sealAuthorizeNum;

    /**
     * 创建时间
     */
    @TableField(value = "create_time")
    private Date createTime;

    /**
     * 修改时间
     */
    @TableField(value = "update_time")
    private Date updateTime;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        SealCon other = (SealCon) that;
        return (this.getUuid() == null ? other.getUuid() == null : this.getUuid().equals(other.getUuid()))
            && (this.getSealId() == null ? other.getSealId() == null : this.getSealId().equals(other.getSealId()))
            && (this.getSealName() == null ? other.getSealName() == null : this.getSealName().equals(other.getSealName()))
            && (this.getSealUseNum() == null ? other.getSealUseNum() == null : this.getSealUseNum().equals(other.getSealUseNum()))
            && (this.getSealPath() == null ? other.getSealPath() == null : this.getSealPath().equals(other.getSealPath()))
            && (this.getSealStatus() == null ? other.getSealStatus() == null : this.getSealStatus().equals(other.getSealStatus()))
            && (this.getSealCreater() == null ? other.getSealCreater() == null : this.getSealCreater().equals(other.getSealCreater()))
            && (this.getSealAuthorizeNum() == null ? other.getSealAuthorizeNum() == null : this.getSealAuthorizeNum().equals(other.getSealAuthorizeNum()))
            && (this.getCreateTime() == null ? other.getCreateTime() == null : this.getCreateTime().equals(other.getCreateTime()))
            && (this.getUpdateTime() == null ? other.getUpdateTime() == null : this.getUpdateTime().equals(other.getUpdateTime()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getUuid() == null) ? 0 : getUuid().hashCode());
        result = prime * result + ((getSealId() == null) ? 0 : getSealId().hashCode());
        result = prime * result + ((getSealName() == null) ? 0 : getSealName().hashCode());
        result = prime * result + ((getSealUseNum() == null) ? 0 : getSealUseNum().hashCode());
        result = prime * result + ((getSealPath() == null) ? 0 : getSealPath().hashCode());
        result = prime * result + ((getSealStatus() == null) ? 0 : getSealStatus().hashCode());
        result = prime * result + ((getSealCreater() == null) ? 0 : getSealCreater().hashCode());
        result = prime * result + ((getSealAuthorizeNum() == null) ? 0 : getSealAuthorizeNum().hashCode());
        result = prime * result + ((getCreateTime() == null) ? 0 : getCreateTime().hashCode());
        result = prime * result + ((getUpdateTime() == null) ? 0 : getUpdateTime().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", uuid=").append(uuid);
        sb.append(", sealId=").append(sealId);
        sb.append(", sealName=").append(sealName);
        sb.append(", sealUseNum=").append(sealUseNum);
        sb.append(", sealPath=").append(sealPath);
        sb.append(", sealStatus=").append(sealStatus);
        sb.append(", sealCreater=").append(sealCreater);
        sb.append(", sealAuthorizeNum=").append(sealAuthorizeNum);
        sb.append(", createTime=").append(createTime);
        sb.append(", updateTime=").append(updateTime);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}