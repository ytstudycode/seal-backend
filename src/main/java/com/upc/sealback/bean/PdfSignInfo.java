package com.upc.sealback.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @description 数字签名信息
 * @create zymac
 * @date 2022/5/31 20:23
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PdfSignInfo {
    /**
     * PDF名称
     */
    private String pdfName;
    /**
     * PDF大小
     */
    private String pdfSize;

    private List<SignatureDetail> signatureDetails = new ArrayList<>();

    public static class SignatureDetail {
        /**
         * 数字签名名称
         */
        public String signName;
        /**
         * 签名日期时间
         */
        public Date signTime;
        /**
         * 有效期开始时间
         */
        public Date validStartTime;
        /**
         * 有效期结束时间
         */
        public Date validEndTime;
        /**
         * 证书名称
         */
        public String certName;
        /**
         * 证书序列号
         */
        public String serialNumber;
        /**
         * 证书公钥
         */
        public String publicKey;
        /**
         * 证书格式
         */
        public String pubKeyFormat;
        /**
         * 证书签名算法
         */
        public String sigAlgName;
        /**
         * 证书颁发者
         */
        public String userDnName;
        /**
         * 是否被篡改
         */
        public boolean validate;

        public String imageContent;
        /**
         * 图片路径
         */
        public String imagePath;

        /**
         * 签名位置
         */
        public String reason;
        /**
         * 签名类型
         */
        public String location;

        /**
         * 签名页面
         */
        public int pageNum;

        /**
         * 图片的base64编码
         */
        public String sealBase64;

        /**
         * 文件摘要
         */
        public String fileDigest;

        public float llx, lly, urx, ury;
        /**
         * 文件路径
         */
        public String filePath;
    }

}
