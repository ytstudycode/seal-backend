/*
 Navicat Premium Data Transfer

 Source Server         : aliyun
 Source Server Type    : MySQL
 Source Server Version : 80022
 Source Host           : rm-m5ej58n62xt8fmkp8lo.mysql.rds.aliyuncs.com:3306
 Source Schema         : seal

 Target Server Type    : MySQL
 Target Server Version : 80022
 File Encoding         : 65001

 Date: 26/11/2022 12:27:11
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for seal_anti_fake
-- ----------------------------
DROP TABLE IF EXISTS `seal_anti_fake`;
CREATE TABLE `seal_anti_fake` (
  `uuid` int NOT NULL AUTO_INCREMENT COMMENT '自增主键',
  `anti_fake_seal_id` varchar(255) DEFAULT NULL COMMENT '防伪电子印章id',
  `anti_fake_seal_template_id` varchar(255) DEFAULT NULL COMMENT '生成电子印章的模板id',
  `anti_fake_sign_id` varchar(255) DEFAULT NULL COMMENT '使用该电子印章的任务id',
  `anti_fake_user` varchar(255) DEFAULT NULL COMMENT '生成电子印章的用户',
  `anti_fake_password` varchar(255) DEFAULT NULL COMMENT '对该电子印章盲水印加密和解密的密码',
  `anti_fake_size` int DEFAULT NULL COMMENT '生成的盲水印大小',
  `anti_fake_path` varchar(255) DEFAULT NULL COMMENT '生成的电子印章路径',
  `anti_fake_purpose` int DEFAULT NULL COMMENT '生成电子印章用途(0:导出电子印章;1:签署文件)',
  `anti_fake_introduction` varchar(5000) DEFAULT NULL COMMENT '生成电子印章的说明',
  `anti_fake_time` datetime DEFAULT NULL COMMENT '生成电子印章的时间',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`uuid`)
) ENGINE=InnoDB AUTO_INCREMENT=133 DEFAULT CHARSET=utf8 COMMENT='保存生成的防伪电子印章详情';

-- ----------------------------
-- Table structure for seal_auth
-- ----------------------------
DROP TABLE IF EXISTS `seal_auth`;
CREATE TABLE `seal_auth` (
  `uuid` int NOT NULL AUTO_INCREMENT COMMENT '自增主键',
  `auth_seal_id` varchar(255) DEFAULT NULL COMMENT '授权印章id',
  `auth_seal_user` varchar(255) DEFAULT NULL COMMENT '印章授权人',
  `auth_seal_role` int DEFAULT NULL COMMENT '授权角色',
  `auth_seal_range` int DEFAULT NULL COMMENT '印章授权范围',
  `auth_start_time` datetime DEFAULT NULL COMMENT '印章授权起始时间',
  `auth_end_time` datetime DEFAULT NULL COMMENT '印章授权结束时间',
  `auth_status` int DEFAULT NULL COMMENT '印章状态',
  `auth_use_num` int DEFAULT NULL COMMENT '该授权人使用印章的次数',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`uuid`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8 COMMENT='保存模板印章授权情况';

-- ----------------------------
-- Table structure for seal_con
-- ----------------------------
DROP TABLE IF EXISTS `seal_con`;
CREATE TABLE `seal_con` (
  `uuid` int NOT NULL AUTO_INCREMENT COMMENT '自增主键',
  `seal_id` varchar(5000) NOT NULL COMMENT '印章编号',
  `seal_name` varchar(5000) DEFAULT NULL COMMENT '印章名称',
  `seal_use_num` int DEFAULT '0' COMMENT '印章使用次数',
  `seal_path` varchar(5000) DEFAULT NULL COMMENT '印章图片',
  `seal_status` int DEFAULT NULL COMMENT '印章状态(0代表停用，1代表正常使用)',
  `seal_creater` varchar(5000) DEFAULT NULL COMMENT '印章创建人',
  `seal_authorize_num` int DEFAULT NULL COMMENT '印章授权数量',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`uuid`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8 COMMENT='保存模板印章的相关信息';

-- ----------------------------
-- Table structure for seal_detail
-- ----------------------------
DROP TABLE IF EXISTS `seal_detail`;
CREATE TABLE `seal_detail` (
  `uuid` int NOT NULL AUTO_INCREMENT COMMENT '自增主键',
  `seal_id` varchar(5000) DEFAULT NULL COMMENT '印章编号',
  `seal_path` varchar(5000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '印章存储路径',
  `seal_name` varchar(255) DEFAULT NULL COMMENT '印章名称',
  `seal_content` varchar(255) DEFAULT NULL COMMENT '印章主体内容',
  `seal_type` int DEFAULT NULL COMMENT '印章类型（0个人印章；1公章；2合同专用章；3发票专用章；4财务专用章；5人事专用章）',
  `seal_rule` int DEFAULT NULL COMMENT '印章规则（1:无;2:带”印”;3:带”之印”）',
  `seal_style` int DEFAULT NULL COMMENT '印章样式（1:矩形章-带框；2矩形章-不带框；3方形左大字-带框；4方形左大字-不带框；5方形右大字-带框；6方形右大字-不带框；11圆形章-不带星；22圆形章-带星；13椭圆章；21圆形章-不带星；22圆形章-带星；31椭圆章；41圆形章-不带星；42圆形章-带星；51圆形章-不带星；52圆形章-带星；53椭圆章；）',
  `seal_transverse` varchar(255) DEFAULT NULL COMMENT '印章横向文',
  `seal_number` varchar(255) DEFAULT NULL COMMENT '印章横向数字',
  `seal_lower` varchar(255) DEFAULT NULL COMMENT '印章下弦文',
  `seal_color` int DEFAULT NULL COMMENT '印章颜色（1代表红色，2代表黑色，3代表蓝色）',
  `seal_opacity` int DEFAULT NULL COMMENT '印章颜色透明度',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`uuid`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8 COMMENT='保存模板印章的详情';

-- ----------------------------
-- Table structure for seal_sign
-- ----------------------------
DROP TABLE IF EXISTS `seal_sign`;
CREATE TABLE `seal_sign` (
  `uuid` int NOT NULL AUTO_INCREMENT COMMENT '自增主键',
  `sign_id` varchar(255) NOT NULL COMMENT '任务ID',
  `sign_name` varchar(255) DEFAULT NULL COMMENT '签署任务名称',
  `sign_file_path` varchar(255) DEFAULT NULL COMMENT '签署文件路径',
  `sign_file_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '签署文件名称',
  `sign_sponsor` varchar(255) DEFAULT NULL COMMENT '签署任务发起人',
  `sign_participant` varchar(255) DEFAULT NULL COMMENT '签署任务参与人',
  `sign_deadline` datetime DEFAULT NULL COMMENT '签署截止时间',
  `sign_start_time` datetime DEFAULT NULL COMMENT '任务发起时间',
  `sign_sign_time` datetime DEFAULT NULL COMMENT '任务签署时间',
  `sign_end_time` datetime DEFAULT NULL COMMENT '任务结束时间',
  `sign_status` int DEFAULT NULL COMMENT '任务状态(0:草稿；1:已过期；2:已撤销；3:进行中；4:已完成)',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `sign_seal_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '生成的防伪电子印章id',
  `sign_seal_path` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '生成的防伪电子印章路径',
  `sign_template_seal_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '使用的印章模板id',
  PRIMARY KEY (`uuid`)
) ENGINE=InnoDB AUTO_INCREMENT=86 DEFAULT CHARSET=utf8 COMMENT='文件签署详情';

-- ----------------------------
-- Table structure for seal_signer
-- ----------------------------
DROP TABLE IF EXISTS `seal_signer`;
CREATE TABLE `seal_signer` (
  `uuid` int NOT NULL AUTO_INCREMENT COMMENT '自增主键',
  `signer_id` varchar(255) DEFAULT NULL COMMENT '签署人id',
  `signer_sign_id` varchar(255) DEFAULT NULL COMMENT '签署任务id',
  `signer_role` varchar(255) DEFAULT NULL COMMENT '签署角色',
  `signer_view_time` datetime DEFAULT NULL COMMENT '签署人查看文件时间',
  `signer_anti_fake_seal_id` varchar(255) DEFAULT NULL COMMENT '签署人使用防伪电子印章id',
  `signer_sign_time` datetime DEFAULT NULL COMMENT '签署时间',
  `signer_sign_status` int DEFAULT NULL COMMENT '签署状态(0：未查看；1:待签署;2:已签署)',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `signer_sign_page` int DEFAULT NULL COMMENT '印章签署页面',
  `signer_sign_x` double DEFAULT NULL COMMENT '印章放置x坐标',
  `signer_sign_y` double DEFAULT NULL COMMENT '印章放置x坐标',
  PRIMARY KEY (`uuid`)
) ENGINE=InnoDB AUTO_INCREMENT=71 DEFAULT CHARSET=utf8 COMMENT='文件签署-签署人表';

-- ----------------------------
-- Table structure for seal_user
-- ----------------------------
DROP TABLE IF EXISTS `seal_user`;
CREATE TABLE `seal_user` (
  `uuid` int NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `user_id` varchar(255) NOT NULL COMMENT '用户id（电话号码）',
  `user_name` varchar(5000) DEFAULT NULL COMMENT '用户名',
  `user_password` varchar(5000) NOT NULL COMMENT '用户密码',
  `user_avatar` varchar(5000) DEFAULT NULL COMMENT '用户头像',
  `user_id_card` varchar(255) DEFAULT NULL COMMENT '用户身份证号码',
  `user_role` int DEFAULT NULL COMMENT '用户角色（0代表普通用户，1代表管理员，2代表超级管理员）',
  `user_email` varchar(255) DEFAULT NULL COMMENT '用户邮箱',
  `user_status` int DEFAULT NULL COMMENT '账号状态（0代表封号，1代表正常，2代表已实名认证）',
  `user_login_time` datetime DEFAULT NULL COMMENT '用户登录时间',
  `create_time` datetime DEFAULT NULL COMMENT '账号创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '账号修改时间',
  PRIMARY KEY (`uuid`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='用户表';

SET FOREIGN_KEY_CHECKS = 1;
