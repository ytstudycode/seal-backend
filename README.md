# 防伪电子印章管理平台
此项目为本人本科毕业设计——防伪电子印章管理平台<br/>
前端项目地址: https://gitee.com/ytstudycode/seal-front <br/>
后端项目地址: https://gitee.com/ytstudycode/seal-backend <br/>
随着电子信息技术的快速发展，电子印章与物理印章已具有同等法律效力，已然成为各单位、社会团体和个人不可或缺的工具。但近年来非法刻制印章的问题尤为突出，造成了诸多经济损失。如何保障电子印章的真实性以及签署文件的合法性成为亟待解决的问题。本系统设计并实现了一个基于盲水印和数字签名的防伪电子印章管理系统，实现了电子印章从生成—使用—鉴别的全流程管理。
<br/>
## 项目介绍
本系统主要实现了以下功能模块：
<br/>
<div align="center">
<img src="readmeImg/img.png">
</div>

系统整体架构图如下：
<br/>
<div align="center">
<img src="readmeImg/img_1.png">
</div>

本系统使用SpringBoot + Vue3框架进行开发，前端逻辑处理主要使用Vue3，并辅以Element UI等第三方前端库组织页面，后端采用SpringBoot处理业务逻辑，使用MyBaits-Plus作为ORM对接MySQL。
<br/>
## 主要功能

### (1)不同类型电子印章模板的设计与实现
<br/>本系统提供**全面、准确的电子印章模板**供用户选择和使用，借鉴国家印章标准，严格控制电子印章尺寸、大小。
<div align="center">
<img src="readmeImg/img_2.png">
</div>

### (2)基于盲水印的防伪电子印章设计

为了保障电子印章不被篡改和盗用，涉及到的关键技术点有：<br/>
① 基于**OpenCV**的图片图层处理方案；<br/>
② 设计基于**二维傅立叶变换**的**盲水印**生成技术，盲水印中携带用户标识、印章信息，且加盲水印后的印章与原印章样式、大小无任何差异。<br/>
### (3)基于数字签名的电子文档保护设计
<br/>如果仅仅是电子印章的防伪并不能彻底解决电子印章被伪造或者被盗用的问题，本系统提出一种基于数字签名的电子文档保护设计方案，目标是保障使用电子印章签署文件后该文件的安全性，涉及到的关键技术点有：
<br/>① 基于**单向散列函数**的**摘要生成算法**，根据文件中所含内容的不同，使用单向散列函数生成内容不同、长度相同的信息摘要，便于后期进行信息比对和防篡改检验。
<br/>② 基于**数字签名**的文档保护方案，为插入盲水印的电子印章增加数字签名，而后签署到文件中，保证了电子文件的完整性和不可修改性。
<br/>文件签署与防伪流程如下图所示:

<div align="center">
<img src="readmeImg/img_3.png"><br/>文件签署流程
</div>

<div align="center">
<img src="readmeImg/img_4.png"><br/>文件防伪流程
</div>

## 系统界面
### (1)登录注册界面
<div align="center">
<img src="readmeImg/img_5.png"><br/>用户注册界面
</div>

<div align="center">
<img src="readmeImg/img_6.png"><br/>用户登录界面
</div>

### (2)印章管理界面
<div align="center">
<img src="readmeImg/img_13.png"><br/>系统工作台
</div>

<div align="center">
<img src="readmeImg/img_7.png"><br/>印章生成界面
</div>

<div align="center">
<img src="readmeImg/img_9.png"><br/>印章使用详情界面
</div>
<div align="center">
<img src="readmeImg/img_10.png"><br/>印章授权信息界面
</div>

### (3)印章使用界面
<div align="center">
<img src="readmeImg/img_11.png"><br/>发起签署界面
</div>

<div align="center">
<img src="readmeImg/img_12.png"><br/>指定签署位置界面
</div>

<div align="center">
<img src="readmeImg/img_14.png"><br/>文件签署界面（浏览器原因造成印章显示错位，建议使用火狐浏览器）
</div>

<div align="center">
<img src="readmeImg/img_15.png"><br/>防伪印章导出界面
</div>

### (4)防伪校验界面

<div align="center">
<img src="readmeImg/img_16.png"><br/>印章防伪校验界面
</div>

<div align="center">
<img src="readmeImg/img_17.png"><br/>印章防伪校验结果界面
</div>

<div align="center">
<img src="readmeImg/img_19.png"><br/>文件防伪校验界面
</div>

<div align="center">
<img src="readmeImg/img_18.png"><br/>文件防伪校验结果界面
</div>

### (5)存证管理界面
<div align="center">
<img src="readmeImg/img_20.png"><br/>签署信息存证界面
</div>

## 系统安装
### (1)环境要求
- 操作系统：macOS 12+ / Windows 10+
- 浏览器：Chrome 80+ / Firefox 74+ / Edge 80+
- Node.js v16.13.0
- npm v8.1.0/ yarn v1.22.17
- jdk 1.8
- mysql 8.0

### (2)安装步骤
#### 前端
1. 安装Node.js
2. 安装yarn/npm
3. 安装依赖包
```
yarn install
```
或者
```
npm install
```
4. 修改阿里云OSS配置<br/>
在src/utils/OSSConf.js下修改阿里云OSS配置，把相关配置改为自己的阿里云OSS配置
5. 启动项目
```
yarn run dev
```
或者
```
npm run dev
```
#### 后端
1. 安装jdk1.8
2. 安装mysql8.0
3. 创建数据库 名为seal
4. 导入数据库文件 <br/>
路径: data/sql/seal.sql
5. 修改阿里云OSS配置<br/>
在src/main/java/com/upc/sealback/utils/FileOSS.java下修改阿里云OSS配置，把相关配置改为自己的阿里云OSS配置
6. 修改实名认证配置<br/>
在src/main/java/com/upc/sealback/utils/Authentication.java下修改实名认证配置，把相关配置改为自己的实名认证配置
7. 修改短信验证码配置（可选）
<br/>在src/main/java/com/upc/sealback/utils/MsCode.java下修改短信验证码配置，把相关配置改为自己的短信验证码配置
8. 启动项目


注：
本系统部分图标来自e签宝-电子签名服务平台网页，部分网页模仿e签宝，没有商用，如有侵权请联系删除。
<br/>本系统使用猪猪侠的CA服务器教程，如有需要请参考：https://gitee.com/zhf_sy/zzxia-openssl-ca-server
